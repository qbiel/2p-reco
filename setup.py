from setuptools import setup

setup(
    name='treco',
    version='0.9.0',
    author='Adam Kubiela',
    author_email='adam.kubiela@fuw.edu.pl',
    packages=['treco', 'treco.sim', 'treco.scanProcessors', 'treco.utils', 'treco.postprocessing'],
    description='Framework for OTPC track reconstruction',
    python_requires='>=3.9.7',
    install_requires=[
        'numpy >= 1.20.1',
        'matplotlib >= 3.3.4',
        'xarray >= 0.19.0',
        'scipy >= 1.6.1',
        'tqdm > 4.56.0',
        'imageio >= 2.9.0',
        'opencv-python-headless >= 4.7.0',
    ],
)
