import numpy as np
import xarray as xr
from itertools import product

from .common import processSubparametersImage, processSubparametersWfm


def processThetaPoint(iSim, expIm,
                      thetas: list[float],
                      cpList):
    """
    Calculate image dzeta function for single energy/phi/theta combination
    and best fitting subparameters x0,y0,sigmaC & scaleC.

    Arguments:
        thetas:      theta angle values in degrees for each particle.
                     Particle number is infered from the size of this tuple.
        cpList:      lists of common parameters for scanning. Must include
                     at least lists of x0, y0, sigmaC, scaleC.
        iSim:        ImageSimulator instance with n Track object attached.
                     n must match thetas list size.
                     Warning: proper e0/phi [imL/wfmL] values must be set
                     before executing this function.
        expIm:       image to be compared with simulation
    Returns:
        dzetaBest:      value of dzeta function for best fitting subparameters
        paramsBest:     an array containing subparameter values that give
                        lowest dzeta value. Slots for subparameters
                        not concerning image (z0, sigmaP, scaleP) are
                        filled with NaNs.
        loc:            xarray-compatible dictionary describing location
                        in dataset (theta-coordinates only)
        pc:             number of parameter combinations processed.


    """
    loc = {}
    for i, theta in enumerate(thetas):
        iSim.tracks[i].theta = theta
        loc['theta{}'.format(i)] = theta

    dzetaBest, paramsBest, pc = processSubparametersImage(iSim, expIm, cpList)
    return dzetaBest, paramsBest, loc, pc

def processImageLenlen(iSim, expIm, coords, thetasDic, cpList):

    particleCount = len(coords.keys()) // 3
    # make a sub-dataset
    dims = []
    for i in range(particleCount):
        dims.append(len(thetasDic[f'theta{i}']))
    dzetasDA = xr.DataArray(np.full(dims, np.nan, dtype=np.float32),
                           coords=coords | thetasDic,
                           dims=thetasDic)
    subparams = {'subparam': ['x0', 'y0', 'sigmaC', 'scaleC']}
    bestParamsDA = xr.DataArray(np.full(dims + [4], np.nan, dtype=np.float32),
                                coords=coords | thetasDic | subparams,
                                dims=thetasDic | subparams)
    subds = xr.Dataset({'dzetasC': dzetasDA, 'bestSubparams': bestParamsDA})

    # set track lengths and get theta lists for each particle
    for i in range(particleCount):
        iSim.tracks[i].setLengths(coords[f'imL{i}'],
                                  coords[f'wfmL{i}'],
                                  coords[f'phiSgn{i}'] > 0)

    # process every combination of theta angles from lists
    processedCount = 0
    for thetaComb in product(*[thList for thList in thetasDic.values()]):
        dzeta, bestParams, loc, pc = processThetaPoint(
            iSim, expIm, thetaComb, cpList)
        subds.dzetasC.loc[loc] = 1000 * dzeta
        loc = loc | {'subparam': ['x0', 'y0', 'sigmaC', 'scaleC']}
        subds['bestSubparams'].loc[loc] = bestParams[[0, 1, 3, 5]]
        processedCount += pc
    return subds, processedCount


# def processImageEphi(iSim, expIm, coords, thetasDic, cpList):

#     particleCount = len(coords.keys()) // 2

#     # make a sub-dataset
#     dims = []
#     for i in range(particleCount):
#         dims.append(len(thetasDic[f'theta{i}']))
#     dztasDA = xr.DataArray(np.full(dims, np.nan, dtype=np.float32),
#                            coords=coords | thetasDic,
#                            dims=thetasDic)
#     bestParamsDA = xr.DataArray(np.full(dims + [4], np.nan, dtype=np.float32),
#                                 coords=coords | thetasDic,
#                                 dims=thetasDic)
#     subds = xr.Dataset({'dzetasC': dzetasDA, 'bestSubparams': bestParamsDA})

#     # set track lengths and get theta lists for each particle
#     for i in range(particleCount):
#         iSim.tracks[i].setParameters(coords[f'e0{i}'],
#                                      coords[f'phi{i}'])

#     # process every combination of theta angles from lists
#     processedCount = 0
#     for thetaComb in product(*[thList for thList in thetasDic.values()]):
#         dzeta, bestParams, loc, pc = processThetaPoint(
#             iSim, expIm, thetaComb, cpList)
#         subds.dzetasC.loc[loc] = 1000 * dzeta
#         loc = loc | {'subparam': ['x0', 'y0', 'sigmaC', 'scaleC']}
#         subds['bestSubparams'].loc[loc] = paramsBest[[0, 1, 3, 5]]
#         subds.bestParams.loc[loc] = bestParams
#         processedCount += pc
#     return subds, processedCount


def processWfmEphi(wSim, expWfm, coords, cpList):

    particleCount = len(coords.keys()) // 2
    for i in range(particleCount):
        wSim.tracks[i].setParameters(coords[f'e0{i}'],
                                     coords[f'phi{i}'])
    return (*processSubparametersWfm(wSim, expWfm, cpList), coords)

def processWfmLenlen(wSim, expWfm, coords, cpList):
    particleCount = len(coords.keys()) // 3
    for i in range(particleCount):
        wSim.tracks[i].setLengths(coords[f'imL{i}'],
                                  coords[f'wfmL{i}'],
                                  coords[f'phiSgn{i}'] > 0)
    return (*processSubparametersWfm(wSim, expWfm, cpList), coords)
