import numpy as np
import matplotlib.pyplot as plt


def processSubparametersImage2(iSim, expImage, cpList): # scales from the list
    """
    Scan image through all given subparameter combinations (x0,y0,sigma,scale)
    and return best dzeta function value and best subparameter combination.
    Arguments:
        iSim:     ImageSimulator instance to be compared
                  with experimental event.
        expImage: experimental image.
        cpList:   subparameter lists to iterate over.
    Returns:
        dzetaBest:  lowest value of dzeta function found for given
                    subparameter values
        paramsBest: best set of subparameters.
        procCount:  number of processed combinations
    """
    dzetaBest = np.inf
    for sigmaC in cpList.sigmaC:
        iSim.sigmaC = sigmaC
        for x0 in cpList.x0:
            iSim.x0 = x0
            for y0 in cpList.y0:
                iSim.y0 = y0
                for scaleC in cpList.scaleC:
                    iSim.scaleC = scaleC
                    # calculate dzeta value
                    dzetaIm = np.sum((iSim.simImage - expImage.image)**2)
                    if dzetaIm < dzetaBest:
                        dzetaBest = dzetaIm
                        paramsBest = np.array(
                            [x0, y0, np.nan, sigmaC,
                             np.nan, scaleC, np.nan])
    procCount = len(cpList.x0) * len(cpList.y0) * \
        len(cpList.sigmaC) * len(cpList.scaleC)
    return dzetaBest, paramsBest, procCount


def processSubparametersWfm2(wSim, expWfm, cpList): # scales from the list
    """
    Scan waveform through all given subparameter combinations (z0,sigma,scale)
    and return best dzeta function value and best subparameter combination.
    Arguments:
        wSim:     WaveformSimulator instance to be compared
                  with experimental event.
        expWfm:   experimental waveform.
        cpList:   subparameter lists to iterate over.
    Returns:
        dzetaBest:  lowest value of dzeta function found for given
                    subparameter values
        paramsBest: best set of subparameters.
        procCount:  number of processed combinations
    """
    dzetaBest = np.inf
    for sigmaP in cpList.sigmaP:
        wSim.sigmaW = sigmaP
        for z0 in cpList.z0:
            wSim.z0 = z0
            for scaleP in cpList.scaleP:
                wSim.scaleW = scaleP

                # calculate dzeta value
                dzetaWfm = np.sum((wSim.simWfm - expWfm.wfm)**2)
                if dzetaWfm < dzetaBest:
                    dzetaBest = dzetaWfm
                    paramsBest = np.array(
                        [np.nan, np.nan, z0, np.nan,
                         sigmaP, np.nan, scaleP])
    processedCount = len(cpList.z0) * len(cpList.sigmaP) * len(cpList.scaleP)
    return (dzetaBest, paramsBest, processedCount)

# original
def processSubparametersImage(iSim, expImage, cpList): # scales fit analytically
    """
    Scan image through all given subparameter combinations (x0,y0,sigma)
    and return best dzeta function value and best subparameter combination.
    Best scale for each point is calculated analytically with linear LSq method.
    Arguments:
        iSim:     ImageSimulator instance to be compared
                  with experimental event.
        expImage: experimental image.
        cpList:   subparameter lists to iterate over.
    Returns:
        dzetaBest:  lowest value of dzeta function found for given
                    subparameter values
        paramsBest: best set of subparameters.
        procCount:  number of processed combinations
    """
    dzetaBest = np.inf
    for sigmaC in cpList.sigmaC:
        iSim.sigmaC = sigmaC
        for x0 in cpList.x0:
            iSim.x0 = x0
            for y0 in cpList.y0:
                iSim.y0 = y0
                bestScale = np.sum(expImage.image * iSim.simImage) / np.sum(iSim.simImage**2)
                # calculate dzeta value
                dzetaIm = np.sum((iSim.simImage * bestScale - expImage.image)**2)
                if dzetaIm < dzetaBest:
                    dzetaBest = dzetaIm
                    paramsBest = np.array(
                        [x0, y0, np.nan, sigmaC,
                         np.nan, bestScale, np.nan])
    procCount = len(cpList.x0) * len(cpList.y0) * \
        len(cpList.sigmaC)
    return dzetaBest, paramsBest, procCount

# original
def processSubparametersWfm(wSim, expWfm, cpList): # scales fit analytically
    """
    Scan waveform through all given subparameter combinations (z0,sigma)
    and return best dzeta function value and best subparameter combination.
    Best scale for each point is calculated analytically with linear LSq method.
    Arguments:
        wSim:     WaveformSimulator instance to be compared
                  with experimental event.
        expWfm:   experimental waveform.
        cpList:   subparameter lists to iterate over.
    Returns:
        dzetaBest:  lowest value of dzeta function found for given
                    subparameter values
        paramsBest: best set of subparameters.
        procCount:  number of processed combinations
    """
    dzetaBest = np.inf
    for sigmaP in cpList.sigmaP:
        wSim.sigmaW = sigmaP
        for z0 in cpList.z0:
            wSim.z0 = z0
            bestScale = np.sum(expWfm.wfm * wSim.simWfm) / np.sum(wSim.simWfm**2)
            # calculate dzeta value
            dzetaWfm = np.sum((wSim.simWfm * bestScale - expWfm.wfm)**2)
            if dzetaWfm < dzetaBest:
                dzetaBest = dzetaWfm
                paramsBest = np.array(
                    [np.nan, np.nan, z0, np.nan,
                     sigmaP, np.nan, bestScale])
    processedCount = len(cpList.z0) * len(cpList.sigmaP)
    return (dzetaBest, paramsBest, processedCount)

# temporary with poisson errors
def processSubparametersImagePoisson(iSim, expImage, cpList):
    """
    Scan image through all given subparameter combinations (x0,y0,sigma)
    and return best dzeta function value and best subparameter combination.
    Best scale for each point is calculated analytically with linear LSq method.
    Arguments:
        iSim:     ImageSimulator instance to be compared
                  with experimental event.
        expImage: experimental image.
        cpList:   subparameter lists to iterate over.
    Returns:
        dzetaBest:  lowest value of dzeta function found for given
                    subparameter values
        paramsBest: best set of subparameters.
        procCount:  number of processed combinations
    """
    dzetaBest = np.inf
    for sigmaC in cpList.sigmaC:
        iSim.sigmaC = sigmaC
        for x0 in cpList.x0:
            iSim.x0 = x0
            for y0 in cpList.y0:
                iSim.y0 = y0
                bestScale = np.sum(expImage.image * iSim.simImage) / np.sum(iSim.simImage**2)
                # calculate dzeta value
                dzetaIm = np.sum((iSim.simImage * bestScale - expImage.image)**2 / (expImage.image + 1))
                if dzetaIm < dzetaBest:
                    dzetaBest = dzetaIm
                    paramsBest = np.array(
                        [x0, y0, np.nan, sigmaC,
                         np.nan, bestScale, np.nan])
    procCount = len(cpList.x0) * len(cpList.y0) * \
        len(cpList.sigmaC)
    return dzetaBest, paramsBest, procCount

# temporary with poisson errors
def processSubparametersWfmPoisson(wSim, expWfm, cpList):
    """
    Scan waveform through all given subparameter combinations (z0,sigma)
    and return best dzeta function value and best subparameter combination.
    Best scale for each point is calculated analytically with linear LSq method.
    Arguments:
        wSim:     WaveformSimulator instance to be compared
                  with experimental event.
        expWfm:   experimental waveform.
        cpList:   subparameter lists to iterate over.
    Returns:
        dzetaBest:  lowest value of dzeta function found for given
                    subparameter values
        paramsBest: best set of subparameters.
        procCount:  number of processed combinations
    """
    dzetaBest = np.inf
    for sigmaP in cpList.sigmaP:
        wSim.sigmaW = sigmaP
        for z0 in cpList.z0:
            wSim.z0 = z0
            bestScale = np.sum(expWfm.wfm * wSim.simWfm) / np.sum(wSim.simWfm**2)
            # calculate dzeta value
            dzetaWfm = np.sum((wSim.simWfm * bestScale - expWfm.wfm)**2 / abs(expWfm.wfm + 1))
            if dzetaWfm < dzetaBest:
                dzetaBest = dzetaWfm
                paramsBest = np.array(
                    [np.nan, np.nan, z0, np.nan,
                     sigmaP, np.nan, bestScale])
    processedCount = len(cpList.z0) * len(cpList.sigmaP)
    return (dzetaBest, paramsBest, processedCount)