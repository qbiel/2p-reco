import numpy as np
from .common import processSubparametersImage, processSubparametersWfm
from .singleTrack import processThetaPoint


def processImageEphi(iSim,
                     expImage,
                     e0Pair: list[int],
                     phiPair: list[float],
                     thetaListPair,
                     cpList):
    """
    Calculate image dzeta function values for single
    energies/phis combination, all theta values from the lists
    and best subparameters (x0,y0,sigma,scale).



    Arguments:
        iSim:        ImageSimulator instance with Track object[s] attached.
        expImage:    original image to be compared with simulation
        e0:          pair or particle energies in keV.
        phi:         pair of vertical angles in degrees.
        thetaList:   pair of lists of horizontal angle values to be scanned.
        cpList:      lists of common parameters for scanning. Must include
                     at least lists of x0, y0, sigmaC, scaleC.
    Returns:
        tuple (dzetas, paramsBestArr, processedCount):
            dzetas:         2D array with lowest dzeta values for given
                            energy/phi and each theta combination.
            paramsBest:     3D array with best subparameter values for
                            given e0/phi and each theta angles in thetaList.
                            Slots for subparameters not concerning image
                            (z0, sigmaP, scaleP) are filled with NaNs.
            processedCount: number of parameter combinations processed.
            e0Pair, phiPair,
            thetaListPair:  arguments passed to function
    """
    # set energies/phis
    for idx, track in enumerate(iSim.tracks):
        track.setParameters(e0=e0Pair[idx], phi=phiPair[idx])

    # make arrays for results
    dzetas = np.zeros((len(thetaListPair[0]), len(thetaListPair[1])))
    paramsBestArr = np.zeros((*dzetas.shape, 7))

    # process every combination of (theta1, theta2)
    processedCount = 0
    for i, theta1 in enumerate(thetaListPair[0]):
        iSim.tracks[0].theta = theta1
        for j, theta2 in enumerate(thetaListPair[1]):
            dzetas[i, j],\
                paramsBestArr[i, j],\
                pc = processThetaPoint(iSim, expImage,
                                       theta2, cpList,
                                       trackIdx=1)
            processedCount += pc
    return (dzetas, paramsBestArr, processedCount,
            e0Pair, phiPair, thetaListPair)


def processImageLenlen(iSim,
                       expImage,
                       imLPair, wfmLPair,
                       phiSgnPair,
                       thetaListPair,
                       cpList):
    """
    Calculate image dzeta function values for single
    image length/wfm length combination, all theta values from the list
    and best subparameters (x0,y0,sigma,scale).

    Arguments:
        iSim:        ImageSimulator instance with Track object[s] attached.
        expImage:    original image to be compared with simulation
        e0, phi:     particle initial energy & vertical angle values.
        thetaList:   list of horizontal angle values to be scanned.
        cpList:      lists of common parameters for scanning. Must include
                     at least lists of x0, y0, sigmaC, scaleC.
    Returns:
        tuple (dzetas, paramsBestArr, processedCount):
            dzetas:         1D array with lowest dzeta values for given
                            energy/phi and each theta value from thetaList.
            paramsBest:     2D array with best subparameter values for
                            given e0/phi and each theta angles in thetaList.
                            Slots for subparameters not concerning image
                            (z0, sigmaP, scaleP) are filled with NaNs.
            processedCount: number of parameter combinations processed.
    """

    # set energies/phis
    for idx, track in enumerate(iSim.tracks):
        track.setLengths(imLPair[idx], wfmLPair[idx], phiSgnPair[idx] > 0)

    processedCount = 0
    # make arrays for results
    dzetas = np.zeros((len(thetaListPair[0]), len(thetaListPair[1])))
    paramsBestArr = np.zeros((*dzetas.shape, 7))

    # process every combination of (theta1, theta2)
    processedCount = 0
    for i, theta1 in enumerate(thetaListPair[0]):
        iSim.tracks[0].theta = theta1
        for j, theta2 in enumerate(thetaListPair[1]):
            dzetas[i, j], paramsBestArr[i, j],\
                pc = processThetaPoint(iSim, expImage,
                                       theta2, cpList,
                                       trackIdx=1)
            processedCount += pc
    return (dzetas, paramsBestArr, processedCount,
            imLPair, wfmLPair, phiSgnPair, thetaListPair)


def processWfmEphi(wSim,
                   expWfm,
                   e0Pair,
                   phiPair,
                   cpList):
    """
    Calculate wfm dzeta function value for single energy/phi combination,
    and best subparameters (z0,sigmaP,scaleP).

    Arguments:
        wSim:     WaveformSimulator instance with Track object[s] attached.
        expWfm:    original waveform to be compared with simulation
        e0, phi:  particle initial energy & vertical angle values.
        cpList:      lists of common parameters for scanning. Must include
                     at least lists of z0, sigmaP, scaleP.
        trackIndex:  index of a track that will have given theta value
                     applied. Defaults to 0 for single-track scanning.

    Returns:
        tuple (dzeta, paramsBestArr, processedCount):
            dzetas:         value of dzeta function for given e0/phi
                            and best fitting subparameters.
            paramsBest:     1D array with best subparameter values for
                            given e0/phi.
                            Slots for subparameters not concerning wfm
                            (x0&y0, sigmaC, scaleC) are filled with NaNs.
            processedCount: number of parameter combinations processed.
    """
    for idx, track in enumerate(wSim.tracks):
        track.setParameters(e0=e0Pair[idx], phi=phiPair[idx])
    return (*processSubparametersWfm(wSim, expWfm, cpList), e0Pair, phiPair)


def processWfmLenlen(wSim,
                     expWfm,
                     imLPair, wfmLPair, phiSgnPair,
                     cpList):
    """
    Calculate wfm dzeta function value for single imLen/wfmLen/phiSgn
    combination and best subparameters (z0,sigmaP,scaleP).

    Arguments:
        wSim:     WaveformSimulator instance with Track object[s] attached.
        expWfm:    original waveform to be compared with simulation
        imL, wfmL:  particle track projection lengths on image and wfm
        phiSgn:     sign of vertical angle (True=up, False=down)
        cpList:      lists of common parameters for scanning. Must include
                     at least lists of z0, sigmaP, scaleP.
        trackIndex:  index of a track that will have given theta value
                     applied. Defaults to 0 for single-track scanning.

    Returns:
        tuple (dzeta, paramsBestArr, pc, imL, wfmL, phiSgn):
            dzetas:         value of dzeta function for given e0/phi
                            and best fitting subparameters.
            paramsBest:     1D array with best subparameter values for
                            given e0/phi.
                            Slots for subparameters not concerning wfm
                            (x0&y0, sigmaC, scaleC) are filled with NaNs.
            processedCount: number of parameter combinations processed.
    """
    for idx, track in enumerate(wSim.tracks):
        track.setLengths(imLPair[idx], wfmLPair[idx], phiSgnPair[idx] > 0)
    return (*processSubparametersWfm(wSim, expWfm, cpList),
            imLPair, wfmLPair, phiSgnPair)
