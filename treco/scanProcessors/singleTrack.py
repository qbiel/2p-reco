import numpy as np
from .common import processSubparametersImage, processSubparametersWfm


def processThetaPoint(iSim,
                      expImage,
                      theta: float,
                      cpList,
                      trackIdx: int = 0):
    """
    Calculate image dzeta function for single energy/phi/theta combination
    and best fitting subparameters x0,y0,sigmaC & scaleC.

    Arguments:
        iSim:        ImageSimulator instance with Track object[s] attached.
                     Warning: proper e0/phi (imL/wfmL) values must be set
                     before executing this function.
        expImage:    original image to be compared with simulation
        theta:       theta angle value in degrees.
        cpList:      lists of common parameters for scanning. Must include
                     at least lists of x0, y0, sigmaC, scaleC.
        trackIndex:  index of a track that will have given theta value
                     applied. Defaults to 0 for single-track scanning.
    Returns:
        dzetaBest:      value of dzeta function for best fitting subparameters
        paramsBest:     an array containing subparameter values that give
                        lowest dzeta value. Slots for subparameters
                        not concerning image (z0, sigmaP, scaleP) are
                        filled with NaNs.
        processedCount: number of parameter combinations processed.

    """
    iSim.tracks[trackIdx].theta = theta
    return processSubparametersImage(iSim, expImage, cpList)


def processImageEphi(iSim,
                     expImage,
                     e0, phi,
                     thetaList,
                     cpList,
                     trackIdx: int = 0):
    """
    Calculate image dzeta function values for single
    energy/phi combination, all theta values from the list
    and best subparameters (x0,y0,sigma,scale).

    Arguments:
        iSim:        ImageSimulator instance with Track object[s] attached.
        expImage:    original image to be compared with simulation
        e0, phi:     particle initial energy & vertical angle values.
        thetaList:   list of horizontal angle values to be scanned.
        cpList:      lists of common parameters for scanning. Must include
                     at least lists of x0, y0, sigmaC, scaleC.
        trackIndex:  index of a track that will have given theta value
                     applied. Defaults to 0 for single-track scanning.
    Returns:
        tuple (dzetas, paramsBestArr, processedCount):
            dzetas:         1D array with lowest dzeta values for given
                            energy/phi and each theta value from thetaList.
            paramsBest:     2D array with best subparameter values for
                            given e0/phi and each theta angles in thetaList.
                            Slots for subparameters not concerning image
                            (z0, sigmaP, scaleP) are filled with NaNs.
            processedCount: number of parameter combinations processed.
    """
    processedCount = 0
    iSim.tracks[trackIdx].setParameters(e0=e0, phi=phi)
    dzetas = np.zeros(len(thetaList))
    paramsBestArr = np.zeros((len(thetaList), 7))
    for j, theta in enumerate(thetaList):
        dzetas[j],\
            paramsBestArr[j],\
            pc = processThetaPoint(iSim, expImage,
                                   theta, cpList,
                                   trackIdx)
        processedCount += pc
    return (dzetas, paramsBestArr, processedCount, e0, phi, thetaList)


def processImageLenlen(iSim,
                       expImage,
                       imL, wfmL, phiSgn,
                       thetaList,
                       cpList,
                       trackIdx: int = 0):
    """
    Calculate image dzeta function values for single
    image length/wfm length combination, all theta values from the list
    and best subparameters (x0,y0,sigma,scale).

    Arguments:
        iSim:        ImageSimulator instance with Track object[s] attached.
        expImage:    original image to be compared with simulation
        e0, phi:     particle initial energy & vertical angle values.
        thetaList:   list of horizontal angle values to be scanned.
        cpList:      lists of common parameters for scanning. Must include
                     at least lists of x0, y0, sigmaC, scaleC.
        trackIndex:  index of a track that will have given theta value
                     applied. Defaults to 0 for single-track scanning.
    Returns:
        tuple (dzetas, paramsBestArr, processedCount):
            dzetas:         1D array with lowest dzeta values for given
                            energy/phi and each theta value from thetaList.
            paramsBest:     2D array with best subparameter values for
                            given e0/phi and each theta angles in thetaList.
                            Slots for subparameters not concerning image
                            (z0, sigmaP, scaleP) are filled with NaNs.
            processedCount: number of parameter combinations processed.
    """
    processedCount = 0
    iSim.tracks[trackIdx].setLengths(imL, wfmL, phiSgn > 0)
    dzetas = np.zeros(len(thetaList))
    paramsBestArr = np.zeros((len(thetaList), 7))
    for j, theta in enumerate(thetaList):
        dzetas[j], paramsBestArr[j], pc = processThetaPoint(iSim, expImage,
                                                            theta, cpList,
                                                            trackIdx)
        processedCount += pc
    return (dzetas, paramsBestArr, processedCount,
            imL, wfmL, phiSgn, thetaList)


def processWfmEphi(wSim,
                   expWfm,
                   e0, phi, cpList,
                   trackIdx: int = 0):
    """
    Calculate wfm dzeta function value for single energy/phi combination,
    and best subparameters (z0,sigmaP,scaleP).

    Arguments:
        wSim:     WaveformSimulator instance with Track object[s] attached.
        expWfm:    original waveform to be compared with simulation
        e0, phi:  particle initial energy & vertical angle values.
        cpList:      lists of common parameters for scanning. Must include
                     at least lists of z0, sigmaP, scaleP.
        trackIndex:  index of a track that will have given theta value
                     applied. Defaults to 0 for single-track scanning.

    Returns:
        tuple (dzeta, paramsBestArr, processedCount):
            dzetas:         value of dzeta function for given e0/phi
                            and best fitting subparameters.
            paramsBest:     1D array with best subparameter values for
                            given e0/phi.
                            Slots for subparameters not concerning wfm
                            (x0&y0, sigmaC, scaleC) are filled with NaNs.
            processedCount: number of parameter combinations processed.
    """
    wSim.tracks[trackIdx].setParameters(e0=e0, phi=phi)
    return (*processSubparametersWfm(wSim, expWfm, cpList), e0, phi)


def processWfmLenlen(wSim,
                     expWfm,
                     imL, wfmL, phiSgn,
                     cpList,
                     trackIdx: int = 0):
    """
    Calculate wfm dzeta function value for single imLen/wfmLen/phiSgn
    combination and best subparameters (z0,sigmaP,scaleP).

    Arguments:
        wSim:     WaveformSimulator instance with Track object[s] attached.
        expWfm:    original waveform to be compared with simulation
        imL, wfmL:  particle track projection lengths on image and wfm
        phiSgn:     sign of vertical angle (True=up, False=down)
        cpList:      lists of common parameters for scanning. Must include
                     at least lists of z0, sigmaP, scaleP.
        trackIndex:  index of a track that will have given theta value
                     applied. Defaults to 0 for single-track scanning.

    Returns:
        tuple (dzeta, paramsBestArr, pc, imL, wfmL, phiSgn):
            dzetas:         value of dzeta function for given e0/phi
                            and best fitting subparameters.
            paramsBest:     1D array with best subparameter values for
                            given e0/phi.
                            Slots for subparameters not concerning wfm
                            (x0&y0, sigmaC, scaleC) are filled with NaNs.
            processedCount: number of parameter combinations processed.
    """
    wSim.tracks[trackIdx].setLengths(imL, wfmL, phiSgn > 0)
    return (*processSubparametersWfm(wSim, expWfm, cpList),
            imL, wfmL, phiSgn)
