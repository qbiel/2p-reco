# basic constants
driftVel = 19600  # um/us
pixelWidth = 660  # um/px
maxImageSize = 512

# directory with experiment runs
runDirectory = '/home/adam/rikendata'

# track definitions - key is an alias used by *treco.Track*, value is a path to SRIM file
ptypes = {
    'proton': '/home/adam/cernbox/Documents/zn54/treco/srimTables/srimOutH.txt',
    'alpha': '/home/adam/cernbox/Documents/zn54/treco/srimTables/srimOutHe.txt',
    'zinc54':'/home/adam/cernbox/Documents/zn54/treco/srimTables/srimOutZn.txt'
}

# default parameters for track
e0 = 1000 # keV
theta = 20 # deg
phi = -45 # deg
nuclCorr = False # not implemented yet

# default parameters for image
imSize = (128,128) # (y,x)?
x0 = imSize[1] // 2 # pixels
y0 = imSize[0] // 2 # pixels
binning = 1
sigmaC = 1 # pixels
scaleC = 1

# default parameters for waveform
wfmSize = 2000 # samples
sampling = 100 # MSa/s
wfmStart =  - (wfmSize // 2) / sampling # us
z0 = 0 # us
sigmaW = 3 # samples
scaleW = 1

massTable = {
    '1h' : 938272,
    '54zn' : 5.0294114e7,
    '54ni' : 5.03006815e7 - 39278, 
    '53Co' : 4.93691874e7 - 42659.4,
}