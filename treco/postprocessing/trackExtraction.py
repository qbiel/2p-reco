import numpy as np
import scipy.ndimage as ndim
from scipy.stats import norm
from SmartLoad import SmartLoad

from scipy.ndimage import gaussian_filter1d


def getNoiseBg(arr, threshold=None):
    """
    Get information on gaussian noise on image/wfm.

    This function creates a histogram of pixel/sample values and calculates mean and standard deviation
    of those values. A threshold can be set to ignore too bright pixels/samples.

    Arguments:
        arr: input data array, e.g. an image 2D array or 1D wfm array.
        threshold: maximum pixel/sample brightness. Pixels/samples brigher than threshold are ignored.

    Returns:
        mu: mean pixel/sample intensity (background)
        sigma: standard deviation of pixel/sample intensity

    """
    if threshold is None:
        mu, sigma = norm.fit(arr)
    else:
        maskedInput = np.ma.masked_array(arr, arr < threshold)
        mu, sigma = norm.fit(maskedInput[maskedInput.mask])
    return mu, sigma


def cropImageToChamber(image, leftMargin: int = 82, rightMargin: int = 400):
    """
    Crop image to remove diodes and areas outside the chamber.

    """

    return image[:, leftMargin:rightMargin]


def cropImageToTrack(image, margin=4):
    """
    Remove empty areas from image leaving only the portion containing a track
    """
    nonzeros = np.nonzero(image)
    ymin = np.amin(nonzeros[0])
    ymax = np.amax(nonzeros[0])
    xmin = np.amin(nonzeros[1])
    xmax = np.amax(nonzeros[1])
    return image[ymin - margin:ymax + margin + 1,
                 xmin - margin:xmax + margin + 1]


def removeImageBackground(image,
                          pixelThreshold: int = 2000,
                          median: int = 3,
                          sigma: float = 2,
                          gaussianThreshold: int = 1800):
    """
    Remove all background from an image leaving only tracks.

    This function calculates mean background, subtrats it, then sets pixel values to 0 leaving only pixels brighter
    than pixelThreshold parameter. Median filter and gaussian filter are additionally used to limit additional noise
    before brightness check. Output image, though, is not affected by filters

    Arguments:
        image: input image
        pixelThreshold: Minimum brightness of the pixel to be considered part of the track
        median: size of median filter
        sigma: sigma of gaussian filter
        gaussianThreshold: maximum pixel brightness to considered gaussian noise
    Returns:
        trackImage: image of the same size as input with background removed.
        noiseSigma: standard deviation of gaussian noise in input image
    """
    trackImage = image.copy()
    mu, noiseSigma = getNoiseBg(image, gaussianThreshold)
    trackImage[trackImage > int(mu)] -= int(mu)
    trackImage[trackImage <= int(mu)] = 0
    filteredIm = ndim.median_filter(trackImage, median)
    filteredIm = ndim.gaussian_filter(filteredIm, sigma)
    mask = filteredIm > pixelThreshold
    trackImage[~mask] = 0
    return trackImage, mu, noiseSigma


def getWfmNoiseParams(wfmObj, start, stop):

    size = stop - start
    wfmObj.loadChannel(start - size, start)
    leftSide = wfmObj.data.copy()
    wfmObj.loadChannel(stop, stop + size)
    rightSide = wfmObj.data.copy()
    borders = np.concatenate((leftSide, rightSide))
    mu, sigma = getNoiseBg(borders)
    return mu, sigma


def findPmtSignal(wfmObj: SmartLoad,
                  padding: float = 0,
                  borders: tuple[float, float] = (870, None),
                  maxSignalLen: float = 15):
    """ Find a signal in a PMT waveform.

    This function works only with SmartSave waveforms and assumes a single wfm signal is
    present (apart from triggering ion and sync LEDs).

    Arguments:
        wfmObj: SmartLoad object from a waveform file
        padding: additional padding applied to found signal
        borders: timestamps of borders of waveform area of interest in us. None for no limit.
            Left hand border should be chosen to eliminate incoming ion signal at
            around 0 us and sync LED signals at around 800 us. This means, however,
            that if a signal lies beyond specified borders, it will not be found.
            Default is (870, None).
        maxSignalLen: maximum expected length of the signal in microseconds.

    """
    wfmObj.loadChannel(*borders)
    argmin = np.argmin(wfmObj.data)
    xmax = wfmObj.x[argmin]
    ymax = wfmObj.data[argmin]
    wfmObj.loadChannel(xmax - maxSignalLen, xmax + maxSignalLen)
    xsig = wfmObj.x.copy()
    ySig = wfmObj.data.copy()
    wfmObj.loadChannel(xmax + maxSignalLen, xmax + 2 * maxSignalLen)
    lowbg = gaussian_filter1d(wfmObj.data, 20)
    stdbg = lowbg.std()
    bg = lowbg.mean()
    lowSignal = gaussian_filter1d(ySig, 20)
    thresholdStd = (bg - 5 * stdbg)  # is this necessary? thresholdAmp seems better in most situations
    thresholdAmp = bg - 0.1 * abs(ymax - bg)
    threshold = min(thresholdAmp, thresholdStd)
    mask = (lowSignal < threshold)
    if not mask.any():
        return (None, None)
    xAboveThr = xsig[mask]
    x0 = xAboveThr[0]
    x1 = xAboveThr[-1]
    return x0 - padding, x1 + padding
