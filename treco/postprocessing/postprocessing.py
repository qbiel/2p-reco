import numpy as np
from scipy.ndimage import gaussian_filter
from scipy import signal


def prepareImage(image, sigma: float, bg: int = 0, scaling: float = 0):
    """
    Apply backgound reduction, smoothing & scaling to the image.

    Arguments:
        image:   2D array of pixel values, type uint16
        sigma:   smoothing factor for gaussian filter
        bg:      background to subtract. If pixel value is lower than bg,
                 this pixel will be clipped to 0.
        scaling: scaling factor. If 0, no rescaling will be performed.
                 Otherwise, image will first be rescaled so that scaling
                 is the value of the brightest pixel.
    Returns:
        image:       input image after preparation
        scalingFact: absolute scaling factor applied to image
    """
    image = gaussian_filter(image, sigma) if sigma > 0 else image
    if bg > 0:
        image = np.clip(image, bg, np.amax(image))  # remove?
        image -= bg
    if scaling > 0:
        scalingFact = scaling / np.amax(image)
        image = image * scalingFact
    else:
        scalingFact = 1
    return image, scalingFact


def prepareWfm(wfm, sigma, bg=0, scaling=1):
    """
    Apply background reduction, smoothing & scaling to the waveform.

    Arguments:
        wfm:     1D array of waveform samples
        sigma:   smoothing factor for gaussian filter
        bg:      background value to subtract
        scaling: scaling factor. If 0, no rescaling will be performed.
                 Otherwise, wfm will first be rescaled to 1 at highest sample,
                 then multiplied by scaling.
    Returns:
        wfm:      input waveform after preparation
        scalingF: scaling factor applied to the waveform
    """
    wfm = np.copy(wfm)
    wfm = gaussian_filter(wfm, sigma, truncate=8) if sigma > 0 else wfm
    wfm = wfm - bg
    if scaling > 0:
        scalingFact = scaling / np.amax(wfm)
        wfm = wfm * scalingFact
    else:
        scalingFact = 1
    return wfm, scalingFact


def lowpass1d(inSignal, fc, fs):
    """Apply a Butterworth lowpass digital filter to the signal

    Arguments:
        inSignal: input signal
        fc: cutoff frequency of the filter in Hz
        fs: sampling rate of the signal in Sa/s

    Returns:
        np.array: filtered signal
    """
    w = fc / (fs / 2)  # Normalize the frequency
    b, a = signal.butter(5, w, 'low')
    return signal.filtfilt(b, a, inSignal)


def highpass1d(inSignal, fc, fs):
    """Apply a Butterworth highpass digital filter to the signal

    Arguments:
        inSignal: input signal
        fc: cutoff frequency of the filter in Hz
        fs: sampling rate of the signal in Sa/s

    Returns:
        np.array: filtered signal
    """
    w = fc / (fs / 2)  # Normalize the frequency
    b, a = signal.butter(5, w, 'high')
    return signal.filtfilt(b, a, inSignal)
