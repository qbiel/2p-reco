import numpy as np
import math

from .postprocessing import prepareImage, prepareWfm
from . import constants as const


class ImageSimulator:
    """
    Class creating models of single- or multiparticle images that
    can be directly compared to data events.
    """

    def __init__(self, x0: int = None, y0: int = None,
                 imSize: tuple = const.imSize,
                 binning: int = const.binning,
                 sigma: float = const.sigmaC,
                 scale: float = const.scaleC,
                 pixelSize: int = const.pixelWidth,
                 simulateProjections: bool = True):
        self._x0 = x0 if x0 is not None else imSize[1] // 2
        self._y0 = y0 if y0 is not None else imSize[0] // 2
        self._imSize = imSize
        self._binning = binning
        self._sigmaC = sigma
        self._scaleC = scale
        self._pixelSize = pixelSize

        self._fullImage = np.zeros((max(self.imSize) * 3, max(self.imSize) * 3))
        self._fullImageScaled = np.zeros((max(self.imSize) * 3, max(self.imSize) * 3))
        self._simImage = np.zeros(self.imSize)
        self.horizProj = np.zeros(self.imSize[1])
        self.vertProj = np.zeros(self.imSize[0])
        self.tracks = []
        self._listening = False
        self.simulateProjections = simulateProjections

    def addTrack(self, trackObj):
        """
        Add a track to the model.

            Arguments:
                trackObj: *treco.Track* object

        """
        trackObj._connectViewer(self)
        self.tracks.append(trackObj)
        self._listening = True
        trackObj.imageLoss = trackObj.makeImageLoss(self.pixelSize, self.binning)
        trackObj.image = trackObj.makeImage(trackObj.imageLoss,
                                            self.imSize,
                                            padded=True)[0]
        self.updateImageSimulation()

    def removeTrack(self, trackObj):
        """
        Remove a track from the model.

            Arguments:
                trackObj: *treco.Track* object previously added to the model
        """
        trackObj._disconnectViewer(self)
        del(trackObj.imageLoss)
        del(trackObj.image)
        idx = self.tracks.index(trackObj)
        self.tracks.pop(idx)
        if not self.tracks:
            self._listening = False
        self.updateImageSimulation()

    def _recvNotification(self, sender, message):
        if self._listening:
            if message == 'loss':
                sender.imageLoss = sender.makeImageLoss(self.pixelSize, self.binning)
                sender.image = sender.makeImage(sender.imageLoss,
                                                self.imSize,
                                                padded=True)[0]
            elif message == 'image':
                sender.image = sender.makeImage(sender.imageLoss,
                                                self.imSize,
                                                padded=True)[0]
            self.updateImageSimulation()

    def remakeImageSimulation(self):
        """
        Remake loss profiles, single track images and model.
        In short, model image is made from scratch.

        """
        for track in self.tracks:
            track.imageLoss = track.makeImageLoss(self.pixelSize, self.binning)
            track.image = track.makeImage(track.imageLoss,
                                          self.imSize,
                                          padded=True)[0]
        self.updateImageSimulation()

    def updateImageSimulation(self):
        """ Remake model from single track images """

        self._fullImage = np.zeros((max(self.imSize) * 3, max(self.imSize) * 3))
        if len(self.tracks) == 0:
            self.simImage = np.zeros(self.imSize)
            return
        for track in self.tracks:
            self._fullImage += track.image

        # apply sigma and scaling
        self._fullImage = prepareImage(self._fullImage,
                                       self._sigmaC,
                                       0, 0)[0]

        self._fullImageScaled = self._fullImage * self._scaleC
        self.simImage = self._unpadImage(self._fullImageScaled)

    def getAngleBetweenTracks(self, track1, track2):
        """
        Calculate angle between two tracks in 3D space.

        Arguments:
            track1, track2: *treco.Track* objects used in calculation

        Returns:
            angle between given tracks in degrees
        """
        dot = np.dot(track1.vector, track2.vector)
        return np.arccos(dot) * 180 / np.pi

    @property
    def simImage(self):
        """
        Model of an event image.
        """
        return self._simImage

    @simImage.setter
    def simImage(self, value):
        self._simImage = value
        if self.simulateProjections:
            self.horizProj = np.sum(self.simImage, 0)
            self.vertProj = np.sum(self.simImage, 1)

    @property
    def x0(self):
        """ X position of tracks starting point. 0 is on the left hand side """
        return self._x0

    @x0.setter
    def x0(self, value):
        self._checkX0Limits(value)
        self._x0 = value
        self.simImage = self._unpadImage(self._fullImageScaled)

    @property
    def y0(self):
        """ Y position of tracks starting point. 0 is on the upper side """
        return self._y0

    @y0.setter
    def y0(self, value):
        self._checkY0Limits(value)
        self._y0 = value
        self.simImage = self._unpadImage(self._fullImageScaled)

    @ property
    def sigmaC(self):
        """ Sigma for gaussian tracks smoothing in pixels. """
        return self._sigmaC

    @ sigmaC.setter
    def sigmaC(self, value):
        self._sigmaC = value
        self.updateImageSimulation()

    @ property
    def scaleC(self):
        """ Coefficient scaling pixel values """
        return self._scaleC

    @scaleC.setter
    def scaleC(self, value):
        self._scaleC = value
        self._fullImageScaled = self._fullImage * self._scaleC
        self.simImage = self._unpadImage(self._fullImageScaled)

    @property
    def imSize(self):
        """ Image size (y,x) in pixels """
        return self._imSize

    @imSize.setter
    def imSize(self, value):
        oldSize = self._imSize
        self._imSize = value
        try:
            self._checkX0Limits(self.x0)
            self._checkX0Limits(self.y0)
        except ValueError:
            self._imSize = oldSize
            raise ValueError(
                """Old x0, y0 {}, {} out of range of new image size {}.
                Change x0 / y0 first.""".format(self.x0, self.y0, value))
        self.remakeImageSimulation()

    @property
    def binning(self):
        """
        Pixel binning. Binning of n merges n x n pixels into one.
        Typical values are 1, 2 or 4.
        """
        return self._binning

    @binning.setter
    def binning(self, value):
        self._binning = value
        self.remakeImageSimulation()

    @property
    def pixelSize(self):
        """ Real width of a square covered by one pixel on the image (when binning is 1).
            Unit is um/pixel.
        """
        return self._pixelSize

    @pixelSize.setter
    def pixelSize(self, value):
        self._pixelSize = value
        self.remakeImageSimulation()

    def _checkX0Limits(self, value):
        """ Return ValueError if x0 out of image"""
        if value >= self.imSize[1] or value < 0:
            raise ValueError(
                'x0 value {} out of range ({}:{})'.format(
                    value, 0, self.imSize[1] - 1))

    def _checkY0Limits(self, value):
        """ Return ValueError if y0 out of image"""
        if value >= self.imSize[0] or value < 0:
            raise ValueError(
                'y0 value {} out of range ({}:{})'.format(
                    value, 0, self.imSize[0] - 1))

    def _unpadImage(self, image):
        """ Internal method used to change x0 and y0. """
        return image[image.shape[0] // 2 - self.y0: image.shape[0] // 2 - self.y0 + self.imSize[0],
                     image.shape[1] // 2 - self.x0: image.shape[1] // 2 - self.x0 + self.imSize[1]]


class WfmSimulator:
    """
    Class creating models of single- or multiparticle PMT waveforms that
    can be directly compared to data events.
    """

    def __init__(self,
                 z0: float = const.z0,
                 samplingRate: int = const.sampling,
                 wfmSize: int = const.wfmSize,
                 wfmStart: float = const.wfmStart,
                 driftVelocity: int = const.driftVel,
                 sigma: float = const.sigmaW,
                 scale: float = const.scaleW):
        self._z0 = z0
        self._sampling = samplingRate
        self._sigmaW = sigma
        self._scaleW = scale
        self._wfmSize = wfmSize
        self._driftVelocity = driftVelocity
        self._wfmStart = wfmStart
        self._wfmStop = self._getWfmStop()

        self._z0Samples = self._usToSamples(z0)
        self._fullSimWfm = np.zeros(self._wfmSize * 2)
        self._fullSimWfmScaled = np.zeros(self._wfmSize * 2)
        self._simWfm = np.zeros(self._wfmSize)
        self.tracks = []
        self._listening = False

    def addTrack(self, trackObj):
        """
        Add a track to the model.

            Arguments:
                trackObj: *treco.Track* object

        """
        trackObj._connectViewer(self)
        self.tracks.append(trackObj)
        self._listening = True
        trackObj.wfmLoss = trackObj.makeWfmLoss(self.sampling,
                                                self.driftVelocity)
        trackObj.wfm = trackObj.makeWfm(trackObj.wfmLoss,
                                        self.wfmSize,
                                        padded=True)[0]
        self.updateWfmSimulation()

    def removeTrack(self, trackObj):
        """
        Remove a track from the model.

            Arguments:
                trackObj: *treco.Track* object previously added to the model
        """
        trackObj._disconnectViewer(self)
        del(trackObj.wfmLoss)
        del(trackObj.wfm)
        idx = self.tracks.index(trackObj)
        self.tracks.pop(idx)
        if not self.tracks:
            self._listening = False
        self.updateWfmSimulation()

    def _recvNotification(self, sender, message):
        if self._listening:
            if message == 'loss':
                sender.wfmLoss = sender.makeWfmLoss(self.sampling,
                                                    self.driftVelocity)
                sender.wfm = sender.makeWfm(sender.wfmLoss,
                                            self.wfmSize,
                                            padded=True)[0]
            self.updateWfmSimulation()

    def remakeWfmSimulation(self):
        """
        Remake loss profiles, single track waveforms and a model.
        In short, model waveform is remade from scratch.
        """
        for track in self.tracks:
            track.wfmLoss = track.makeWfmLoss(self.sampling,
                                              self.driftVelocity)
            track.wfm = track.makeWfm(track.wfmLoss,
                                      self.wfmSize,
                                      padded=True)[0]
        self.updateWfmSimulation()

    def updateWfmSimulation(self):
        """ Remake model from single track waveforms. """

        self.wfmX = np.linspace(self._wfmStart,
                                self._wfmStop,
                                round((self._wfmStop - self._wfmStart) * self.sampling), endpoint=False)
        self._fullSimWfm = np.zeros(self.wfmSize * 2)
        if not self.tracks:
            self.simWfm = np.zeros(self.wfmSize)
            return
        for track in self.tracks:
            self._fullSimWfm += track.wfm

        self._fullSimWfm = prepareWfm(self._fullSimWfm,
                                      self.sigmaW,
                                      0, 0)[0]
        self._fullSimWfmScaled = self._fullSimWfm * self._scaleW
        self.simWfm = self._unpadWfm(self._fullSimWfmScaled)

    def _usToSamples(self, us):
        """Convert position on the waveform from us to samples.
        Sample 0 is in the middle of the plot
        """
        return round((us - self._wfmStart) * self.sampling) - self.wfmSize // 2

    def _getWfmStop(self):
        """
        Calculate waveform stop point from waveform start point, wfm size and sampling.
        Result is in us.
        """

        return self._wfmStart + self._wfmSize / self.sampling

    @property
    def z0(self):
        """ Position of tracks starting point in us. """
        return self._z0

    @z0.setter
    def z0(self, value):
        self._checkZ0Limits(value)
        shift = round((value - self._z0) * self.sampling)
        self._z0 = self._z0 + shift / self.sampling
        self.simWfm = self._unpadWfm(self._fullSimWfmScaled)

    @property
    def sigmaW(self):
        """ Sigma for gaussian tracks smoothing in samples. """
        return self._sigmaW

    @sigmaW.setter
    def sigmaW(self, value):
        self._sigmaW = value
        self.updateWfmSimulation()

    @property
    def scaleW(self):
        """ Coefficient scaling sample values """
        return self._scaleW

    @scaleW.setter
    def scaleW(self, value):
        self._scaleW = value
        self._fullSimWfmScaled = self._fullSimWfm * self._scaleW
        self.simWfm = self._unpadWfm(self._fullSimWfmScaled)

    @property
    def sampling(self):
        """
        Waveform sampling rate in MSa/s.

        Changing sampling rate preserves wfm start point and size,
        but changes wfm stop point.
        """
        return self._sampling

    @sampling.setter
    def sampling(self, value):
        self._sampling = value
        self._wfmStop = self._getWfmStop()
        self.remakeWfmSimulation()

    @property
    def wfmSize(self):
        """
        Waveform size in samples.

        Changing waveform size preserves its start point and sampling,
        but changes stop point.
        """
        return self._wfmSize

    @wfmSize.setter
    def wfmSize(self, value):
        self._wfmSize = value
        self._wfmStop = self._getWfmStop()
        self.remakeWfmSimulation()

    @property
    def driftVelocity(self):
        """
        Drift velocity in m/s.
        """
        return self._driftVelocity

    @driftVelocity.setter
    def driftVelocity(self, value):
        self._driftVelocity = value
        self.remakeWfmSimulation()

    @property
    def wfmStart(self):
        """
        Timestamp of the first point of the waveform in us.

        wfmStart cannot be changed directly, instead use setWfmParameters() method.
        """

        return self._wfmStart

    @property
    def simWfm(self):
        """ Model of an event waveform """
        return self._simWfm

    @simWfm.setter
    def simWfm(self, value):
        self._simWfm = value

    def setWfmParameters(self,
                         sampling: int,
                         wfmSize: int,
                         driftVelocity: int = None,
                         z0: float = None,
                         wfmStart: float = None):
        """
        Set multiple waveform parameters at once.

        Arguments:
            sampling:      wfm sampling rate in MSa/s
            wfmSize:       size of a waveform in samples
            driftVelocity: in m/s
            z0:            position of tracks starting point in us.
            wfmStart:      timestamp of the first point of the waveform in us.
        """
        oldSampling = self.sampling
        oldSize = self.wfmSize
        oldStart = self._wfmStart

        self._sampling = sampling
        self._wfmSize = wfmSize
        if wfmStart is not None:
            self._wfmStart = wfmStart
        self._wfmStop = self._getWfmStop()
        if z0 is None:
            z0 = self.z0
        try:
            self._checkZ0Limits(z0)
        except ValueError:
            msg = 'z0 value {} not in range with new settings ({}:{})'.format(
                z0, self._wfmStart, self._wfmStop)
            self._sampling = oldSampling
            self._size = oldSize
            self._wfmStart = oldStart
            self._wfmStop = self._getWfmStop()
            raise ValueError(msg)
        self._z0 = z0
        if driftVelocity:
            self._driftVelocity = driftVelocity
        self.remakeWfmSimulation()

    def _checkZ0Limits(self, value):
        """ Return ValueError if z0 out of image"""
        if value < self._wfmStart or value > self._wfmStart + self.wfmSize / self.sampling:
            raise ValueError(
                'z0 value {} our of range({}:{})'.format(value,
                                                         self._wfmStart,
                                                         self._wfmStop))

    def _unpadWfm(self, wfm):
        """ Internal method used to change z0 """
        return wfm[self.wfmSize // 2 - self._usToSamples(self.z0):
                   self.wfmSize // 2 + self.wfmSize - self._usToSamples(self.z0)]
