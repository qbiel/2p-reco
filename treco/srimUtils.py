from dataclasses import dataclass, field
import os
import subprocess
import numpy as np

"""
A set of tools for generating and handling SRIM inputs and outputs.
"""

#: densities of gases at 0degC and 1013.25 hPa in g/dm3
rho0 = {
    'he': 0.1785,
    'ar': 1.784,
    'cf4': 3.947,  # ?
    'n2': 1.251,
    'o2': 1.429,
    'co2': 1.842,
}

#: molar masses of elements in g/mol. Warning: stoichiometry not included
molarMass = {
    'h': 1.008,
    'he': 4.002602,
    'ar': 39.948,
    'c': 12.011,
    'f': 18.998403143,
    'o': 15.999,
    'n': 28.014,
    '13n': 13.0057,
    '14n': 14.00307,
    '15n': 15.00011,
    '16n': 16.00610,
    '17n': 17.00845,
    '18n': 18.014079,
    '16o': 15.994915,
    '17o': 16.999132,
    '12c': 12.000000,
    '13c': 13.003355,
    '14c': 14.003242,
    '12b': 12.014352,
    '13b': 13.017780,
    '9be': 9.0121822,
    '2h': 2.01410178,
    '3h': 3.01604928,
}

#: atomic number of common elements
Z = {
    'h': 1,
    'he': 2,
    'li': 3,
    'be': 4,
    'b': 5,
    'c': 6,
    'n': 7,
    'o': 8,
    'f': 9,
    'ne': 10,
    'ar': 18
}


@dataclass
class Component:
    """
    Class representing one gas component in a gas mixture.

    In case of complex gases, an instance of Component must be created for each element in a gas with proper
    stoichiometry. For instance, if CF4 is present in a gas mixture, one instance of Component should be created
    for carbon with stoich=CF4_stoich and one instance should be created for fluorine with stoich=CF4_stoich * 4.

    Arguments:
        z: atomic number of the element
        name: custom name for a component (usually element name)
        stoich: stoichiometry of a component in a gas mixture
        mass: molar mass of the component in g/mol
    """

    z: int
    name: str
    stoich: float
    mass: float

    def printComponentLine(self):
        """
        Return a string of component data in a format required by SR.IN file
        """

        output = str(self.z) + '\t' + '\"' + str(self.name) + \
            '     \"' + '\t' + str(self.stoich) + '\t' + str(self.mass)
        return output


@dataclass
class SrimInput:
    """
    Class handling input information necessary to create a SRIM range table.

    This class stores information on ion projectile, target and energy range
    in a simulation. Method generateSrimInput() can also generate a SR.IN type file which SR module uses
    to create a range table

    Arguments:
        ionZ: atomic number of the projectile
        ionMass: Atomic mass of the projectile
        targetDensity: density of the target in g/cm3
        targetElementsNo: number of elements in the target (length of componentList)
        componentList: list of target components represented as Component object
        emin: lowest projectile energy in table in keV
        emax: highest projectile energy in table in keV
        isTargetGas: 1 if target is a gas, 0 otherwise
        compoundCorr: compound correction factor
        stoppingUnits: integer indicating units of stopping power in output file.
            See Stopping Units below. Default is 2 (keV / micron)
        outputFilename: name of the output SRIM table file. If not given, a default name is used -
            it has a format of {ion name}in{gas1name}{gas1stoichiometry}{gas2name}...d{density}.srim.in

    Stopping Units:
        (1) - eV / Angstrom
        (2) - keV / micron
        (3) - MeV / mm
        (4) - keV / (ug/cm2)
        (5) - MeV / (mg/cm2) = keV / (ug/cm2)
        (6) - keV / (mg/cm2) = eV / (ug/cm2) = MeV / (g/cm2)
        (7) - eV / (1E15 atoms/cm2)
        (8) - L.S.S. reduced units
    """

    ionZ: int = 1
    ionMass: float = 1.008
    ionName: str = ''
    targetDensity: float = None  # in g/dm3
    targetComponents: list[Component] = field(default_factory=list)
    emin: float = 1
    emax: float = 10000
    isTargetGas: bool = True
    compoundCorr: float = 0
    stoppingUnits: int = 2
    outputFilename: str = None

    def checkIfDataValid(self):
        """
        Check if all attributes have a valid value for generating input file.
        To be implemented
        """
        pass

    def generateSrimInput(self,
                          filename: str = 'SR.IN',
                          overwrite: bool = False):
        """
        Generate an input file for the SR module.

        Arguments:
            filename: name of the input file generated.
                Note that this is NOT the same as 'outputFilename' attribute.
            overwrite: if True, overwrites an existing file with the same name.
                Otherwise raises FileExistsError
        """
        self.checkIfDataValid()
        if os.path.exists(filename) and not overwrite:
            raise FileExistsError('File {} already exists.\
            If you want to overwrite it, set "overwrite" argument to True'.format(filename))
        if not self.outputFilename:
            componentStr = ''
            for component in self.targetComponents:
                componentStr += '{}{:02d}+'.format(component.name, component.stoich)
            componentStr = componentStr[:-1]
            self.outputFilename = '{}_in_{}d{:.3f}.srim.dat'.format(self.ionName,
                                                                    componentStr,
                                                                    self.targetDensity)  # filename
        with open(filename, 'w') as file:
            file.write(
                '---Stopping/Range Input Data (Number-format: Period = Decimal Point)\r\n')
            file.write('---Output File Name\r\n')
            file.write('\"' + self.outputFilename + '\"\r\n')
            file.write('---Ion(Z), Ion Mass(u)\r\n')
            file.write(str(self.ionZ) + '\t' + str(self.ionMass) + '\r\n')
            file.write(
                '---Target Data: (Solid=0,Gas=1), Density(g/cm3), Compound Corr.\r\n')
            file.write(str(int(self.isTargetGas)) + '\t' + str(self.targetDensity / 1000) +
                       '\t' + str(self.compoundCorr) + '\r\n')
            file.write('---Number of Target Elements\r\n')
            file.write(str(len(self.targetComponents)) + '\r\n')
            file.write(
                '---Target Elements: (Z), Target name, Stoich, Target Mass(u)\r\n')
            for component in self.targetComponents:
                file.write(component.printComponentLine() + '\r\n')
            file.write('---Output Stopping Units (1-8)\r\n')
            file.write(str(self.stoppingUnits) + '\r\n')
            file.write('---Ion Energy : E-Min(keV), E-Max(keV)\r\n')
            file.write(str(self.emin) + '\t' + str(self.emax) + '\r\n')

    def makeSrimTable(self,
                      outputDir: str = '.',
                      srmoduleDir: str = ''):
        """
        Experimental - make a SRIM range table directly.

        This method requires SR Module to be present (along with dependency files) in srmoduleDir
        and wine to be installed (so Linux only).
        A SR.IN file is first created directly in srmoduleDir, then SRModule.exe is executed and
        finally an output file is moved to outputDir.

        Arguments:
            outputDir: directory where SRIM table will be saved
            srmoduleDir: path to directory containing SRModule.exe file with all its dependencies
        """

        self.generateSrimInput(filename=os.path.join(os.path.abspath(srmoduleDir), 'SR.IN'),
                               overwrite=True)
        subprocess.call('wine "{}"'.format(os.path.join(os.path.abspath(srmoduleDir), 'SRModule.exe')), shell=True)
        subprocess.call(['mv', os.path.join(srmoduleDir, self.outputFilename),
                         os.path.join(os.path.abspath(outputDir), self.outputFilename)])


class SrimOutput:
    """
    Class parsing SRIM output table.

    Arguments:
        filepath: path to SRIM output file
    """

    def __init__(self, filepath):

        with open(filepath) as f:
            lines=f.readlines()
        lineTypes=('title', 'filename', 'ionNoMass', 'density', 'targetData',
                     'targetAtomsNo', 'targetAtoms', 'stoppingUnits',
                     'labels', 'data', 'end')
        linetype=0  # title
        energyArr=[]
        dedxElecArr=[]
        dedxNuclArr=[]
        projRangeArr=[]
        longStragArr=[]
        latStragArr=[]
        self._gasPercentage={}

        for line in lines:
            if lineTypes[linetype] == 'title':
                # assuming there is an empty line after title
                if line == '\n':
                    linetype += 1
                # title += line
            elif lineTypes[linetype] == 'filename':
                # assuming there is an empty line after title
                if line == '\n':
                    linetype += 1
                else:
                    pass  # this is SR.IN always, so useless
            elif lineTypes[linetype] == 'ionNoMass':
                if line == '\n':
                    linetype += 1
                else:
                    self._ion=line.split('= ')[1].split()[0]
            elif lineTypes[linetype] == 'density':
                if 'Density' in line:
                    self._density=1000 * float(line.split('= ')[1].split()[0])
                else:
                    linetype += 1
            elif lineTypes[linetype] == 'targetData':
                if 'Target' in line and 'Composition' in line:
                    pass
                elif '---' in line:
                    pass
                elif 'Atom' in line or 'Name' in line:
                    pass
                elif '===' in line:
                    linetype += 1
                else:
                    split=line.split()
                    self._gasPercentage[split[0]]=split[2]

            elif "Ion" in line and "dE/dx" in line and "Projected" in line:
                linetype=8
            elif lineTypes[linetype] == 'labels':
                if "---" in line:
                    linetype += 1  # switch to data
            elif lineTypes[linetype] == 'data':
                if "---" in line:
                    linetype += 1  # switch to end
                    continue
                dataLine=line.split()
                energy=float(dataLine[0])  # energy in keV
                if dataLine[1] == 'MeV':
                    energy *= 1.0e3
                elif dataLine[1] == 'GeV':
                    energy *= 1.0e6
                elif dataLine[1] == 'eV':
                    energy /= 1.0e3
                dedxElec=float(dataLine[2])
                dedxNucl=float(dataLine[3])
                projRange=float(dataLine[4])  # range in um
                if dataLine[5] == 'A':
                    projRange /= 10000
                if dataLine[5] == 'mm':
                    projRange *= 1000
                if dataLine[5] == 'm':
                    projRange *= 1000000
                longStrag=float(dataLine[6])  # range in um
                if dataLine[7] == 'A':
                    longStrag /= 10000
                if dataLine[7] == 'mm':
                    longStrag *= 1000
                if dataLine[7] == 'm':
                    longStrag *= 1000000
                latStrag=float(dataLine[8])  # range in um
                if dataLine[9] == 'A':
                    latStrag /= 10000
                if dataLine[9] == 'mm':
                    latStrag *= 1000
                if dataLine[9] == 'm':
                    latStrag *= 1000000
                energyArr += [energy]
                dedxElecArr += [dedxElec]
                dedxNuclArr += [dedxNucl]
                projRangeArr += [projRange]
                longStragArr += [longStrag]
                latStragArr += [latStrag]
            elif lineTypes[linetype] == 'end':
                continue
        self._energyArr=np.array(energyArr)
        self._dedxElecArr=np.array(dedxElecArr)
        self._dedxNuclArr=np.array(dedxNuclArr)
        self._rangeArr=np.array(projRangeArr)
        self._longStragArr=np.array(longStragArr)
        self._latStragArr=np.array(latStragArr)

        self.filepath=filepath

    def __repr__(self):
        return f'SrimOutput({os.path.abspath(self.filepath)})'

    @ property
    def gasPercentage(self):
        """Dictionary of gas composition"""
        return self._gasPercentage

    @ property
    def ion(self):
        """ Name of projectile ion"""
        return self._ion

    @ property
    def density(self):
        """Gas density in g/dm3"""
        return self._density

    @ property
    def energyArr(self):
        """Numpy array with energy values in keV"""
        return self._energyArr

    @ property
    def rangeArr(self):
        """Numpy array with ion ranges in um"""
        return self._rangeArr

    @ property
    def dedxElecArr(self):
        """Numpy array with ionization energy loss in keV/um"""
        return self._dedxElecArr

    @ property
    def dedxNuclArr(self):
        """Numpy array with nuclear energy loss in keV/um"""
        return self._dedxElecArr

    @ property
    def longStragArr(self):
        """Numpy array with longitudal straggling in um"""
        return self._longStragArr

    @ property
    def latStragArr(self):
        """Numpy array with lateral straggling in um"""
        return self._latStragArr


def calculateDensity(componentStoich: tuple[float],
                     componentDens: tuple[float],
                     temp: float=25,
                     pressure: float=1013.25):
    """
    Calculate density of ideal gas mixture.

    Arguments:
        componentStoich: stoichiometry of components
        componentDens: densities of components at 1013hPa & 0*C in g/dm3
        temp: temperature in degrees Celsius
        pressure: gas pressure in hPa
    Returns:
            float: gas mixture density in g/dm3
    """

    density=0
    t0=273.15
    p0=1013.25
    for i in range(len(componentStoich)):
        dens=componentDens[i] * (t0 / (temp + t0)) * (pressure / p0)
        density += dens * componentStoich[i]
    return density / sum(componentStoich)
