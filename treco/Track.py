import math
import numpy as np
import scipy.interpolate as scp

from . import srimUtils as srim
from .sim import lossProfile as loss
from . import constants as const
from .sim.simulationTools import imageFromLoss, pmtFromLoss
from .postprocessing import prepareImage, prepareWfm


class Track():

    """OTPC track simulator based on SRIM energy loss simulation.

    This class handles track parameters like initial energy, angles
    or position and can generate a simulated image and PMT waveform.

    Arguments:
        ptype: particle name (eg. 'proton' or 'alpha') or path to SRIM table.
            Name should match one of the keys in 'ptypes' dictionary in constants.py file.
            If name is not found in that dictionary, argument will be treated as path.
        e0: initial energy in keV.
        theta:  angle of particle track on camera image in degrees.
            0 means from left to right, 90 from bottom to top.
        phi:    angle between particle track and camera plane in degrees.
            0 is horizontal track, -90 is vertical from top to bottom.
    """

    def __init__(self, ptype: str,
                 e0: float = const.e0,
                 theta: float = const.theta,
                 phi: float = const.phi):

        try:
            self.srimFile = const.ptypes[ptype]
        except KeyError:
            self.srimFile = ptype
        self.type = ptype

        # load SRIM output file
        data = srim.SrimOutput(self.srimFile)
        self.rangeF = scp.interp1d(data.energyArr, data.rangeArr, kind='cubic')
        self.energyF = scp.interp1d(data.rangeArr, data.energyArr, kind='cubic')
        self.corrF = scp.interp1d(data.energyArr,
                                  data.dedxElecArr / (data.dedxElecArr + data.dedxNuclArr),
                                  kind='cubic')  # not used yet
        self.eLim = (data.energyArr[0], data.energyArr[-1])  # energy limits
        self.rangeLim = (data.rangeArr[0], data.rangeArr[-1])  # range limits

        self._checkE0Limits(e0)
        self._checkPhiLimits(phi)

        # set basic track parameters
        self._e0 = e0
        self._theta = theta
        self._phi = phi
        self._range = self.rangeF(self.e0).item()
        self._updateLengths()
        self._updateVector()

        # notifications
        self._notifying = False
        self._listeners = []

    def __repr__(self):
        return 'treco.Track({},e0={:.0f},theta={:.1f},phi={:.1f})'.format(self.type,
                                                                          self.e0,
                                                                          self.theta,
                                                                          self.phi)

    def _updateLengths(self):
        """Recalculate lengths of camera and wfm track projections"""

        self._imLength = self._range * math.cos(self._phi * math.pi / 180)
        self._wfmLength = self._range * \
            abs(math.sin(self._phi * math.pi / 180))

    def _updateVector(self):
        """Recalculate directional vector"""
        self._vector = np.array((math.cos(self._phi * math.pi / 180) * math.cos(self._theta * math.pi / 180),
                                 math.cos(self._phi * math.pi / 180) * math.sin(self._theta * math.pi / 180),
                                 math.sin(self._phi * math.pi / 180)))

    def makeImageLoss(self,
                      pixelSize: int = const.pixelWidth,
                      binning: int = const.binning,
                      nuclCorr: bool = const.nuclCorr):
        """Generate loss profile for the camera.

        Bin size depends on pixel width (incl. binning) and track vertical angle.

        Arguments:
            pixelSize: length on an image in um covered by one pixel.
                If 0 (default), value will be taken from constants file.
            binning: pixel binning; 1, 2 or 4. Default 1.
            nuclCorr: enable nuclear losses correction when calculating loss profile.
                Default False (disabled).
        Returns:
            numpy.ndarray: loss profile as 1D array. Index is bin number,
                Y is energy loss in bin (in keV).
        """

        if abs(self._phi) == 90:
            binSize = math.inf
        else:
            binSize = pixelSize * binning / math.cos(
                math.pi * self._phi / 180)

        return loss.makeLossProfileVec(self.energyF,
                                       self.rangeF,
                                       self._e0,
                                       binSize,
                                       rangemin=self.rangeLim[0],
                                       nuclCorrF=self.corrF)

    def makeWfmLoss(self,
                    sampling: int = const.sampling,
                    driftVelocity: int = const.driftVel,
                    nuclCorr: bool = const.nuclCorr):
        """Generate loss profile for the waveform.

        Bin size depends on drift velocity, sampling rate and track vertical angle.

        Arguments:
            sampling: sampling rate of the waveform in MSa/s.
            driftVelocity: drift velocity of the gas mixture in m/s.
            nuclCorr: enable nuclear losses correction when calculating loss profile.
                Default False (disabled).
        Returns:
            numpy.ndarray: loss profile as 1-D numpy array. Index is bin number,
                Y is energy loss in bin (in keV).
        """

        if self._phi == 0:
            binSize = math.inf
        else:
            binSize = abs(1 / sampling * driftVelocity /
                          math.sin(math.pi * self._phi / 180))

        return loss.makeLossProfileVec(self.energyF,
                                       self.rangeF,
                                       self._e0,
                                       binSize,
                                       rangemin=self.rangeLim[0],
                                       nuclCorrF=self.corrF)

    @property
    def e0(self):
        """
        Particle initial energy in keV.
        Setting will update range and im&wfm track lengths.
        """
        return self._e0

    @e0.setter
    def e0(self, value):
        self._checkE0Limits(value)
        self._e0 = value
        self._range = self.rangeF(self._e0).item()  # 0-D array to float
        self._updateLengths()
        self._notify('loss')

    @property
    def range(self):
        """
        Track range in um.
        Setting will update energy and im&wfm track lengths.
        """
        return self._range

    @range.setter
    def range(self, value):
        self._checkRangeLimits(value)
        self._range = value
        self._e0 = self.energyF(self._range).item()  # 0-D array to float
        self._updateLengths()
        self._notify('loss')

    @property
    def phi(self):
        """Angle between track and camera plane in degrees.
        0 is horizontal track, -90 is vertical from top to bottom.
        Setting will update im&wfm track lengths
        """
        return self._phi

    @phi.setter
    def phi(self, value):
        self._checkPhiLimits(value)
        self._phi = value
        self._updateLengths()
        self._updateVector()
        self._notify('loss')

    @property
    def theta(self):
        """Angle on camera plane in degrees."""
        return self._theta

    @theta.setter
    def theta(self, value):
        self._theta = value
        self._updateVector()
        self._notify('image')

    @property
    def imLength(self):
        """Track length on the image in um."""
        return self._imLength

    @property
    def wfmLength(self):
        """ Track length on the waveform in um."""
        return self._wfmLength

    @property
    def vector(self):
        """ Directional vector of the track """
        return self._vector

    def getParameters(self):
        """Return the dictionary of track parameters in
        a human readable format. Keys are named after attributes.

        Returns:
            dict: dictionary of tuples(value, unit).
                Keys are named after attributes.
        """
        dic = {}
        dic['e0'] = (self._e0, 'keV')
        dic['theta'] = (self.theta, 'deg')
        dic['phi'] = (self._phi, 'deg')
        dic['range'] = (self._range / 1000., 'mm')
        dic['imLength'] = (self._imLength / 1000., 'mm')
        dic['wfmLength'] = (self._wfmLength / 1000., 'mm')
        dic['vector'] = (self._vector), '[x,y,z]'
        return dic

    def setParameters(self,
                      e0: float = None,
                      prange: float = None,
                      phi: float = None,
                      theta: float = None):
        """Set multiple track parameters at once.

        This method should be called when one is setting multiple
        track parameters at once(especially e0, phi & theta),
        as this method avoids multiple image & wfm updates and in most
        cases should be faster than setting individual parameters.
        """

        if e0:
            self._checkE0Limits(e0)
            self._e0 = e0
            self._range = self.rangeF(e0).item()
        elif prange:
            self._checkRangeLimits(prange)
            self._range = prange
            self._e0 = self.energyF(prange).item()
        if phi is not None:
            self._checkPhiLimits(phi)
            self._phi = phi
        if theta is not None:
            self._theta = theta
        self._updateLengths()
        self._updateVector()
        self._notify('loss')

    def setImageLength(self,
                       length: int,
                       e0: float = None,
                       phi: float = None,
                       phiSgn: bool = True):
        """Set track length on the image assuming given energy or vertical angle.

        Arguments:
            length: track length on the image in um
            e0: initial track energy. If this argument is given, phi angle
                and wfm length will be recalculated.
            phi: vertical angle of the track. If this argument is given,
                initial track energy and wfm length will be recalculated.
            phiSgn: sign of phi angle; positive if True, negative otherwise.
                This argument will be ignored if phi argument is given.

        Exactly ONE of e0 or phi arguments must be specified.
        If both or neither of e0 and phi are given, TypeError exception
        will occur.
        """
        if (e0 is None and phi is None) or\
           (e0 is not None and phi is not None):
            raise TypeError('''Image length can not be set when
                             both/neither e0 and/or phi are given''')
        if e0 is not None:
            self._checkE0Limits(e0)
            prange = self.rangeF(e0).item()
            if length > prange:
                raise ValueError("Image length cannot be set for given energy")
            phi = math.acos(length / prange) * 180 / math.pi
            if not phiSgn:
                phi = -phi
        elif phi is not None:
            self._checkPhiLimits(phi)
            prange = length / math.cos(phi * math.pi / 180)
            e0 = self.energyF(prange).item()

        self._e0 = e0
        self._range = prange
        self._phi = phi
        self._updateLengths()
        self._updateVector()
        self._notify('loss')

    def setWfmLength(self,
                     length: int,
                     e0: float = None,
                     phi: float = None,
                     phiSgn: bool = True):
        """
        Set track length on the waveform assuming given energy
        or vertical angle.

        Arguments:
            length: track length on the waveform in um
            e0:     initial track energy. If this argument is given, phi angle
                    and image length will be recalculated.
            phi:    vertical angle of the track. If this argument is given,
                    initial track energy and image length will be recalculated.
            phiSgn: sign of phi angle; positive if True, negative otherwise.
                    This argument will be ignored if phi argument is given.

        Exactly ONE of e0 or phi arguments must be specified.
        If both or neither of e0 and phi are given, TypeError exception
        will occur.
        """
        if (e0 is None and phi is None) or\
           (e0 is not None and phi is not None):
            raise TypeError('''Waveform length cannot be set when
                             both/neither e0 and/or phi are given''')
        if e0 is not None:
            self._checkE0Limits(e0)
            prange = self.rangeF(e0).item()
            if length > prange:
                raise ValueError("Image length cannot be set for given energy")
            phi = math.asin(length / prange) * 180 / math.pi
            if not phiSgn:
                phi = -phi
        elif phi is not None:
            self._checkPhiLimits(phi)
            prange = length / abs(math.sin(phi * math.pi / 180))
            e0 = self.energyF(prange).item()

        self._e0 = e0
        self._range = prange
        self._phi = phi
        self._updateLengths()
        self._updateVector()
        self._notify('loss')

    def setLengths(self,
                   imageLength: int,
                   wfmLength: int,
                   phiSgn: bool = True):
        """Set track energy and phi angle based on given track
        lengths on camera and waveform.

        Arguments:
            imageLength: track length on the image in um
            wfmLength: track length on the waveform in um
            phiSgn: sign of phi angle; positive if True,
                negative otherwise.
        """

        phi = math.atan(wfmLength / imageLength) * 180 / math.pi
        if not phiSgn:
            phi = -phi
        prange = math.sqrt(imageLength**2 + wfmLength**2)
        self._checkRangeLimits(prange)
        e0 = self.energyF(prange).item()

        self._e0 = e0
        self._range = prange
        self._phi = phi
        self._updateLengths()
        self._updateVector()
        self._notify('loss')

    def makeImage(self,
                  loss,
                  shape: tuple[int, int],
                  x0: int = None,
                  y0: int = None,
                  sigma: float = 0,
                  amplitude: float = 0,
                  padded: bool = False):
        """Make image simulation from a loss profile. Image is a 2D numpy array.

        Arguments:

            loss: track loss profile, most likely generated
                with makeImageLoss() method.
            shape: image shape in form(width, height).?
            x0,y0: start of the track position in pixels.
                (0,0) is at the top left. Default (None) places
                the track in the middle of the image.
            sigma: sigma parameter for gaussian filter applied
                to the image. 0 means no filtering.
            amplitude: value of the brightest pixel after scaling.
                Default (0) means no scaling is performed.
            padded: if true, returned image is padded with size * 1.5 on all sides.

        Returns:
            image:      2D numpy array with pixel values
            scaleCoeff: scaling coefficient. Divide output image by this to get unscaled pixel values.
        """
        if x0 is None:
            x0 = shape[1] // 2
        if y0 is None:
            y0 = shape[0] // 2
        image = imageFromLoss(loss,
                              shape,
                              self._theta,
                              x0, y0,
                              padded=padded)
        image, scaleCoeff = prepareImage(image, sigma, 0, amplitude)
        return image, scaleCoeff

    def makeWfm(self,
                loss,
                size: int,
                z0: int = 0,
                sigma: float = 0,
                amplitude: float = 0,
                padded: bool = False):
        """Make a waveform simulation.

        Arguments:
            loss: track loss profile, most likely generated with makeImageLoss() method.
            size: waveform size in samples
            z0: start of the track position in samples.
                Sample 0 is in the middle of the waveform.
            sigma: sigma parameter for gaussian filter applied to the waveform.
                Default (0) means no filtering.
            amplitude: value of the brightest sample after scaling.
                Default (0) means no scaling is performed.
            padded: if True, returned waveform is padded with size // 2 zeros on both sides
                and z0 is ignored (using default z0=0)

        Returns:
            (tuple): tuple containing:
                image: 1D numpy array with sample values
                scaleCoeff: scaling coefficient. Divide output waveform by this
                to get unscaled sample values.
        """

        wfm = pmtFromLoss(loss, size, z0,
                          flip=(self._phi > 0),
                          padded=padded)
        wfm, scaleCoeff = prepareWfm(wfm, sigma, 0, amplitude)
        return wfm, scaleCoeff

    def _checkE0Limits(self, value):
        """Return ValueError if energy outside limits determined by SRIM file."""
        if value < self.eLim[0] or value > self.eLim[1]:
            raise ValueError('E0 value {} out of range {}:{}'.format(
                value, *self.eLim))

    def _checkRangeLimits(self, value):
        """Return ValueError if range too high/low.
            Range limits are determined by SRIM file."""
        if value < self.rangeLim[0] or value > self.rangeLim[1]:
            raise ValueError('Range value {} out of range {}:{}'.format(
                value, *self.rangeLim))

    def _checkPhiLimits(self, value):
        if value < -90 or value > 90:
            raise ValueError('Phi value {} out of range {}:{}'.format(
                value, -90, 90))

    # name used in EventViewer, do not change
    def _connectViewer(self, viewerObj):
        """
        Connect EventViewer, ImageSimulator or WfmSimulator
        object to be notified of changes in Track images / wfms.

        If an object is connected, every time there is a change in an image
        or waveform, recvNotification() method of that object is invoked.
        """
        self._listeners.append(viewerObj)
        self._notify = self._notifyWrapper
        self._notifying = True

    def _disconnectViewer(self, viewerObj):
        """ Disconnect Viewer object connected earlier."""
        idx = self._listeners.index(viewerObj)
        self._listeners.pop(idx)
        if len(self._listeners) == 0:
            self._notifying = False
            self._notify = lambda: None
        else:
            self._notify = self._notifyWrapper

    def _notifyWrapper(self, message):
        """Invokes recvNotification method in all listening objects."""
        if self._notifying:
            for listener in self._listeners:
                listener._recvNotification(self, message)

    def _notify(self, message):
        """Placeholder function"""
        pass
