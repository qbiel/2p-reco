import numpy as np
from SmartLoad import SmartLoad

from ..postprocessing import prepareWfm


class ExpWaveform:
    """
    Class storing PMT waveforms from OTPC experiment and preparing it for analysis.
    Currently supports background reduction, gaussian smoothing and value scaling.

    """

    def __init__(self, path: str,
                 sampling: int = 0,
                 start: float = None,
                 stop: float = None,
                 channel: int = 0,
                 flip: bool = True,
                 sigma: float = 0,
                 bg: int = 0,
                 scale: float = 1,
                 filetype='sl'):

        self.path = path
        self.channel = channel
        self.filetype = filetype

        if self.filetype == 'sl':
            self.wfmObj = SmartLoad(self.path, self.channel)
            self.wfmObj.loadChannel(start, stop)
            self._rawwfm = self.wfmObj.data.copy()
            self._sampling = self.wfmObj.samplingRate / 1e6
            self.wfmX = self.wfmObj.x.copy()
            self._wfmStart = self.wfmX[0]
            self._wfmStop = self.wfmX[-1]
        elif self.filetype == 'vec':
            with open(path, 'r') as f:
                lines = f.readlines()
                self._rawwfm = np.zeros(len(lines))
                for i in range(self._rawwfm.size):
                    self._rawwfm[i] = float(lines[i].split(' ')[-1][:-1])
            if not sampling:
                raise ValueError('Sampling rate must be given explicitly for .vec files')
            self._sampling = sampling
            self._wfmStart = start if start is not None else 0
            self._wfmStop = stop if stop is not None else self._rawwfm.size / self.sampling
            self.wfmX = np.arange(self._wfmStart, self.wfmStop, 1 / self._sampling)
            self._rawwfm = self._rawwfm[int(self._wfmStart * self._sampling):int(self._wfmStop * self._sampling)]
        elif self.filetype == 'npy':
            if not sampling:
                raise ValueError('Sampling rate must be given explicitly for .npy files')
            self._sampling = sampling
            self._rawwfm = np.load(path)
            self._wfmStart = start if start is not None else 0
            self._wfmStop = stop if stop is not None else self._rawwfm.size / self.sampling
            self.wfmX = np.arange(self._wfmStart, self.wfmStop, 1 / self._sampling)
            self._rawwfm = self._rawwfm[int(self._wfmStart * self._sampling):int(self._wfmStop * self._sampling)]

        if flip:
            self._rawwfm = - self._rawwfm
        self._bg = bg
        self._scale = scale
        self.sigma = sigma  # fist wfm preparation happens here

        # notifications
        self._notifying = False
        self._listeners = []

    def refresh(self):
        self.wfm = prepareWfm(self._rawwfm,
                              self._sigma,
                              self._bg,
                              0)[0] * self._scale

    @property
    def sampling(self):
        return self._sampling

    @property
    def sigma(self):
        return self._sigma

    @property
    def wfmStart(self):
        return self._wfmStart

    @property
    def wfmStop(self):
        return self._wfmStop

    @sigma.setter
    def sigma(self, value):
        # decouple smoothing from bg/scale for faster bg/scale setting
        self._sigma = value
        self.refresh()

    @property
    def bg(self):
        return self._bg

    @bg.setter
    def bg(self, value):
        self._bg = value
        self.refresh()

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, value):
        self._scale = value
        self.refresh()

    @property
    def wfm(self):
        return self._wfm

    @wfm.setter
    def wfm(self, value):
        self._wfm = value
        self._notify()

    def _connectViewer(self, viewerObj):
        """
        Connect EventViewer object to be notified of changes.

        If an object is connected, every time there is a change in scale, bg etc,
        _recvNotification() method of that object is invoked.
        """
        self._listeners.append(viewerObj)
        self._notify = self._notifyWrapper
        self._notifying = True

    def _disconnectViewer(self, viewerObj):
        """ Disconnect Viewer object connected earlier."""
        idx = self._listeners.index(viewerObj)
        self._listeners.pop(idx)
        if len(self._listeners) == 0:
            self._notifying = False
            self._notify = lambda: None
        else:
            self._notify = self._notifyWrapper

    def _notifyWrapper(self):
        """Invokes _recvNotification method in all listening objects."""
        if self._notifying:
            for listener in self._listeners:
                listener._recvNotification(self, 'expwfm')

    def _notify(self):
        """Placeholder function"""
        pass
