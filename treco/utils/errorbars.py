import math
import copy
import numpy as np

from ..constants import pixelWidth


def getThetaStep(trackObj):
    return math.degrees(pixelWidth / trackObj.imLength)


def getRangeError(trackObj, imLError, wfmLError):
    s2 = math.cos(math.radians(trackObj.phi))**2 * imLError**2 + \
        math.sin(math.radians(trackObj.phi))**2 * wfmLError**2

    return math.sqrt(s2)


def getPhiError(trackObj, imLError, wfmLError):
    protoS2 = wfmLError**2 * math.cos(math.radians(trackObj.phi))**2 +\
        imLError**2 * math.sin(math.radians(trackObj.phi))**2
    return math.degrees(1 / trackObj.range * math.sqrt(protoS2))


def getEnergyError(trackObj, rangeErr):
    """Returns (errorLow, errorHigh, errorAvg)"""
    t = copy.copy(trackObj)
    e0 = t.e0
    t.range -= rangeErr
    minE = t.e0
    t.range += 2 * rangeErr
    maxE = t.e0
    del t
    return (e0 - minE, maxE - e0, (maxE - minE) / 2)


def getTrackAngleError(trackObj1, trackObj2, imL1Error, imL2Error, wfmL1Error, wfmL2Error, theta1Error, theta2Error):
    """ Returns errorbar on an angle between tracks"""

    v1 = trackObj1.vector * trackObj1.range
    v2 = trackObj2.vector * trackObj2.range
    dvdil1 = np.array([np.cos(math.radians(trackObj1.theta)), np.sin(math.radians(trackObj1.theta)), 0])
    dvdil2 = np.array([np.cos(math.radians(trackObj2.theta)), np.sin(math.radians(trackObj2.theta)), 0])
    dvdwl1 = np.array([0, 0, 1])
    dvdwl2 = np.array([0, 0, 1])
    dvdtheta1 = np.array([-v1[1], v1[0], 0])
    dvdtheta2 = np.array([-v2[1], v2[0], 0])
    v1v2 = trackObj1.range * trackObj2.range
    cosalpha = np.dot(v1, v2) / v1v2
    print(np.arccos(cosalpha))

    # sTheta1 = math.radians(theta1Error) * (np.dot(v2, dvdtheta1) / v1v2)
    # sTheta2 = math.radians(theta2Error) * (np.dot(v1, dvdtheta2) / v1v2)
    # sIml1 = imL1Error * (np.dot(v2, dvdil1) + np.dot(v1, v2) * trackObj1.imLength / np.sum(v1**2)) / v1v2
    # sIml2 = imL2Error * (np.dot(v1, dvdil2) + np.dot(v1, v2) * trackObj2.imLength / np.sum(v2**2)) / v1v2
    # sWfml1 = wfmL1Error * (np.dot(v2, dvdwl1) + np.dot(v1, v2) * trackObj1.wfmLength / np.sum(v1**2)) / v1v2
    # sWfml2 = wfmL2Error * (np.dot(v1, dvdwl2) + np.dot(v1, v2) * trackObj2.wfmLength / np.sum(v2**2)) / v1v2

    sTheta1 = math.radians(theta1Error) * (np.dot(v2, dvdtheta1) / v1v2)
    sTheta2 = math.radians(theta2Error) * (np.dot(v1, dvdtheta2) / v1v2)
    sIml1 = imL1Error * (np.dot(v2, dvdil1) - cosalpha * trackObj1.imLength * trackObj2.range / trackObj1.range) / v1v2
    sIml2 = imL2Error * (np.dot(v1, dvdil2) - cosalpha * trackObj2.imLength * trackObj1.range / trackObj2.range) / v1v2
    sWfml1 = wfmL1Error * (np.dot(v2, dvdwl1) - cosalpha * trackObj1.wfmLength * trackObj2.range / trackObj1.range) / v1v2
    sWfml2 = wfmL2Error * (np.dot(v1, dvdwl2) - cosalpha * trackObj2.wfmLength * trackObj1.range / trackObj2.range) / v1v2

    sCos2 = sTheta1**2 + sTheta2**2 + sIml1**2 + sIml2**2 + sWfml1**2 + sWfml2**2
    sAlpha = np.sqrt(sCos2 / (1 - cosalpha**2))
    return math.degrees(sAlpha)
