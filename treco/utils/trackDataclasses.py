from dataclasses import dataclass, field
from typing import Optional

from . import scanParser as sp


@dataclass(frozen=True)
class particleParamList:
    """Class keeping lists of particle-specific parameters."""
    name: str
    theta: list[float]
    e0: Optional[list[int]] = field(default=None)
    phi: Optional[list[float]] = field(default=None)
    imL: Optional[list[int]] = field(default=None)
    wfmL: Optional[list[int]] = field(default=None)
    phiSgn: Optional[list[bool]] = field(default=None)


@dataclass(frozen=True)
class commonParamList:
    """Class keeping lists of parameters common to all particles
       in a simulation"""

    x0: list[int]
    y0: list[int]
    z0: list[float]
    sigmaC: list[float]
    sigmaP: list[float]
    scaleC: list[float]
    scaleP: list[float]

    @classmethod
    def fromConfig(cls, cfg):
        """ Prepare common parameters list from config file"""
        x0 = sp.getSubparameter(cfg, 'x0')
        y0 = sp.getSubparameter(cfg, 'y0')
        z0 = sp.getSubparameter(cfg, 'z0')
        sigmaC = sp.getSubparameter(cfg, 'sigmaC')
        sigmaP = sp.getSubparameter(cfg, 'sigmaP')
        scaleC = sp.getSubparameter(cfg, 'scaleC')
        scaleP = sp.getSubparameter(cfg, 'scaleP')
        return cls(x0, y0, z0, sigmaC, sigmaP, scaleC, scaleP)
