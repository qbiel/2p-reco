import numpy as np
from itertools import product

from .. import Track
from . import scanParser as sp


def makeConstraintParamListEphi(cfg, constrainFunc):
    """
    Make an array of parameter combinations fitting specified constraints.

    Arguments:
        cfg:           ConfigParser instance with loaded config
                       file for parameter scanner
        constrainFunc: function (list[treco.Track], dict) -> bool.
                       Function should accept list of Track objects and
                       a dictionary with a single parameter combination
                       with keys (e0_0, theta_0, phi_0, e0_1...).
                       Function should return True if given combination
                       fits specified constraints and False otherwise.
    Returns:
        paramArray:    numpy's structured array with parameter combinations
                       fitting constraints specified by constrainFunc.
                       Array keys: (e0_0, theta_0, phi_0, e0_1, theta_1...)
    """

    particleCount = cfg['Misc'].getint('particlecount')

    # lists of parameter arrays
    tracks = []
    iterables = []
    combinationCount = 1

    for i in range(1, particleCount + 1):
        e0 = sp.getParticleParameter(cfg, i, 'e0')
        # theta = sp.getParticleParameter(cfg, i, 'theta')
        phi = sp.getParticleParameter(cfg, i, 'phi')
        tracks.append(Track(cfg[f'Particle{i}']['particle']))
        # iterables.extend([e0, theta, phi])
        iterables.extend([e0, phi])
        # combinationCount *= len(e0) * len(theta) * len(phi)
        combinationCount *= len(e0) * len(phi)

    paramset = product(*iterables)

    # initialize structures array
    names = []
    formats = []
    for i in range(particleCount):
        # names.extend([f'e0_{i}', f'theta_{i}', f'phi_{i}'])
        # formats.extend(['u2', 'f4', 'f4'])
        names.extend([f'e0_{i}', f'phi_{i}'])
        formats.extend(['u2', 'f4'])

    paramArray = np.zeros(combinationCount,
                          dtype=np.dtype({'names': names,
                                          'formats': formats}))
    j = 0
    i = 0
    for paramtuple in paramset:
        paramDic = {}
        for j in range(particleCount):
            paramDic[f'e0_{j}'] = paramtuple[j * 2]
            # paramDic[f'theta_{j}'] = paramtuple[j * 3 + 1]
            # paramDic[f'phi_{j}'] = paramtuple[j * 3 + 2]
            paramDic[f'phi_{j}'] = paramtuple[j * 2 + 1]
        if constrainFunc(tracks, paramDic):
            paramArray[i] = paramtuple
            i += 1
    paramArray = paramArray[:i]
    return paramArray


def makeConstraintParamListLenlen(cfg, constrainFunc):
    """
    Make an array of parameter combinations fitting specified constraints.

    Arguments:
        cfg:           ConfigParser instance with loaded config
                       file for parameter scanner
        constrainFunc: function (list[treco.Track], dict) -> bool.
                       Function should accept list of Track objects and
                       a dictionary with a single parameter combination
                       with keys (imL_0, wfmL_0, theta_0, imL_1...).
                       Function should return True if given combination
                       fits specified constraints and False otherwise.
    Returns:
        paramArray:    numpy's structured array with parameter combinations fitting
                       constraints specified by constrainFunc.
                       Array keys: (imL_0, wfmL_0, theta_0, imL_1, wfmL_1...)
    """

    particleCount = cfg['Misc'].getint('particlecount')

    # lists of parameter arrays
    tracks = []
    iterables = []
    combinationCount = 1

    for i in range(1, particleCount + 1):
        imL = sp.getParticleParameter(cfg, i, 'imL')
        wfmL = sp.getParticleParameter(cfg, i, 'wfmL')
        phiSgn = sp.getParticleParameter(cfg, i, 'phiSgn')
        tracks.append(Track(cfg[f'Particle{i}']['particle']))
        # iterables.extend([imL, wfmL, theta])
        iterables.extend([imL, wfmL, phiSgn])
        combinationCount *= len(imL) * len(wfmL) * len(phiSgn)

    paramset = product(*iterables)

    # initialize structures array
    names = []
    formats = []
    for i in range(particleCount):
        names.extend([f'imL_{i}', f'wfmL_{i}', f'phiSgn_{i}'])
        formats.extend(['u4', 'u4', 'i2'])
    paramArray = np.zeros(combinationCount,
                          dtype=np.dtype({'names': names,
                                          'formats': formats}))
    j = 0
    i = 0
    for paramtuple in paramset:
        paramDic = {}
        for j in range(particleCount):
            paramDic[f'imL_{j}'] = paramtuple[j * 3]
            paramDic[f'wfmL_{j}'] = paramtuple[j * 3 + 1]
            # paramDic[f'theta_{j}'] = paramtuple[j * 3 + 2]
            paramDic[f'_{j}'] = paramtuple[j * 3 + 2]
        if constrainFunc(tracks, paramDic):
            paramArray[i] = paramtuple
            i += 1
    paramArray = paramArray[:i]
    return paramArray


def constraintEphiExample(trackObjs, paramDic):
    for i, track in enumerate(trackObjs):
        track.setParameters(paramDic[f'e0_{i}'],
                            paramDic[f'phi_{i}'])
    if abs(trackObjs[0].e0 + trackObjs[1].e0 - 750) < 50:
        return True
    else:
        return False


def constraintLenlenExample(trackObjs, paramDic):
    for i, track in enumerate(trackObjs):
        track.setLengths(paramDic[f'imL_{i}'],
                         paramDic[f'wfmL_{i}'],
                         paramDic[f'phiSgn_{i}'])
    if abs(trackObjs[0].e0 + trackObjs[1].e0 - 750) < 50:
        return True
    else:
        return False


if __name__ == '__main__':

    import argparse as agp
    import configparser

    parser = agp.ArgumentParser(
        description='''Apply constraints to parameter space''')
    parser.add_argument('config', type=str,
                        help='Config file for parameter scanner')
    parser.add_argument('--output', '-o', type=str,
                        help='Output file path')
    parser.add_argument('--preview', '-p', action='store_true')
    args = parser.parse_args()

    cfg = configparser.ConfigParser()
    cfg.read(args.config)
    if cfg.has_option('Particle1', 'e0'):
        mode = 'ephi'
    elif cfg.has_option('Particle1', 'imL'):
        mode = 'lenlen'
    else:
        raise KeyError("Missing option: e0/imL")

    if mode == 'ephi':
        paramArr = makeConstraintParamListEphi(cfg, constraintEphiExample)
    else:
        paramArr = makeConstraintParamListLenLen(cfg, constraintLenlenExample)

    if args.output:
        np.save(args.output, paramArr)

    if args.preview:
        import matplotlib.pyplot as plt
        # from mpl_toolkits.axes_grid1 import make_axes_locatable
        particleCount = cfg['Misc'].getint('particlecount')
        fig, ax = plt.subplots(nrows=1, ncols=particleCount)
        hists = []
        for i in range(particleCount):
            binsx = sp.getParticleParameter(cfg, i + 1, 'e0').size
            binsy = sp.getParticleParameter(cfg, i + 1, 'phi').size
            hists.append(ax[i].hist2d(x=paramArr[f'e0_{i}'], y=paramArr[f'phi_{i}'],
                                      bins=[binsx, binsy],
                                      cmin=1))
            ax[i].set_title(f'particle {i}')
            ax[i].set_xlabel(f'e0_{i}')
            ax[i].set_ylabel(f'phi_{i}')
            plt.colorbar(hists[-1][3], ax=ax[i])
        plt.show()
