import os
import treco.constants as const
import configparser


def makeScannerConfig(viewerObj,
                      outputfile,
                      imLPoints: int = 15,
                      wfmLPoints: int = 15,
                      thetaPoints: int = 7,
                      x0Points: int = 7,
                      y0Points: int = 7,
                      z0Points: int = 13,
                      sigmaCPoints: int = 5,
                      sigmaWPoints: int = 5,
                      imLPrecision: float = 1,
                      wfmLPrecision: float = 1,
                      thetasteps: float = [4],
                      sigmaPrecision: float = 0.33,
                      run: int = 0,
                      event: int = 0,
                      outputName: str = 'output.hdf5',
                      outputDir: str = '.'):

    imLPrecision = imLPrecision * const.pixelWidth
    wfmLPrecision = wfmLPrecision * (const.driftVel / viewerObj.sampling)
    x0Precision = 1
    y0Precision = 1
    z0Precision = 1 / viewerObj.sampling
    scalePrecision = 0.02
    trackCnt = len(viewerObj.tracks)

    cfg = configparser.ConfigParser()
    cfg['Paths'] = {'imagepath': '',
                    'pmtpath': '',
                    'outputdirectory': '',
                    'outputfilename': ''}

    cfg['Particle1'] = {'particle': '',
                        'imL': '',
                        'wfmL': '',
                        'phiSgn': '',
                        'theta': ''}

    if trackCnt == 2:
        cfg['Particle2'] = {'particle': '',
                            'imL': '',
                            'wfmL': '',
                            'phiSgn': '',
                            'theta': ''}

    cfg['Subparameters'] = {'x0': '',
                            'y0': '',
                            'z0': '',
                            'sigmac': '',
                            'sigmap': '',
                            'scalec': '',
                            'scalep': ''
                            }
    cfg['Misc'] = {
        'particlecount': '',
        'run': '',
        'event': '',
        'pmtfragmentstart': '',
        'pmtfragmentstop': '',
        'imagesmoothsigma': '',
        'pmtsmoothsigma': '',
        'pmtbglevel': '',
        'camerabinning': ''
    }
    cfg['Paths']['imagepath'] = os.path.abspath(viewerObj.expimage.path)
    cfg['Paths']['pmtpath'] = os.path.abspath(viewerObj.expwfm.path)
    cfg['Paths']['outputdirectory'] = outputDir
    cfg['Paths']['outputfilename'] = outputName

    curImL = []
    curWfmL = []
    curTheta = []
    for i, track in enumerate(viewerObj.tracks):
        curImL.append(track.imLength // imLPrecision * imLPrecision)
        curWfmL.append(track.wfmLength // wfmLPrecision * wfmLPrecision)
        curTheta.append(track.theta // thetasteps[i] * thetasteps[i])
    curX0 = viewerObj.x0
    curY0 = viewerObj.y0
    curZ0 = viewerObj.z0
    curSigmaC = viewerObj.sigmaC
    curSigmaW = viewerObj.sigmaW

    for i in range(trackCnt):
        cfg[f'Particle{i+1}']['particle'] = viewerObj.tracks[i].type
        imLinit = curImL[i] - imLPoints // 2 * imLPrecision
        imLfinal = imLinit + imLPoints * imLPrecision + 1
        if imLinit <= 0:
            imLinit = const.pixelWidth
        cfg[f'Particle{i+1}']['imL'] = '{},{},{}'.format(imLinit, imLfinal, imLPrecision)

        wfmLinit = curWfmL[i] - wfmLPoints // 2 * wfmLPrecision
        wfmLfinal = wfmLinit + wfmLPoints * wfmLPrecision + 1
        if wfmLinit <= 0:
            wfmLinit = (const.driftVel / viewerObj.sampling)
        cfg[f'Particle{i+1}']['wfmL'] = '{},{},{}'.format(wfmLinit, wfmLfinal, wfmLPrecision)

        thetainit = curTheta[i] - thetaPoints // 2 * thetasteps[i]
        thetaFinal = thetainit + thetaPoints * thetasteps[i]
        cfg[f'Particle{i+1}']['theta'] = '{},{},{}'.format(thetainit, thetaFinal, thetasteps[i])

        phiSgn = (1, 2, 1) if viewerObj.tracks[i].phi > 0 else (-1, 0, 1)
        cfg[f'Particle{i+1}']['phiSgn'] = '{},{},{}'.format(*phiSgn)

# --------------------------------------------

    x0init = curX0 - x0Points // 2 * x0Precision
    x0final = x0init + x0Points * x0Precision
    y0init = curY0 - y0Points // 2 * y0Precision
    y0final = y0init + y0Points * y0Precision
    z0init = curZ0 - z0Points // 2 * z0Precision
    z0final = z0init + z0Points * z0Precision
    if z0init < viewerObj.wfmStart:
        z0init = round(viewerObj.wfmStart + 2*z0Precision, 2)
    if z0final > viewerObj._getWfmStop():
        z0final = round(viewerObj._getWfmStop() - 2*z0Precision, 2)
    sigmaCinit = curSigmaC - sigmaCPoints // 2 * sigmaPrecision
    if sigmaCinit <= 0:
        sigmaCinit = sigmaPrecision
    sigmaCfinal = sigmaCinit + sigmaCPoints * sigmaPrecision
    sigmaWinit = curSigmaW - sigmaWPoints // 2 * sigmaPrecision
    if sigmaWinit <= 0:
        sigmaWinit = sigmaPrecision
    sigmaWfinal = sigmaWinit + sigmaWPoints * sigmaPrecision
    cfg['Subparameters']['x0'] = '{},{},{}'.format(x0init, x0final + 1, x0Precision)
    cfg['Subparameters']['y0'] = '{},{},{}'.format(y0init, y0final + 1, y0Precision)
    cfg['Subparameters']['z0'] = '{},{},{}'.format(z0init, z0final + z0Precision, z0Precision)
    cfg['Subparameters']['sigmaC'] = '{},{},{}'.format(sigmaCinit, sigmaCfinal + 0.1, sigmaPrecision)
    cfg['Subparameters']['sigmaP'] = '{},{},{}'.format(sigmaWinit, sigmaWfinal + 0.1, sigmaPrecision)
    cfg['Subparameters']['scaleC'] = '{},{},{}'.format(0.8, 1.201, scalePrecision)
    cfg['Subparameters']['scaleP'] = '{},{},{}'.format(0.8, 1.201, scalePrecision)

# --------------------------------
    cfg['Misc']['particlecount'] = str(trackCnt)
    cfg['Misc']['run'] = str(run)
    cfg['Misc']['event'] = str(event)
    cfg['Misc']['pmtfragmentstart'] = str(viewerObj.wfmStart)
    cfg['Misc']['pmtfragmentstop'] = str(viewerObj._getWfmStop())
    cfg['Misc']['imagesmoothsigma'] = str(viewerObj.expimage.sigma)
    cfg['Misc']['pmtsmoothsigma'] = str(viewerObj.expwfm.sigma)
    cfg['Misc']['pmtbglevel'] = str(viewerObj.expwfm.bg)
    cfg['Misc']['camerabinning'] = str(viewerObj.binning)
    with open(outputfile, 'w') as f:
        cfg.write(f)
