import numpy as np
import imageio as io

from ..postprocessing import prepareImage


class ExpImage:
    """
    Class storing camera image from OTPC experiment and preparing it for analysis.
    Currently supports background reduction, gaussian smoothing and pixel scaling.

    """

    def __init__(self, path: str,
                 sigma: float = 0,
                 bg: int = 0,
                 scale: float = 1):

        self.path = path
        self._rawImage = io.imread(path)
        self._bg = bg
        self._scale = scale
        self.sigma = sigma  # fist image preparation happens here

        self.updateProjections()

        # notifications
        self._notifying = False
        self._listeners = []

    def updateProjections(self):
        self.hProj = np.sum(self.image, 0)
        self.vProj = np.sum(self.image, 1)

    def refresh(self):
        self.image = prepareImage(self._rawImage,
                                  self._sigma,
                                  self._bg,
                                  0)[0] * self._scale

    @property
    def sigma(self):
        return self._sigma

    @sigma.setter
    def sigma(self, value):
        self._sigma = value
        self.refresh()

    @property
    def bg(self):
        return self._bg

    @bg.setter
    def bg(self, value):
        self._bg = value
        self.refresh()

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, value):
        self._scale = value
        self.refresh()

    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, value):
        self._image = value
        self.updateProjections()
        self._notify()

    def _connectViewer(self, viewerObj):
        """
        Connect EventViewer object to be notified of changes.

        If an object is connected, every time there is a change in scale, bg etc,
        _recvNotification() method of that object is invoked.
        """
        self._listeners.append(viewerObj)
        self._notify = self._notifyWrapper
        self._notifying = True

    def _disconnectViewer(self, viewerObj):
        """ Disconnect Viewer object connected earlier."""
        idx = self._listeners.index(viewerObj)
        self._listeners.pop(idx)
        if len(self._listeners) == 0:
            self._notifying = False
            self._notify = lambda: None
        else:
            self._notify = self._notifyWrapper

    def _notifyWrapper(self):
        """Invokes _recvNotification method in all listening objects."""
        if self._notifying:
            for listener in self._listeners:
                listener._recvNotification(self, 'expimage')

    def _notify(self):
        """Placeholder function"""
        pass
