import numpy as np


def makeRange(string, asFloat=True):
    stringList = string.split(',')
    if asFloat:
        for i in range(len(stringList)):
            stringList[i] = float(stringList[i])
    else:
        for i in range(len(stringList)):
            stringList[i] = int(stringList[i])
    return np.arange(start=stringList[0],
                     stop=stringList[1],
                     step=stringList[2])


def getArrayFromConfig(parserObj, key):
    """
    Parse parameter range from config file and return an array
    containing all parameter values to scan.
    """
    if key.lower() in ['x0','y0']:
        arr = makeRange(parserObj['ScanRange'][key],asFloat=False)
    else:
        arr = makeRange(parserObj['ScanRange'][key])
    return arr

def getPathFromConfig(parserObj, key):
    return parserObj['Paths'][key]

def getMiscFromConfig(parserObj, key):
    """
    Return miscellaneous entries from config file, like binning or
    wfm background. All values are returned as integers
    """
    return int(parserObj['Misc'][key])

def getParticleParameter(parserObj, particleNo, key):
    section = 'Particle{}'.format(particleNo)
    return makeRange(parserObj[section][key])

def getSubparameter(parserObj,key):
    if key.lower() in ['x0','y0']:
        arr = makeRange(parserObj['Subparameters'][key],asFloat=False)
    else:
        arr = makeRange(parserObj['Subparameters'][key])
    return arr