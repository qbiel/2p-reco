import xarray as xr
import numpy as np

from . import scanParser as sp


def makeDimsCoordsEphi(cfg):
    dimsC = []
    dimSizeC = []
    dimsP = []
    dimSizeP = []
    coords = {}
    particleCount = cfg['Misc'].getint('particlecount')
    for i in range(particleCount):
        dimsC.extend([f'e0_{i}', f'phi_{i}', f'theta_{i}'])
        dimsP.extend([f'e0_{i}', f'phi_{i}'])
        e0Arr = sp.getParticleParameter(cfg, i + 1, 'e0')
        phiArr = sp.getParticleParameter(cfg, i + 1, 'phi')
        thetaArr = sp.getParticleParameter(cfg, i + 1, 'theta')
        dimSizeC.extend([len(e0Arr), len(phiArr), len(thetaArr)])
        dimSizeP.extend([len(e0Arr), len(phiArr)])
        coords[f'e0_{i}'] = e0Arr
        coords[f'phi_{i}'] = phiArr
        coords[f'theta_{i}'] = thetaArr
    coords['subparam'] = ['x0', 'y0', 'z0',
                          'sigmaC', 'sigmaP', 'scaleC', 'scaleP']
    return dimsC, dimsP, dimSizeC, dimSizeP, coords


def makeDimsCoordsLenlen(cfg):
    dimsC = []
    dimSizeC = []
    dimsP = []
    dimSizeP = []
    coords = {}
    particleCount = cfg['Misc'].getint('particlecount')
    for i in range(particleCount):
        dimsC.extend([f'imL_{i}', f'wfmL_{i}', f'phiSgn_{i}', f'theta_{i}'])
        dimsP.extend([f'imL_{i}', f'wfmL_{i}', f'phiSgn_{i}'])
        imLArr = sp.getParticleParameter(cfg, i + 1, 'imL')
        wfmLArr = sp.getParticleParameter(cfg, i + 1, 'wfmL')
        phiSgnArr = sp.getParticleParameter(cfg, i + 1, 'phiSgn')
        # phiSgnArr = [True, False]
        thetaArr = sp.getParticleParameter(cfg, i + 1, 'theta')
        dimSizeC.extend([len(imLArr), len(wfmLArr), len(phiSgnArr), len(thetaArr)])
        dimSizeP.extend([len(imLArr), len(wfmLArr), len(phiSgnArr)])
        coords[f'imL_{i}'] = imLArr
        coords[f'wfmL_{i}'] = wfmLArr
        coords[f'phiSgn_{i}'] = phiSgnArr
        coords[f'theta_{i}'] = thetaArr
    coords['subparam'] = ['x0', 'y0', 'z0',
                          'sigmaC', 'sigmaP', 'scaleC', 'scaleP']
    return dimsC, dimsP, dimSizeC, dimSizeP, coords


def makeDataset(mode, cfg, attributes={}):
    attrs = dict(cfg['Misc'])
    attrs = attrs | dict(cfg['Paths'])
    attrs = attrs | attributes
    for i in range(cfg['Misc'].getint('particleCount')):
        attrs = attrs | {f'particle{i+1}': cfg[f'Particle{i+1}']['particle']}

    if mode.lower() == 'ephi':
        dimsC, dimsP, dimSizeC, dimSizeP, coords = makeDimsCoordsEphi(cfg)
    elif mode.lower() == 'lenlen':
        dimsC, dimsP, dimSizeC, dimSizeP, coords = makeDimsCoordsLenlen(cfg)
    else:
        raise KeyError("Mode argument should be either 'ephi' or 'lenlen'")

    ds = xr.Dataset(
        {
            'dzetasC': (dimsC,
                        np.full(dimSizeC, np.nan, dtype=np.float32)),
            'dzetasP': (dimsP,
                        np.full(dimSizeP, np.nan, dtype=np.float32)),
            'bestSubparams': (dimsC + ['subparam'],
                              np.full(dimSizeC + [7], np.nan, dtype=np.float32))
        }, coords=coords, attrs=attrs)
    return ds


if __name__ == '__main__':

    import argparse as agp
    from configparser import ConfigParser
    parser = agp.ArgumentParser(description="""Test dataset making function""")
    parser.add_argument('mode', type=str, choices=['lenlen', 'ephi'])
    parser.add_argument('--config', '-c',
                        default='configTemplate.cfg',
                        type=str,
                        help='Config file for scanner')
    args = parser.parse_args()
    cfg = ConfigParser()
    cfg.read(args.config)
    ds = makeDataset(args.mode, cfg)
