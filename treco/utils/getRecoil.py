import math
import numpy as np


def getRecoil(trackObj1, trackObj2, track1Mass, track2Mass, recoilMass):
    """
    Masses in keV/c2
    """

    momentum1 = trackObj1.vector * math.sqrt(2*track1Mass*trackObj1.e0)
    momentum2 = trackObj2.vector * math.sqrt(2*track2Mass*trackObj2.e0)
    recoilMomentum = -(momentum2 + momentum1)
    recoilMomentumValue = np.sum(recoilMomentum**2)
    return recoilMomentumValue/(2*recoilMass)