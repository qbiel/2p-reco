import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import Normalize
import xarray as xr

from treco import EventViewer, Track


class MatrixViewer:
    """
    Viewer for scanner results
    """

    def __init__(self, hdf5file: str):
        """ matrix: image or wfm"""
        # load output file
        self.dataset = xr.open_dataset(hdf5file)

        # initialize figure
        self.resetFigure()

        self.viewer = None
        self.slice = None
        self.constants = {}
        self._cmapNorm = None
        self._interpolation = 'nearest'
        self._weights = (50, 50)
        self._updateWeights()
        self._histfig = None
        self._histAx = None

        if 'imL_0' in self.dataset.coords:
            self._axes = ('imL_0', 'wfmL_0')
        else:
            self._axes = ('e0_0', 'e0_1')
        self._makeSlice()
        self.updatePlot()

    def resetFigure(self):
        """ Completely remake a figure. Useful when one accidentally closes
            a pyplot figure.
        """

        self.fig = plt.figure()
        self.ax = self.fig.add_axes([0.075, 0.1, 0.9, 0.85])
        self.__chooseMode = False
        self.__displayCid = 0
        self.__chooseModeCid = self.fig.canvas.mpl_connect(
            'key_press_event', self.__setChooseMode)

    def _updateWeights(self):
        self.dataArray = (self.dataset.dzetasC * self.weights[0] +
                          self.dataset.dzetasP * self.weights[1]) / sum(self.weights)
        self.globalMin = self.dataArray.isel(self.dataArray.argmin(...))

    def __setChooseMode(self, cbEvent):
        """
        (De)activate interactive simulation displays for matrix points.
        This method is invoked when pressing 'i' while focused on figure.

        """

        if self.__chooseMode and cbEvent.key == 'i':
            self.fig.canvas.mpl_disconnect(self.__displayCid)
            self.__chooseMode = False
            print('Choose mode off')
        elif cbEvent.key == 'i':
            self.__chooseMode = True
            self.__displayCid = self.fig.canvas.mpl_connect(
                'button_press_event', self.__displayEvent)
            print('Choose mode on; choose point to display event',
                  flush=True)

    def __displayEvent(self, cbEvent):
        """
        Display simulation with parameters set by clicking on the matrix point.
        """
        pass

        # get parameter indices of clicked point
        x = cbEvent.xdata
        y = cbEvent.ydata
        # spawn EventViewer if closed before/never spawned

        if not self.viewer:
            self.spawnViewer()

        coords = self.slice.sel({self.axes[1]: x,
                                 self.axes[0]: y}, method='nearest').coords
        subparams = self.dataset.bestSubparams.sel(coords)

        self._setTracks(coords)
        self._setSubparams(subparams)

        dzetaSlice = self.slice.sel({self.axes[1]: x,
                                     self.axes[0]: y}, method='nearest').values
        dzetaCalcC = 1000 * np.sum(
            (self.viewer.simImage - self.viewer.expimage.image)**2)
        dzetaCalcP = 1000 * np.sum(
            (self.viewer.simWfm - self.viewer.expwfm.wfm)**2)
        print('Dzeta from matrix: {}'.format(dzetaSlice))
        print('Image matrix:')
        print('Dzeta recalculated:{}'.format(dzetaCalcC))

        print('Wfm matrix:')
        print('Dzeta recalculated:{}'.format(dzetaCalcP))

        plt.draw_all()  # needed for ipython to update

    def _setTracks(self, coords):
        if 'e0_0' in self.slice.coords:  # ephi coords
            for i in range(int(self.dataset.attrs['particlecount'])):
                e0 = coords['e0_{}'.format(i)].item()
                phi = coords['phi_{}'.format(i)].item()
                try:
                    theta = coords['theta_{}'.format(i)].item()
                    self.viewer.tracks[i].setParameters(
                        e0=e0, phi=phi, theta=theta)
                except KeyError:
                    self.viewer.tracks[i].setParameters(
                        e0=e0, phi=phi)
        elif 'imL_0' in self.slice.coords:  # lenlen coords
            for i in range(int(self.dataset.attrs['particlecount'])):
                imL = coords['imL_{}'.format(i)].item()
                wfmL = coords['wfmL_{}'.format(i)].item()
                phiSgn = coords['phiSgn_{}'.format(i)].item()
                try:
                    theta = coords['theta_{}'.format(i)].item()
                    self.viewer.tracks[i].theta = theta
                except KeyError:
                    pass
                self.viewer.tracks[i].setLengths(imL, wfmL, phiSgn > 0)
        # self._setSubparams(sliceCoords)

    def _setSubparams(self, subparamDic):
        print(subparamDic.sel(subparam='x0'))
        self.viewer.x0 = int(subparamDic.sel(subparam='x0'))
        self.viewer.y0 = int(subparamDic.sel(subparam='y0'))
        self.viewer.z0 = subparamDic.sel(subparam='z0').item()
        print(subparamDic.sel(subparam='sigmaC').item())
        self.viewer.sigmaC = subparamDic.sel(subparam='sigmaC').item()
        self.viewer.sigmaW = subparamDic.sel(subparam='sigmaP').item()
        self.viewer.scaleC = subparamDic.sel(subparam='scaleC').item()
        self.viewer.scaleW = subparamDic.sel(subparam='scaleP').item()
        self.viewer.updateSimulation()

    def spawnViewer(self):
        """
        Spawn an EventViewer instance, load event and a simulation.

        This method uses scanner config file to ensure same conditions
        as when making parameter scan.
        """

        self.viewer = EventViewer()
        self.viewer.x0 = 0
        self.viewer.y0 = 0

        # load event waveform
        self.viewer.loadEventWfm(path=self.dataset.attrs['pmtpath'],
                                 wfmStart=float(self.dataset.attrs['pmtfragmentstart']),
                                 wfmStop=float(self.dataset.attrs['pmtfragmentstop']))
        self.viewer.expwfm.sigma = float(self.dataset.attrs['pmtsmoothsigma'])
        self.viewer.expwfm.bg = int(self.dataset.attrs['pmtbglevel'])

        # load event image
        self.viewer.loadEventImage(self.dataset.attrs['imagepath'])
        self.viewer.binning = int(self.dataset.attrs['camerabinning'])
        self.viewer.expimage.sigma = float(self.dataset.attrs['imagesmoothsigma'])

        for i in range(int(self.dataset.attrs['particlecount'])):
            track = Track(ptype=self.dataset.attrs['particle{}'.format(i + 1)])
            self.viewer.addTrack(track)

    def __findNearest(self, array, value):
        array = np.asarray(array)
        idx = (np.abs(array - value)).argmin()
        return array[idx]

    def updatePlot(self):
        """ Redraw slice on the grid.
            Grid must be first created using 'makeGrid' method"""

        xCoords = self.slice[self.axes[1]].data
        yCoords = self.slice[self.axes[0]].data
        self.ax.cla()
        self.ax.set_xlabel(self.axes[1])
        self.ax.set_ylabel(self.axes[0])
        # self.slice = self.slice.transpose(self.axes[1], self.axes[0])
        self.ax.imshow(self.slice.data,
                       cmap='hot',
                       aspect='auto',
                       norm=Normalize(
                           *self.cmapNorm) if self.cmapNorm else None,
                       extent=(xCoords[0] - (xCoords[1] - xCoords[0]) / 2,
                               xCoords[-1] + (xCoords[-1] - xCoords[-2]) / 2,
                               yCoords[-1] + (yCoords[-1] - yCoords[-2]) / 2,
                               yCoords[0] - (yCoords[1] - yCoords[0]) / 2),
                       interpolation=self.interpolation)

    def update(self):
        """ Update both slice and plot.
        """

        # self.updateSlice()
        self.updatePlot()

    @property
    def weights(self):
        return self._weights

    @weights.setter
    def weights(self, value):
        self._weights = value
        self._updateWeights()
        self._makeSlice()
        self.updatePlot()

    @property
    def axes(self):
        return self._axes

    @axes.setter
    def axes(self, value):
        self._axes = value
        self._makeSlice()
        self.updatePlot()

    @property
    def cmapNorm(self):
        return self._cmapNorm

    @cmapNorm.setter
    def cmapNorm(self, value):
        self._cmapNorm = value
        self.updatePlot()

    @property
    def interpolation(self):
        return self._interpolation

    @interpolation.setter
    def interpolation(self, value):
        self._interpolation = value
        self.updatePlot()

    def _makeSlice(self):
        # self.slice = self.dataArray.sel(self.globalMin.drop_vars(self._axes).coords)
        da = self.dataArray
        da = da.sel(self.constants, method='nearest')
        for coord in self.dataArray.drop_vars(self.axes + tuple(self.constants.keys())).coords:
            idx = da.idxmin(coord)
            da = da.sel({coord: idx})
        self.slice = da

    def subparamHist(self, subparam):
        if not self._histfig:
            self._histfig, self._histAx = plt.subplots()
        self._histAx.cla()
        subparamSet = self.dataset.bestSubparams.sel(subparam=subparam).to_numpy().flatten()
        self._histAx.hist(subparamSet)

    def setCombinedMinimum(self):
        if not self.viewer:
            return
        cameraMin = self.dataset.dzetasC.isel(self.dataset.dzetasC.argmin(...))
        pmtMin = self.dataset.dzetasP.isel(self.dataset.dzetasP.argmin(...))

        imL = [0] * int(self.dataset.attrs['particlecount'])
        wfmL = [0] * int(self.dataset.attrs['particlecount'])
        theta = [0] * int(self.dataset.attrs['particlecount'])
        phiSgn = [0] * int(self.dataset.attrs['particlecount'])
        coords = {}
        for i in range(int(self.dataset.attrs['particlecount'])):
            imL[i] = cameraMin.coords['imL_{}'.format(i)].item()
            theta[i] = cameraMin.coords['theta_{}'.format(i)].item()
            wfmL[i] = pmtMin.coords['wfmL_{}'.format(i)].item()
            phiSgn[i] = pmtMin.coords['phiSgn_{}'.format(i)].item()
            self.viewer.tracks[i].setLengths(imL[i], wfmL[i], phiSgn[i] > 0)
            self.viewer.tracks[i].theta = theta[i]
            coords[f'imL_{i}'] = imL[i]
            coords[f'wfmL_{i}'] = wfmL[i]
            coords[f'phiSgn_{i}'] = phiSgn[i]
            coords[f'theta_{i}'] = theta[i]

        self.viewer.x0 = int(self.dataset.bestSubparams.sel(coords).sel(subparam='x0').item())
        self.viewer.y0 = int(self.dataset.bestSubparams.sel(coords).sel(subparam='y0').item())

        self.viewer.z0 = float(self.dataset.bestSubparams.sel(coords).sel(subparam='z0').to_numpy().flatten()[0])
        self.viewer.sigmaC = float(self.dataset.bestSubparams.sel(coords).sel(subparam='sigmaC').item())
        self.viewer.sigmaW = float(self.dataset.bestSubparams.sel(coords).sel(subparam='sigmaP').to_numpy().flatten()[0])
        self.viewer.scaleC = float(self.dataset.bestSubparams.sel(coords).sel(subparam='scaleC').item())
        self.viewer.scaleW = float(self.dataset.bestSubparams.sel(coords).sel(subparam='scaleP').to_numpy().flatten()[0])
