import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import argparse as agp


def generateLossProfile(energyF, rangeF, E0, binSize=672, length=512, rangemin=39.39, nuclCorrF=None):
    """
    Generate dE/dx profile using energy before/after bin method 
    for given bin size and initial energy.

    Arguments:
        energyF: function of energy vs. range, for example an output
            from scipy.interpolation.interpolate1d function
        rangeF: funcion of range vs. energy
        E0: initial particle energy in keV
        binSize: bin size in um. Energy loss will be calculated per bin.
        length: length of the output array. Make sure the array is longer
            than the loss profile for defined bins
        rangemin: minimum range in SRIM output tables. When calculation
            reaches this value the remaining energy is deposited
            in the following bin and calculation stops.
            This method is unreliable when binSize is smaller
            than rangeMin

    Returns:
        numpy.ndarray:   1D array of size given by length containing energy loss profile
    """

    output = np.zeros(length)
    i = 0
    penergy = E0
    prange = rangeF(penergy)
    while (prange - binSize > rangemin):
        dE = penergy - energyF(prange - binSize)
        output[i] = dE * nuclCorrF(penergy - dE) if nuclCorrF is not None else dE
        penergy -= dE
        prange -= binSize
        i += 1
        if i == output.size:
            raise IndexError(
                "Loss profile too long for specified output length")
    output[i] = penergy
    return output


def makeLossProfileVec(energyF, rangeF, E0, binSize=672, length=512, rangemin=39.39, nuclCorrF=None):
    range0 = rangeF(E0)
    ranges = np.arange(range0, 0, -binSize)

    # if last range smaller than interpolation range
    if ranges[-1] < rangemin:
        ranges[-1] = rangemin + 0.01

    energies = energyF(ranges)
    if nuclCorrF:
        corrections = nuclCorrF(energies)
    dE = energies - np.roll(energies, -1)
    dE[-1] = energies[-1]
    if nuclCorrF:
        dE = dE * corrections
    return dE
