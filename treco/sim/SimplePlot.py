import numpy as np
import matplotlib.pyplot as plt


class SimplePlot:
    """ Class handling quick and simple plotting of OTPC image & wfm."""

    def __init__(self):
        self.imFig = None
        self.wfmFig = None

    def plotImage(self):
        if not self.imFig:
            self.imFig, self.imAx = plt.subplots(1, 1)
            self.imFig.canvas.mpl_connect('close_event', self.onCloseIm)
        else:
            self.imAx.clear()
        self.imAx.imshow(self.image, cmap='hot')

    def plotWfm(self):
        if not self.wfmFig:
            self.wfmFig, self.wfmAx = plt.subplots(1, 1)
            self.wfmFig.canvas.mpl_connect('close_event', self.onCloseWfm)
        else:
            self.wfmAx.clear()
        self.wfmAx.plot(self.wfm)

    def onCloseIm(self, event):
        self.imFig = None

    def onCloseWfm(self, event):
        self.wfmFig = None

    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, value):
        self._image = value
        if self.imFig:
            self.plotImage()

    @property
    def wfm(self):
        return self._wfm

    @wfm.setter
    def wfm(self, value):
        self._wfm = value
        if self.wfmFig:
            self.plotWfm()
