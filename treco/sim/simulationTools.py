import numpy as np
import math
from scipy.ndimage import rotate, gaussian_filter
import cv2 as cv


def imageFromLossOlder(loss,
                       size: tuple[int, int],
                       theta: float,
                       x0: int,
                       y0: int,
                       sigma: float = 0,
                       padded: bool = False
                       ):
    """
    Generate image based on loss profile and theta angle.
    Phi angle should be already included in loss profile.

    Arguments:
        loss:  array of energy loss profile.
               Should already be corrected for phi angle and pixel size
        size:  size of an output image in px. Format: (width,height).
               If loss profile is longer than 1.5x width/height
               (whichever is bigger), it will be trimmed.
        theta: angle of the track on camera in degrees.
               0 is from left to right, 90 from bottom to top.
        x0,y0: initial position of the track in px.
               x is from left to right, y from top to bottom.
        padded: if true, returned image is padded with size * 3 on all sides.
    Returns:
        image: 2D array of size 'size' representing image.
    """
    # for worst case scenario when theta=45 and track is as long as diagonal
    initSize = (math.ceil(3 * max(size)), math.ceil(3 * max(size)))
    p = np.zeros(initSize)
    middlePoint = (initSize[0] // 2, initSize[1] // 2)
    if loss.size > middlePoint[1]:
        loss = loss[:middlePoint[1]]
    p[middlePoint[0], middlePoint[1]:middlePoint[1] + loss.size] = loss
    p = rotate(p, theta, reshape=False)
    if x0 not in range(0, size[1]) or y0 not in range(0, size[0]):
        raise ValueError('''Track coordinates ({},{}) out of range
                            of the image of size ({},{})'''.format(
            x0, y0, size[0], size[1]))
    if padded:
        image = p
    else:
        image = p[initSize[0] // 2 - y0:initSize[0] // 2 - y0 + size[0],
                  initSize[1] // 2 - x0:initSize[1] // 2 - x0 + size[1]]
    if sigma > 0:
        image = gaussian_filter(image, sigma, truncate=8.0)
    return image


def imageFromLossOld(loss,
                     size: tuple[int, int],
                     theta: float,
                     x0: int,
                     y0: int):
    protoIm = np.zeros((5, 2 * (loss.size + 2)))
    protoIm[2, loss.size + 2: -2] = loss
    protoIm = rotate(protoIm, theta)
    if theta % 360 < 90:
        protoIm = protoIm[:protoIm.shape[0] // 2 + 3, protoIm.shape[1] // 2 - 2:]  # now x0=2, y0=2
        shiftX = 2
        shiftY = protoIm.shape[0] - 3
    elif theta % 360 < 180:
        protoIm = protoIm[:protoIm.shape[0] // 2 + 2, :protoIm.shape[1] // 2 + 3]  # now x0=2, y0=size-2
        shiftX = protoIm.shape[1] - 3
        shiftY = protoIm.shape[0] - 2
    elif theta % 360 < 270:
        protoIm = protoIm[protoIm.shape[0] // 2 - 2:, :protoIm.shape[1] // 2 + 2]  # now x0=2, y0=size-2
        shiftX = protoIm.shape[1] - 3
        shiftY = 2
    else:
        protoIm = protoIm[protoIm.shape[0] // 2 - 2:, protoIm.shape[1] // 2 - 2:]  # now x0=2, y0=size-2
        shiftX = 2
        shiftY = 2
    image = np.zeros(size)
    try:
        image[:protoIm.shape[0], :protoIm.shape[1]] = protoIm
    except ValueError:
        raise ValueError('Image size too small to fit the track')
    image = np.roll(image, x0 - shiftX, 1)
    image = np.roll(image, y0 - shiftY, 0)
    return image


def imageFromLossCVSimple(loss,
                          size: tuple[int, int],
                          theta: float,
                          x0: int = None,
                          y0: int = None,
                          returnProtoIm: bool = False):
    """
    Reprise of the imageFromLossOlder algorithm, but using openCV.

    Arguments:
        loss:            energy loss profile as 1D array
        size:            size of the output image (width, length)
        theta:           angle of the track in degrees (anticlockwise). theta=0 if to the right.
        x0, y0:          position of the start of the track in pixels. (0,0) is top left hand corner.
        returnProtoIm :  if True, function returns image of size 3x given size and track in the middle,
                         regardless of given x0 & y0.
                         Useful if x0 and y0 are positioned elsewhere
    Returns:
        image:           2D array of pixel values.

    """
    if x0 not in range(0, size[1]) or y0 not in range(0, size[0]):
        raise ValueError('''Track coordinates ({},{}) out of range
                            of the image of size ({},{})'''.format(
            x0, y0, size[0], size[1]))

    protoWidth = (max(size) * 3, max(size) * 3)
    protoIm = np.zeros(protoWidth, dtype=np.float64)
    middlePoint = (protoWidth[0] // 2, protoWidth[1] // 2)
    if loss.size > middlePoint[1]:
        # loss = loss[:middlePoint[1]]
        raise ValueError('Track too long for image of given size')
    protoIm[middlePoint[0], middlePoint[1]:middlePoint[1] + loss.size] = loss
    matrix = cv.getRotationMatrix2D(center=middlePoint, angle=theta, scale=1)
    protoIm = cv.warpAffine(protoIm, matrix, dsize=protoIm.shape)

    if returnProtoIm:
        return protoIm

    image = protoIm[protoWidth[0] // 2 - y0:protoWidth[0] // 2 - y0 + size[0],
                    protoWidth[1] // 2 - x0:protoWidth[1] // 2 - x0 + size[1]]
    return image


def imageFromLossCVPrototype(loss,
                             size: tuple[int, int],
                             theta: float,
                             x0: int,
                             y0: int):

    protoImWidth = math.ceil(loss.size + 4)  # 2 px safety border
    protoIm = np.zeros((protoImWidth, protoImWidth))
    if theta % 360 < 90:
        protoIm[-2, 2:2 + loss.size] = loss
        shiftX = 2
        shiftY = protoIm.shape[0] - 2
        matrix = cv.getRotationMatrix2D(center=(shiftX, shiftY), angle=theta, scale=1)
    elif theta % 360 < 180:
        protoIm[-2, -loss.size - 2:-2] = np.flip(loss)
        shiftX = protoIm.shape[1] - 3
        shiftY = protoIm.shape[0] - 2
        matrix = cv.getRotationMatrix2D(center=(shiftX, shiftY), angle=-(180 - theta), scale=1)
    elif theta % 360 < 270:
        protoIm[2, -loss.size - 2:-2] = np.flip(loss)
        shiftX = protoIm.shape[1] - 3
        shiftY = 2
        matrix = cv.getRotationMatrix2D(center=(shiftX, shiftY), angle=theta - 180, scale=1)
    else:
        protoIm[2, 2: 2 + loss.size] = loss
        shiftX = 2
        shiftY = 2
        matrix = cv.getRotationMatrix2D(center=(shiftX, shiftY), angle=theta, scale=1)

    if size[1] > protoIm.shape[0] and size[0] > protoIm.shape[1]:
        image = cv.warpAffine(protoIm, matrix, dsize=(size[1], size[0]))
        image = np.roll(image, x0 - shiftX, 1)
        image = np.roll(image, y0 - shiftY, 0)
    else:
        image = cv.warpAffine(protoIm, matrix, dsize=(protoIm.shape))
        image = np.roll(image, x0 - shiftX, 1)
        image = np.roll(image, y0 - shiftY, 0)
        image = image[:size[1], :size[0]]

    return image


def pmtFromLoss(loss,
                size: int,
                z0: int,
                flip: bool = False,
                padded: bool = False):
    """
    Generates simulated PMT waveform based on loss profile

    Arguments:
        loss:  array of energy loss profile. Should already
               be corrected for phi angle
        size:  number of samples in a waveform in samples
        z0:    initial position of the track on waveform in samples.
               0 is in the middle of the waveform.
        flip:  if True, waveform is mirrored (for positive phi angles).
        padded: if True, returned waveform is padded with size // 2 zeros on both sides
                      and z0 is ignored (using default z0=0)

    Returns:
        wfm:   Array of length 'size' (or size * 2 with returnPadded) representing track waveform
    """
    wfm = np.zeros(size * 2)
    if loss.size > size:
        loss = loss[:size]
    if flip:
        loss = np.flip(loss)
        wfm[size - loss.size:size] = loss
    else:
        wfm[size: size + loss.size] = loss
    if padded:
        return wfm
    else:
        return wfm[size // 2 - z0: size + size // 2 - z0]


#imageFromLoss = imageFromLossCVSimple
imageFromLoss = imageFromLossOlder
