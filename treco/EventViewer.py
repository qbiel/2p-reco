import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import os
import json
import math

from .Track import Track
from .Simulators import ImageSimulator, WfmSimulator
from . import constants as const
from .utils.ExpImage import ExpImage
from .utils.ExpWaveform import ExpWaveform


class EventViewer(ImageSimulator, WfmSimulator):
    """
    Interactive tool for comparing model events with experimental data.
    """

    def __init__(self,
                 paramFile: str = '',
                 x0: int = const.x0,
                 y0: int = const.y0,
                 z0: float = const.z0,
                 imSize: tuple = const.imSize,
                 binning: int = const.binning,
                 pixelSize: int = const.pixelWidth,
                 samplingRate: int = const.sampling,
                 wfmSize: int = const.wfmSize,
                 wfmStart: float = const.wfmStart,
                 driftVelocity: int = const.driftVel,
                 imSigma: float = const.sigmaC,
                 wfmSigma: float = const.sigmaW,
                 scaleC: float = const.scaleC,
                 scaleW: float = const.scaleW):

        ImageSimulator.__init__(self,
                                x0=x0,
                                y0=y0,
                                imSize=imSize,
                                binning=binning,
                                sigma=imSigma,
                                scale=scaleC,
                                pixelSize=pixelSize,
                                simulateProjections=True)
        WfmSimulator.__init__(self,
                              z0=z0,
                              samplingRate=samplingRate,
                              wfmSize=wfmSize,
                              wfmStart=wfmStart,
                              driftVelocity=driftVelocity,
                              sigma=wfmSigma,
                              scale=scaleW)

        self.tracks = []
        self.expimage = None
        self.expwfm = None

        self._singleWfmLines = {}

        self._autoscale = True

        self.resetPlots()
        if paramFile:
            self.loadParams(paramFile)

    def resetPlots(self):
        """Remake figure from scratch, for example if closed accidentaly"""

        self.fig = plt.figure(figsize=(15.4, 9.6))
        self.gs = self.fig.add_gridspec(
            ncols=7, nrows=4,
            wspace=0.2, hspace=0.2,
            left=0.05, right=0.95)
        boxProps = dict(boxstyle='round', facecolor='white', alpha=0.9)
        self.simImageAx = self.fig.add_subplot(self.gs[:3, :3])
        self.simImageAx.text(0.05, 0.95, 'Model',
                             transform=self.simImageAx.transAxes,
                             bbox=boxProps)
        self.eventImageAx = self.fig.add_subplot(self.gs[:3, 4:])
        self.eventImageAx.text(0.05, 0.95, 'Experiment',
                               transform=self.eventImageAx.transAxes,
                               bbox=boxProps)
        self.horizProjAx = self.fig.add_subplot(self.gs[3, :3])
        self.horizProjAx.set_xlabel('px')
        self.vertProjAx = self.fig.add_subplot(self.gs[:3, 3])
        self.vertProjAx.set_ylabel('px')
        self.wfmAx = self.fig.add_subplot(self.gs[3:, 4:])
        self.wfmAx.set_xlabel('Time [\u03BCs]')
        self.wfmAx.text(0.05, 0.85, 'Wfm',
                        transform=self.wfmAx.transAxes,
                        bbox=boxProps)
        self.legendAx = self.fig.add_subplot(self.gs[3, 3], frameon=False)
        self.legendAx.axis('off')

        self.axes = (self.simImageAx, self.eventImageAx,
                     self.horizProjAx, self.vertProjAx, self.wfmAx)

        self.wfmX = np.linspace(self._wfmStart,
                                self._wfmStop,
                                math.floor((self._wfmStop - self._wfmStart) * self.sampling), endpoint=False)

        self._pltImage = self.simImageAx.imshow([[0]], cmap='hot')
        self._vProjLine, = self.vertProjAx.plot([], [], label='Model')
        self._hProjLine, = self.horizProjAx.plot([], [], label='Model')
        self._vexpProjLine, = self.vertProjAx.plot([], [], label='Experiment')
        self._hexpProjLine, = self.horizProjAx.plot([], [], label='Experiment')
        self._wfmLine, = self.wfmAx.plot([], [], label='Model', zorder=3)
        self._pltExpImage = self.eventImageAx.imshow([[0]], cmap='hot', aspect='equal')
        self._expWfmLine, = self.wfmAx.plot([], [], label='Experiment', zorder=2)

        self._singleWfmLines = {}
        self._singleProjLines = {}  # dict of tuples (vProjLine, hProjLine)
        for i, track in enumerate(self.tracks):
            self._singleWfmLines[track], = self.wfmAx.plot([], [], '--', label=f'Track {i}')
            self._singleProjLines[track] = (self.vertProjAx.plot([], [], '--', label=f'Track {i}')[0],
                                            self.horizProjAx.plot([], [], '--', label=f'Track {i}')[0])
        if len(self.tracks) == 1:
            trackObj = self.tracks[0]
            self._singleProjLines[trackObj][0].set_visible(False)
            self._singleProjLines[trackObj][1].set_visible(False)
            self._singleWfmLines[trackObj].set_visible(False)
        if self.tracks:
            self.updateSimulation()
        if self.expimage:
            self.expimage.refresh()
        if self.expwfm:
            self.expwfm.refresh()

        self.legendAx.legend(handles=[self._wfmLine, self._expWfmLine, *self._singleWfmLines.values()], loc='upper left')

    # somehow this breaks on too small portions (~ 20us)
    def loadEventImage(self,
                       path: str = '',
                       run: int = None,
                       event: int = None):
        """
        Load event image from data (png format).

        This method also changes model image size to match data image size.
        If path is not given, this method will search for raw image
        in data directory given in constants.py file.
        If path is given, event and run arguments will be ignored.

        Arguments:
            path: filepath of the image. If not specified, method will
                load raw image from run & event arguments (if given).
            run: run number
            event: event number
        """

        # get image from file
        if path:
            imPath = path
        elif run is not None and event is not None:
            eventImageName = 'R{0:03d}_{1:05d}'.format(run, event)
            imPath = eventImageName + '.new.png'
            imPath = os.path.join(const.runDirectory,
                                  eventImageName[0:4],
                                  imPath)
        self.expimage = ExpImage(imPath)

        # change simulation size to fit event image
        try:
            self.imSize = self.expimage._rawImage.shape
        except ValueError:
            self.x0 = self.expimage._rawImage.shape[1] // 2
            self.y0 = self.expimage._rawImage.shape[0] // 2
            self.imSize = self.expimage._rawImage.shape

        # connect Expimage to EventViewer
        self.expimage._connectViewer(self)
        self._listening = True
        self.expimage._notify()

    def loadEventWfm(self,
                     run: int = 0,
                     event: int = 0,
                     wfmStart: float = None,
                     wfmStop: float = None,
                     wfmChannel: int = 0,
                     flip: bool = True,
                     path: str = None,
                     filetype: str = None):
        """
        Load waveform event from data.

        This method also changes simulated waveform size
        and sampling rate to fit event waveform.

        WfmStart and wfmStop arguments allow to load portions of the waveform from a file.
        For instance, waveforms loaded with SmartLoad module are very long and usually have 0 us
        associated with the trigger. Therefore if a rough position of the track is known (e.g. between 708
        and 710 us from the trigger), one can load only a portion of a raw data waveform containing a track
        by setting wfmStart=708 and wfmStop=712.
        For .vec files and .npy arrays, the first point of the waveform is at 0 us.

        Arguments:
            run: run number. Only valid if filetype == 'sl'.
            event: event number. Only valid if filetype == 'sl'.
            wfmStart: timestamp of the first point of interest in a waveform in us
            wfmStop: timestamp of the last point of interest in a waveform in us
            wfmChannel: channel number in ADlink digitizer. Only used if filetype == 'sl'.
            flip: if True, original waveform is y-inverted
            path: path to .meta file if skipping run & event arguments
            filetype: type of the waveform file. If None, method
                will try to determinie the filetype by file extension
        """

        # clean previous event waveform if exists
        if self.expwfm:
            del(self.expwfm)

        # prepare path
        if path:
            self.eventName = path
            wfmPath = path
        else:
            self.eventName = 'R{0:03d}_{1:05d}'.format(run, event)
            wfmFile = self.eventName + '.new_scope.meta'
            wfmPath = os.path.join(const.runDirectory,
                                   self.eventName[0:4], wfmFile)

        # try to determine filetype
        if filetype is None:
            if wfmPath[-4:] == 'meta':
                filetype = 'sl'
            elif wfmPath[-3:] == 'vec':
                filetype = 'vec'
            elif wfmPath[-3:] == 'npy':
                filetype = 'npy'
            else:
                raise RuntimeError('Could not determine waveform filetype or filetype not supported')

        # load waveform. If filetype is not 'sl', current sampling is passed
        self.expwfm = ExpWaveform(path=wfmPath,
                                  start=wfmStart,
                                  stop=wfmStop,
                                  channel=wfmChannel,
                                  flip=flip,
                                  sampling=self.sampling,
                                  filetype=filetype)
        self.expwfm._connectViewer(self)

        # change simulation wfm size & sampling
        self.setWfmParameters(self.expwfm.sampling,
                              self.expwfm._rawwfm.size,
                              wfmStart=self.expwfm.wfmStart,
                              z0=self.expwfm.wfmStart + self.expwfm._rawwfm.size // 2 / self.expwfm.sampling)
        self.expwfm._connectViewer(self)
        self._listening = True
        self.expwfm._notify()

    def rescale(self, axes=None):
        if axes:
            axes.relim(visible_only=True)
            axes.autoscale()
        else:
            for ax in self.axes:
                ax.relim(visible_only=True)
                ax.autoscale()
        plt.pause(0.001)

    def updateSimulation(self):
        """
        Remake image & wfm simulations from single track images & wfms.
        """
        if len(self.tracks) == 0:
            self.simImage = np.zeros(self.imSize)
            self.simWfm = np.zeros(self.wfmSize)
            return
        self.updateImageSimulation()
        self.updateWfmSimulation()
        self._updateSingleTrackProjections()

    def _updateSingleTrackProjections(self):

        projectXH = np.arange(self.simImage.shape[1])
        projectXV = np.arange(self.simImage.shape[0], 0, -1)

        if self.simulateProjections:
            for trackObj in self.tracks:
                trackObj.vertProj = np.sum(self._unpadImage(trackObj.image), 1)
                trackObj.horizProj = np.sum(self._unpadImage(trackObj.image), 0)

                self._singleWfmLines[trackObj].set_data(self.wfmX,
                                                        self._unpadWfm(trackObj.wfm) * self.scaleW / 2)
                self._singleProjLines[trackObj][0].set_data(trackObj.vertProj * self.scaleC / 2, projectXV)
                self._singleProjLines[trackObj][1].set_data(projectXH, trackObj.horizProj * self.scaleC / 2)

    def addTrack(self, trackObj):
        """
        Add a track to the model.

            Arguments:
                trackObj: *treco.Track* object

        """
        self.tracks.append(trackObj)
        trackObj._connectViewer(self)
        self._listening = True

        self._singleWfmLines[trackObj], = self.wfmAx.plot(self.wfmX[0], 0, '--', visible=False, label=f'Track {self.tracks.index(trackObj)}')
        self._singleProjLines[trackObj] = (self.vertProjAx.plot(0, 0, '--', visible=False, label=f'Track {self.tracks.index(trackObj)}')[0],
                                           self.horizProjAx.plot(0, 0, '--', visible=False, label=f'Track {self.tracks.index(trackObj)}')[0])

        trackObj.imageLoss = trackObj.makeImageLoss(self.pixelSize, self.binning)
        trackObj.image = trackObj.makeImage(trackObj.imageLoss,
                                            self.imSize,
                                            padded=True)[0]
        trackObj.wfmLoss = trackObj.makeWfmLoss(self.sampling,
                                                self.driftVelocity)
        trackObj.wfm = trackObj.makeWfm(trackObj.wfmLoss,
                                        self.wfmSize,
                                        padded=True)[0]
        self._wfmLine.set_visible(True)
        self.updateSimulation()
        self._updateSingleTrackProjections()

        if len(self.tracks) > 1:
            for trackObj in self.tracks:
                self._singleProjLines[trackObj][0].set_visible(True)
                self._singleProjLines[trackObj][1].set_visible(True)
                self._singleWfmLines[trackObj].set_visible(True)
        self.legendAx.legend(handles=[self._wfmLine, self._expWfmLine, *self._singleWfmLines.values()], loc='upper left')

    def removeTrack(self, trackObj):
        """
        Remove a track from the model.

        Arguments:
            trackObj: *treco.Track* object previously added to the model

        """
        trackObj._disconnectViewer(self)
        idx = self.tracks.index(trackObj)
        self.tracks.pop(idx)
        del(trackObj.wfmLoss)
        del(trackObj.wfm)
        del(trackObj.imageLoss)
        del(trackObj.image)
        imProjs = self._singleProjLines.pop(trackObj)
        self._singleWfmLines.pop(trackObj).remove()
        imProjs[0].remove()
        imProjs[1].remove()

        if not self.tracks and not self.expimage and not self.expwfm:
            self._listening = False

        if len(self.tracks) == 1:
            self._singleProjLines[self.tracks[0]][0].set_visible(False)
            self._singleProjLines[self.tracks[0]][1].set_visible(False)
            self._singleWfmLines[self.tracks[0]].set_visible(False)
        self.updateSimulation()
        self.legendAx.legend(handles=[self._wfmLine, self._expWfmLine, *self._singleWfmLines.values()], loc='upper left')

    def _recvNotification(self, sender, message):
        if self._listening:
            if message == 'loss':
                sender.wfmLoss = sender.makeWfmLoss(self.sampling,
                                                    self.driftVelocity)
                sender.wfm = sender.makeWfm(sender.wfmLoss,
                                            self.wfmSize,
                                            padded=True)[0]
                sender.imageLoss = sender.makeImageLoss(self.pixelSize, self.binning)
                sender.image = sender.makeImage(sender.imageLoss,
                                                self.imSize,
                                                padded=True)[0]
            elif message == 'image':
                sender.image = sender.makeImage(sender.imageLoss,
                                                self.imSize,
                                                padded=True)[0]
            elif message == 'expwfm':
                self._expWfmLine.set_data(self.wfmX, self.expwfm.wfm)
            elif message == 'expimage':
                self._pltExpImage.set_data(self.expimage.image)
                self._pltExpImage.set_extent((-0.5, self.imSize[1] - 0.5, self.imSize[0] - 0.5, -0.5))
                self._pltExpImage.set_clim(0, np.amax(self.expimage.image))
                projectXH = np.arange(self.simImage.shape[1])
                projectXV = np.arange(self.simImage.shape[0], 0, -1)
                self._vexpProjLine.set_data(self.expimage.vProj, projectXV)
                self._hexpProjLine.set_data(projectXH, self.expimage.hProj)
            self.updateSimulation()

    @WfmSimulator.z0.setter
    def z0(self, value):
        WfmSimulator.z0.fset(self, value)
        self._updateSingleTrackProjections()

    @ImageSimulator.x0.setter
    def x0(self, value):
        ImageSimulator.x0.fset(self, value)
        self._updateSingleTrackProjections()

    @ImageSimulator.y0.setter
    def y0(self, value):
        ImageSimulator.y0.fset(self, value)
        self._updateSingleTrackProjections()

    def saveParams(self, filepath):
        """
        Save all image & waveform parameters, event filepaths
        as well as tracks and their parameters to JSON file.
        """

        params = {}

        imageSim = {
            'binning': self.binning,
            'imSize': self.imSize,
            'pixelSize': self.pixelSize,
            'sigmaC': self.sigmaC,
            'scaleC': self.scaleC,
            'x0': int(self.x0),
            'y0': int(self.y0),
        }
        params['imageSim'] = imageSim

        wfmSim = {
            'driftVelocity': self.driftVelocity,
            'sampling': self.sampling,
            'sigmaW': self.sigmaW,
            'scaleW': self.scaleW,
            'wfmSize': self.wfmSize,
            'wfmStart': self.wfmStart,
            'wfmStop': self._getWfmStop(),
            'z0': self.z0,
        }
        params['wfmSim'] = wfmSim

        tracks = []
        for track in self.tracks:
            tracks.append({
                'srimFile': os.path.abspath(track.srimFile),
                'type': track.type,
                'e0': track.e0,
                'theta': track.theta,
                'phi': track.phi,
                'range': track.range,
                'imLength': track.imLength,
                'wfmLength': track.wfmLength,
                'vector': list(track.vector),
            })
        params['tracks'] = tracks

        if self.expimage:
            eventImage = {
                'path': os.path.abspath(self.expimage.path),
                'sigma': self.expimage.sigma,
                'bg': self.expimage.bg,
                'scale': float(self.expimage.scale),
            }
        else:
            eventImage = {}

        if self.expwfm:
            eventWfm = {
                'path': os.path.abspath(self.expwfm.path),
                'sigma': self.expwfm.sigma,
                'scale': float(self.expwfm.scale),
                'bg': self.expwfm.bg,
            }
        else:
            eventWfm = {}

        params['eventImage'] = eventImage
        params['eventWfm'] = eventWfm

        with open(filepath, 'w') as f:
            json.dump(params, f, indent=4)

    def loadParams(self, file):
        """
        Load parameters from JSON file.

        Not optimized yet.
        """

        autoscale = self.autoscale
        self.autoscale = False

        with open(file, 'r') as f:
            params = json.load(f)

        # remove previous tracks
        for track in self.tracks.copy():
            self.removeTrack(track)

        self.x0 = 0  # for safety
        self.y0 = 0

        # image parameters
        if params['eventImage']:
            self.loadEventImage(params['eventImage']['path'])
            self.expimage.sigma = params['eventImage']['sigma']
            self.expimage.bg = params['eventImage']['bg']
            self.expimage.scale = params['eventImage']['scale']
        else:
            self.imSize = tuple(params['imageSim']['imSize'])

        self.binning = params['imageSim']['binning']
        self.pixelSize = params['imageSim']['pixelSize']
        self.sigmaC = params['imageSim']['sigmaC']
        self.scaleC = params['imageSim']['scaleC']
        self.x0 = params['imageSim']['x0']
        self.y0 = params['imageSim']['y0']

        # waveform parameters
        if params['eventWfm']:
            self.loadEventWfm(path=params['eventWfm']['path'],
                              wfmStart=params['wfmSim']['wfmStart'],
                              wfmStop=params['wfmSim']['wfmStop'])
            self.expwfm.sigma = params['eventWfm']['sigma']
            self.expwfm.bg = params['eventWfm']['bg']
            self.expwfm.scale = params['eventWfm']['scale']
        else:
            self.wfmSize = params['wfmSim']['wfmSize']
            self.wfmStart = params['wfmSim']['wfmStart']
            self.sampling = params['wfmSim']['sampling']

        self.sigmaW = params['wfmSim']['sigmaW']
        self.scaleW = params['wfmSim']['scaleW']
        self.z0 = params['wfmSim']['z0']

        # create new tracks and set their parameters
        for trackDic in params['tracks']:
            track = Track(ptype=trackDic['type'],
                          e0=trackDic['e0'],
                          phi=trackDic['phi'],
                          theta=trackDic['theta'])
            self.addTrack(track)
        self.autoscale = autoscale

    @property
    def dzetaC(self):
        return np.sum((self.simImage - self.expimage.image)**2)

    @property
    def dzetaW(self):
        return np.sum((self.simWfm - self.expwfm.wfm)**2)

    def measureImage(self):
        """
        Measure track length and theta angle by clicking two points on the image.

        Returns:
            (float, float): tuple containing:
                imL: distance between two points in micrometers
                theta: angle of the line between two points in degrees
        """

        points = np.array(plt.ginput(2))

        if len(points) != 2:
            return
        imL = np.sqrt(np.sum((points[0] - points[1])**2))
        imL = imL * self.pixelSize * self.binning
        dx, dy = points[1] - points[0]
        if dx > 0:
            theta = np.arctan(-dy / dx)
        else:
            theta = -np.arctan(dy / dx) + np.pi
        theta = theta * 180 / np.pi
        return imL, theta

    def measureWfm(self):
        """
        Measure distance between two points on the waveform

        Returns:
            wfmL: distance between two clicked points on the waveform in us
        """
        points = plt.ginput(2)

        if len(points) != 2:
            return
        wfmL = abs(points[0][0] - points[1][0])
        wfmL = wfmL * self.driftVelocity
        return wfmL

    @property
    def simWfm(self):
        return self._simWfm

    @simWfm.setter
    def simWfm(self, value):
        self._simWfm = value
        self._wfmLine.set_data(self.wfmX, self.simWfm)
        self._updateSingleTrackProjections()
        if self.autoscale:
            self.rescale()

    @property
    def simImage(self):
        return self._simImage

    @simImage.setter
    def simImage(self, value):
        self._simImage = value

        # update projections
        self.horizProj = np.sum(self._simImage, 0)
        self.vertProj = np.sum(self._simImage, 1)
        self._updateSingleTrackProjections()

        # update simImage related plots
        self._pltImage.set_data(self._simImage)
        self._pltImage.set_extent((-0.5, self.imSize[1] - 0.5, self.imSize[0] - 0.5, -0.5))
        self._pltImage.set_clim(0, np.amax(self._simImage))
        projectXH = np.arange(self._simImage.shape[1])
        self._hProjLine.set_data(projectXH, self.horizProj)
        projectXV = np.arange(self._simImage.shape[0], 0, -1)
        self._vProjLine.set_data(self.vertProj, projectXV)
        if self.autoscale:
            self.rescale()

    @property
    def autoscale(self):
        return self._autoscale

    @autoscale.setter
    def autoscale(self, value):
        self._autoscale = value
        if value is True:
            self.rescale()

    def normalize(self,
                  target: str = 'model',
                  norm='amplitude',
                  signal='both'):
        """
        Normalize model with respect to the data (or vice versa).

        Arguments:
            target: target dataset to be normalized; 'model' or 'data'
            norm: normalization type; 'amplitude' or 'area'
            signal: signal to be normalized; 'image', 'waveform' or 'both'
        """

        if target == 'model':
            if self.expimage and signal in ('both', 'image'):
                if norm == 'amplitude':
                    self.scaleC *= np.amax(self.expimage.image) / np.amax(self.simImage)
                elif norm == 'area':
                    self.scaleC *= np.sum(self.expimage.image) / np.sum(self.simImage)
            if self.expwfm and signal in ('both', 'wfm'):
                if norm == 'amplitude':
                    self.scaleW *= np.amax(self.expwfm.wfm) / np.amax(self.simWfm)
                elif norm == 'area':
                    self.scaleW *= np.sum(self.expwfm.wfm) / np.sum(self.simWfm)
        elif target == 'data':
            if self.expimage and signal in ('both', 'image'):
                if norm == 'amplitude':
                    self.expimage.scale *= np.amax(self.simImage) / np.amax(self.expimage.image)
                elif norm == 'area':
                    self.expimage.scale *= np.sum(self.simImage) / np.sum(self.expimage.image)
            if self.expwfm and signal in ('both', 'wfm'):
                if norm == 'amplitude':
                    self.expwfm.scale *= np.amax(self.simWfm) / np.amax(self.expwfm.wfm)
                elif norm == 'area':
                    self.expwfm.scale *= np.sum(self.simWfm) / np.sum(self.expwfm.wfm)
    def flip(self, trackObj1, trackObj2):
        """
        Flip mapping of image track to wfm track.

        Arguments:
            trackObj1, trackObj2 - treco.Track objects connected to EventViewer instance
        """
        il1 = trackObj1.imLength
        wl1 = trackObj1.wfmLength
        p1 = trackObj1.phi > 0
        il2 = trackObj2.imLength
        wl2 = trackObj2.wfmLength
        p2 = trackObj2.phi > 0

        trackObj1.setLengths(il1, wl2, p2)
        trackObj2.setLengths(il2, wl1, p1)