Details on reconstruction
=============================

The following section contains detailed, step-by-step explanation of the process of event reconstruction.
It is more maths and physics oriented, but contains few programming details. For more implementation oriented description, visit :ref:`API reference <api>` and usage guide.


.. toctree::
   :maxdepth: 2
   :caption: Reconstruction steps:

   details/model
   details/fitting