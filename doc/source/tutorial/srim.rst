Generating SRIM tables
========================

`SRIM <https://www.srim.org/>`_ is a tool used for calculating energy loss information of a projectile in a target. The current version of *treco* relies heavily on SRIM as energy/range tables from SRIM are used for generating loss profiles.

Although powerful and arguably user friendly due to its graphical interface, SRIM is a poor tool for analyzing multiple gas mixtures. Threrfore *treco* ships with a module *treco.srimUtils* containing a set of tools for making SRIM more machine friendly. For generating a single SRIM table it is probably easier to use an original graphical interface, but for choosing a gas mixture pre-experiment or systematic error analysis *treco.srimUtils* can simplify the workflow.

*treco.srimUtils* works with SRModule.exe, an executable that ships with SRIM.
Detailed information can be found on SRIM website. In a few words, SRModule is SRIM without a GUI frontend. It reads ion and target data from SR.IN file and generates the same output table as the GUI would. *treco.srimUtils* can help in preparing multiple tables fast by generating SR.IN files via a Python interface. An experimental feature can also run SRModule.exe automatically (works on Linux only so far).

Let's try to generate a single SRIM table first. The following information must be provided:
 * target composition
 * target density
 * projectile name, charge and mass

.. note::
    Some constants, like elements atomic number, mass or gas densities at normal conditions are hardcoded in *Z*, *molarMass* and *rho0* dictionaries. Keys are element symbols (all lower case).

Target composition is given using *treco.srimUtils.Component* class. Let's recreate a RIKEN 2018 gas mixture, which was 70 ml/min Ar, 30 ml.min He and 2 ml/min CF4. We will use hardcoded dictionaries for help with masses:


.. code-block:: ipython

   In [1]: import treco.srimUtils as sr

   In [2]: ar = sr.Component(18, 'argon', 70, sr.molarMass['ar'])
   
   In [3]: he = sr.Component(2, 'helium', 30, sr.molarMass['he'])
   
   In [4]: c = sr.Component(6, 'carbon', 2, sr.molarMass['c'])

   In [5]: f = sr.Component(9, 'fluorine', 8, sr.molarMass['f'])

The first argument is an atomic number, second: the name (not used by code, but helps to identify a compound), then stoichiometry and finally the molar mass.
Note two things: we had to separate carbon and fluorine in CF4 as SRIM does not recognize compound gases and we used gas flow as stoichiometry. The latter is allowed as well as using atomic or molar percentage (but not mass percentage).

Next, an instance of *treco.SrimInput* will be used to generate a SR.IN file. Proton will be the projectile:

.. code-block:: ipython
    
    In [6]: srIn = sr.SrimInput()

    In [7]: srIn.ionZ = 1 # proton

    In [8]: srIn.ionMass = sr.molarMass['H']

    In [9]: srIn.ionName = 'proton' # optional

    In [10]: srIn.targetComponents = [ar, he, c, f]

    In [11]: srIn.emin = 1 # minimum energy in keV

    In [12]: srIn.emax = 10000 # maximum energy in keV

All that remains is target density. This can be calculated with *treco.srimUtils.calculateDensity* function. The conditions are standard: 25 degrees Celsius and 1013 hPa. Once again, we well use the help of builtin dictionaries:

.. code-block:: ipython
    
    In [13]: density = sr.calculateDensity(componentStoich=(ar.stoich, he.stoich, c.stoich),
                                           componentDens=(sr.rho0['ar'], sr.rho0['he'], sr.rho0['cf4']),
                                           temp=25, pressure = 1013)
    In [14]: srIn.targetDensity = density


Once all information is passed, we can generate a SR.IN file:


.. code-block:: ipython
    
    In [15]: srIn.generateSrimInput('./SR.IN')

Now we can copy SR.IN file to SR Module directory and run SRModule.exe manually or try the experimental feature (if the latter, the previous step was not necessary):


    In [15]: srIn.makeSrimTable(outputDir='targetDir/', srmoduleDir='path-to-srmodule-dir')


.. warning::
    *treco.srimUtils.makeSrimTable* needs *wine* installed on the system, since SRIM is originally a Windows program.

If all is correct, a range table should appear in target directory. If srIn.outputFilename was not given before, the filename will look something like 'proton_in_argon70+helium30+c2+f8d1.24.srim.dat'

Check example use cases [LINK] to see how to use *treco.srimUtils* to quickly get track range vs gas density plot.