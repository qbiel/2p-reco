Quick start guide
===================

.. _initconf:

Initial configuration
----------------------

*treco* relies on three inputs for correct reconstruction: pixel size, drift velocity and energy-range tables.
First two (and optionally the latter) must be specified in *treco/constants.py* file.

Example of *constants.py* file is shown below:

.. code-block:: python

    driftVel = 19300  # um/us
    pixelWidth = 660  # um/px

    runDirectory = '/mnt/data'

    ptypes = {
        'proton': '/home/user/treco/srimTables/srimOutH.txt',
        'alpha': '/home/user/treco/srimTables/srimOutHe.txt',
    }

Description of variables:

* **driftVel**: electron drift velocity in used gas mixture. Unit is *μm/μs*.
* **pixelWidth**: conversion factor between length on the image in pixels and
  real length in micrometers. Unit is *μm/px*.
* **ptypes** (optional): dictionary of particle names and corresponding locations of SRIM output files
  for those particles. This allows for using particle names instead of file paths while
  creating *treco.Track* objects.
* **runDirectory** (optional): path to directory with experiment data. If specified, events can be loaded
  using run/event number instead of paths. Standard OTPC structure and naming (as of 2021) is recognized
  (eg. *runDirectory/R002/R002_00010.new.png*)

.. note::
   In treco/constants.py file you can also set default values for various treco objects, like default image size, default sampling rate etc. See API reference and treco/constants.py file itself for more info.

Quick overview
---------------

Simulation
###########

*treco* keeps basic information on a track in a *treco.Track* object. Those information are:

* initial energy
* range
* length on the image
* length on the waveform
* angle on camera image θ
* angle on PMT waveform φ

All these information are consistent with each other at all times, which means if you change one of the parameters, all other data dependent on it will be recalculated with the new value.

To model events, *treco* uses *ImageSimulator* and *WfmSimulator* classes.
They can accept one or many *Track* objects and combine single-track images/waveforms
into one event. Simulators also take care of image and waveform sizes, gaussian smoothing of tracks, optionally added background normalization.
Outputs from simulators can be directly compared to real events and are used in parameter scanners.

*treco.EventViewer* is an interactive tool that is the best way to visually inspect what both *treco.ImageSimulator* and *treco.WfmSimulator* are doing, dynamically change their parameters and inspect real events.

Event loading and post-processing
##################################

*treco* can also handle opening camera images and PMT waveforms from experiment using *treco.ExpImage* and *treco.ExpWaveform* classes.
Limited post-processing of those images and waveforms from experiment is also possible. Currently supported operations are background reduction, gaussian smoothing and normalization.

Event fitting
##############

For fitting models to real-life events, *treco* currently utilizes least-squares method by brute force direct search, which compares image/wfm data with models calculated for every combination of parameters from given parameter set. This way a parameter matrix is created from which user can extract best fit and estimate errorbars. More in parameter scanning section.

Example use cases
-------------------

Basic track information
##########################

Let's use *Track* object to get some information about a proton track.
You can use an example configuration file and SRIM table shipped with *treco*
or prepare :ref:`your own configuration <initconf>`.

First, start IPython in your console and, import treco package and create a Track instance:

.. code-block:: ipython

   In [1]: import treco

   In [2]: t = treco.Track('proton')

Here, 'proton' is an alias for SRIM output file defined in *constants.py* file (you can also provide a path to SRIM output file instead).
Track will be initialized with default parameters. Let's take a look at the energy,
and then print all other parameters:

.. code-block:: ipython

   In [3]: t.e0
   Out [3]: 1000

   In [4]: t.getParameters()
   Out[4]:
    {'e0': (1000, 'keV'),
     'theta': (20, 'deg'),
     'phi': (-45, 'deg'),
     'range': (33.02, 'mm'),
     'imLength': (23.3486659147798, 'mm'),
     'wfmLength': (23.348665914779797, 'mm'),
     'vector': (array([ 0.66446302,  0.24184476, -0.70710678]), '[x,y,z]')}

Now we will change some parameters and observe other ones changing accordingly:

.. code-block:: ipython
    
    In [5]: t.e0 = 1500

    In [6]: t.getParameters()
    Out [6]:
    {'e0': (1500, 'keV'),
     'theta': (20, 'deg'),
     'phi': (-45, 'deg'),
     'range': (62.279999999999994, 'mm'),
     'imLength': (44.03861033229818, 'mm'),
     'wfmLength': (44.03861033229817, 'mm')}

    In [7]: t.phi = 30

    In [8]: t.range
    Out [8]: 62279.99999999999

    In [9]: t.imLength
    Out [9]: 53936.062147694836

    In [10]: t.wfmLength
    Out [10]: 31139.999999999993


All other parameters can be changed this way except for track lengths on image
and waveform. Those can be set using *setLengths()*, *setImageLength()* or *setWfmLength()* methods:

.. code-block:: ipython
   
   In [10]: t.setLengths(60000, 30000, phiSgn=True) # length on image first, in micrometers

   In [11]: t.getParameters()
   Out [11]:
   {'e0': (1571.7859641382988, 'keV'),
    'theta': (20, 'deg'),
    'phi': (26.56505117707799, 'deg'),
    'range': (67.08193062498376, 'mm'),
    'imLength': (59.99990277575545, 'mm'),
    'wfmLength': (29.99995138787773, 'mm'),
    'vector': (array([0.84048663, 0.30591212, 0.4472136 ]), '[x,y,z]')}

Note how using setLengths method you can easily get track energy from image and waveform
lengths. Also note that the sign of vertical angle is ambiguous when calculating it using lengths, hence the *phiSgn* argument.

Now, having learned how to modify track parameters, let's make some plots.
We will plot how vertical angle changes with waveform length while keeping image length constant.

.. In order to generate energy loss profiles, additional parameters are required. These are pixel binning (for image) and digitizer sampling rate (for waveform). In this example we will assume binning 1 (default) and sampling rate 100 MSa/s.

.. .. code-block:: ipython

..    In [1]: %matplotlib

..    In [2]: import matplotlib.pyplot as plt

..    In [3]: t = treco.Track('proton')

..    In [4]: t.setImageLength(60000,phi=-30)

..    In [5]: imLoss = t.makeImageLoss(binning=1)

..    In [6]: plt.plot(imLoss, label=f'imL={t.imLength:.1f} um, e0={t.e0:.1f} keV')

..    In [7]: t.setImageLength(60000,phi=-60)

..    In [7]: imLoss = t.makeImageLoss(binning=1)
   
..    In [9]: plt.plot(t.imLoss, label=f'imL={t.imLength:.1f} um, e0={t.e0:.1f} keV')

..    In [10]: plt.xlabel('Pixel number')

..    In [11]: plt.ylabel('Energy loss [keV]')

..    In [12]: plt.legend()

.. .. [place image here]

.. code-block:: ipython

   In [1]: import matplotlib.pyplot as plt

   In [2]: import treco

   In [3]: t = treco.Track('proton')

   In [4]: phis = []; wfmLs = []

   In [5]: for phi in range(-60,1,1):
        .:     t.setImageLength(60000,phi=phi)
        .:     phis.append(phi)
        .:     wfmLs.append(t.wfmLength)

   In [6]: plt.plot(phis, wfmLs, label=f'imL={t.imLength:.1f} um')

   In [7]: plt.xlabel('Vertical angle [deg.]')

   In [8]: plt.ylabel('Length on waveform [um]')

   In [9]: plt.legend()


.. plot:: plots/quickstart/wfmLVsPhiPlot.py


Interactive event model
#############################

Having learned how to get basic track information it is time to simulate events and inspect
them visually. For this we will use *treco.Track* class again, this time in tandem with *treco.EventViewer* class.
Launch IPython and create an *EventViewer* instance:

.. code-block:: ipython
    
   In [1]: %matplotlib

   In [2]: from treco import Track, EventViewer

   In [3]: v = EventViewer()

A Matplotlib window with some empty axes should appear. Let's create one proton with energy 750 keV, vertical angle -30 degrees and horizontal angle 45 degrees. Afterwards, we will add this track to the simulation:

.. code-block:: ipython

   In [4]: t = Track('proton', e0=750, phi=-30, theta=45)

   In [5]: v.addTrack(t)

.. plot:: plots/quickstart/eventExample1.py

At this point, an image and a waveform should appear in a window. To get this result, several
assumptions were made:

* image size was set to default value
* track positions *x0*,*y0* & *z0* are set to the middle of image & wfm 
* pixel binning was set to 1
* waveform size was set to 2000 samples
* waveform sampling rate was set to 100 MSa/s
* smoothing and scaling factors were set to default values

Those parameters can be changed interactively. To show some examples, let's decrease the image size to 64x64 px, wfm size to 1500 samples and change camera binning to 2:

.. code-block:: ipython
 
   In [6]: v.x0 = 10; v.y0 = 10

   In [7]: v.imSize = (64,64)
   
   In [8]: v.wfmSize = 1500
   
   In [9]: v.binning = 2

.. warning::
   Make sure current x0, y0 & z0 are within new boundaries when changing image/wfm size.

Now we can compare the simulation with a real event. We will load both event image and event waveform
from a directory with examples (filepaths may vary):

.. code-block:: ipython

   In [10]: v.loadEventImage('treco/events/r38_226/r38_226.png')

   In [11]: v.loadEventWfm(path='treco/events/r38_226/R038_00226.new_scope.meta', wfmStart=708, wfmStop=712)


.. plot:: plots/quickstart/eventExample2.py

A couple of things happened here: event image appeared on the right and event waveform appeared on top of a model. Also, image & waveform sizes changed to fit event sizes. If a waveform is loaded from original new_scope files (as in this example), simulation sampling rate also changes to fit an event. Binning is one parameter you have to set manually: in this case, event image binning is two and we set this correctly before. The other one is normalization - data signals are in ADC units, while model is in keV. Instead of manually adjusting scaleC and scaleW parameters we can use normalize() method - *treco* will automatically normalize the model to the data.

There is a couple of things we can do to loaded event: we can smooth it a bit with a gaussian filter and we can subtract constant background (from waveform only so far). Slight smoothing will get rid of spiky noise on waveform and salt-and-pepper noise on the image without hurting the reconstruction resolution too much, but improving visual comparison:

.. code-block:: ipython

   In [11]: v.expimage.sigma = 1 # in pixels

   In [12]: v.expwfm.sigma = 3 # in samples

   In [13]: v.expwfm.bg = 600 # in ADC channels

As we have loaded a two proton event, let's add a second proton to our model:

.. code-block:: ipython

  In [14]: t2 = Track('proton', e0=750)

  In [15]: v.addTrack(t2)

Adding a second track caused a single-track components to show on waveform and image projection plots. Keep in mind that the scale of the components is different than scale of the sum. 

All parameter manipulation methods described in previous section work here as well, so let's try to manually match the model with the event. It is easier to operate with image/wfm lengths than with energies/angles in this case, so we will use *track.setLengths()* method and import pixel width and drift velocity values to be able to operate in pixel/us units:


.. code-block:: ipython

   In [11]: import treco.constants as const

   In [12]: v.x0 = 26; v.y0 = 13

   In [13]: v.z0 = 709.7

   In [14]: t.theta = 170; t2.theta = 50

   In [15]: t.setLengths(33 * const.pixelWidth, 0.7 * const.driftVel, False)
   
   In [16]: t2.setLengths(20 * const.pixelWidth, 0.05 * const.driftVel, True)

.. note::
   You can use v.measureImage() and v.measureWfm() methods to get a rough estimation of image/wfm lengths by clicking on the image/waveform. More in [where].

Smoothing and scaling of the model can also be tweaked:

.. code-block:: ipython

   In [17]: v.sigmaC = 1.5; v.sigmaW = 3.5

   In [18]: v.normalize()

.. plot:: plots/quickstart/eventExample3.py

Not bad for a rough estimation! We can now get interesting information from the simulation:

.. code-block:: ipython

   In [19]: t.getParameters()
   Out [19]:
   {'e0': (847.2567346708803, 'keV'),
   'theta': (170, 'deg'),
   'phi': (-31.81107261178019, 'deg'),
   'range': (21.321724746621513, 'mm'),
   'imLength': (18.119006405029296, 'mm'),
   'wfmLength': (11.239108197059036, 'mm')}

   In [20]: t2.getParameters()
   Out [20]:
   {'e0': (542.8253572667016, 'keV'),
   'theta': (50, 'deg'),
   'phi': (4.181230459416301, 'deg'),
   'range': (21.321724746621513, 'mm'),
   'imLength': (21.264975181603624, 'mm'),
   'wfmLength': (1.554598564412689, 'mm')}
   
 
   In [21]: t.e0 + t2.e0
   Out [21]: 1392.32617191906

   In [22]: t.getAngleBetweenTracks(t,t2) # in degrees
   Out [22]: 117.52900762204841 

You may want to save those settings for future analysis:

.. code-block:: ipython
   
   In[23]: v.saveParams('example2p.json')

Settings are saved in a human-readable JSON file. In addition, *treco.EventViewer*
can read this file back and quickly restore those settings. You can use *loadParams(file)* method on a current *EventViewer* instance or initialize a new one with a JSON file path as argument:

.. code-block:: ipython

   In[1]: from treco import Track, EventViewer

   In[2]: v = EventViewer(paramsFile='example2p.json')

   In[3]: t, t2 = v.tracks


