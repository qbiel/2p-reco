.. _api:

Treco API reference
===================

treco.Track
------------------

.. autoclass:: treco.Track
   :members:
   :undoc-members:
   :show-inheritance:

treco.Simulators
-----------------------

.. automodule:: treco.Simulators
   :members:
   :undoc-members:
   :show-inheritance:

treco.EventViewer
------------------------

.. automodule:: treco.EventViewer
   :members:
   :undoc-members:
   :show-inheritance:

treco.srimUtils module
--------------------------

.. automodule:: treco.srimUtils
   :members:

