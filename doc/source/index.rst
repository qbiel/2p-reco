.. Treco documentation master file, created by
   sphinx-quickstart on Thu Sep 23 14:24:24 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Treco's documentation!
=================================

Treco is a toolkit for reconstructing nuclear decay events recorded with the OTPC detector.
Full 3D reconstruction is based on SRIM range simulation and supports single- and multiparticle decays.

.. note::
   
   This project is still under active development.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   quickstart
   maths
   tutorial
   api/treco
   znstatus


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
