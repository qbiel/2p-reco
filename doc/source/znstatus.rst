\ RIKEN 2019 analysis report
================================

This section is a step by step progress report of data analysis from RIKEN 2019 experiment.

.. toctree::
   :maxdepth: 2
   :caption: Analysis steps

   statusreport/borderCheck
   statusreport/zn56
   statusreport/zn55b1p
   statusreport/zn55b2p
   statusreport/zn54
   statusreport/sysError



