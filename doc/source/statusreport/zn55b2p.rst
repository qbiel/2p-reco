\ :sup:`55`\Zn beta-2p reconstruction
=======================================

The goal
########

RIKEN 2019 experiment provided the first observation of \ :sup:`55`\Zn \ :math:`\beta`\2p decay.
AG in her `thesis <http://isods1.zfj.fuw.edu.pl/dokuwiki/lib/exe/fetch.php?media=documentation:publications:giska_master_1100-mgr-fz-fjc-300164.pdf>`_ identified **16** such events. The goal of this analysis is to determine which of those events were fully stopped in the chamber and attempt a full 3D reconstruction of those stopped events. On the other hand, there will be an attempt to estimate length of tracks that escaped the chamber. This may provide lower limits for proton energies.

Full deposition check
######################

Two steps were performed to determine if the tracks were stopped in the chamber. First, checking camera images can quickly eliminate events where one or both protons escaped through the side walls of the chamber. Events where proton(s) escaped through the bases of the chamber (cathode or amplification stage) are more difficult to eliminate, because there is no information in OTPC data on absolute vertical position of the track. Therefore, the second step is to inspect the shape of PMT signals - anomalous shaped (like no Bragg peak) may suggest that a proton escaped.


R018_00013
-----------

None of the tracks is close to the side walls, promising for full deposition.

.. plot:: statusreport/b2p/borderCheck/r018_00013Border.py
    :width: 500

R020_00017
-----------

Both protons escaped through the side wall.

.. plot:: statusreport/b2p/borderCheck/r020_00017Border.py
    :width: 500


R022_00014
-----------

None of the tracks is close to the side walls, but short tracks - protons may have escaped through chamber bases.

.. plot:: statusreport/b2p/borderCheck/r022_00014Border.py
    :width: 500

R022_00018
-----------

One track ends close to the wall, no visible Bragg peak - probably escaped.

.. plot:: statusreport/b2p/borderCheck/r022_00018Border.py
    :width: 500

R022_00034
-----------

None of the tracks is close to the side walls, but short tracks - protons may have escaped through chamber bases.

.. plot:: statusreport/b2p/borderCheck/r022_00034Border.py
    :width: 500

R023_00005
-----------

None of the tracks is close to the side walls, promising for full deposition.

.. plot:: statusreport/b2p/borderCheck/r023_00005Border.py
    :width: 500

R023_00012
-----------

None of the tracks is close to the side walls, but no visible Bragg peak on one of the tracks.
Also, recoil visible?

.. plot:: statusreport/b2p/borderCheck/r023_00012Border.py
    :width: 500

R066_00001
-----------

One track close to the side wall, but both Bragg peaks seem to be visible. Questionable.

.. plot:: statusreport/b2p/borderCheck/r066_00001Border.py
    :width: 500

R067_00032
-----------

None of the tracks is close to the side walls, but short tracks - protons may have escaped through chamber bases.

.. plot:: statusreport/b2p/borderCheck/r067_00032Border.py
    :width: 500

R067_00088
-----------

Both protons may have escaped.

.. plot:: statusreport/b2p/borderCheck/r067_00088Border.py
    :width: 500

R067_00109
-----------

None of the tracks is close to the side walls, but short tracks - protons may have escaped through chamber bases.

.. plot:: statusreport/b2p/borderCheck/r067_00109Border.py
    :width: 500

R068_00006
-----------

One proton clearly escaped.

.. plot:: statusreport/b2p/borderCheck/r068_00006Border.py
    :width: 500

R068_00218
-----------

Both protons clearly escaped.

.. plot:: statusreport/b2p/borderCheck/r068_00218Border.py
    :width: 500

R071_00128
-----------

One proton escaped.

.. plot:: statusreport/b2p/borderCheck/r071_00128Border.py
    :width: 500

R076_00075
-----------

None of the tracks is close to the side walls, but no clear Bragg peak on one of the tracks.

.. plot:: statusreport/b2p/borderCheck/r076_00075Border.py
    :width: 500

R076_00102
-----------

One proton escaped.

.. plot:: statusreport/b2p/borderCheck/r076_00102Border.py
    :width: 500

Test reconstruction - R018_00013
#################################

Before moving on with the analysis, the reconstruction procedure was attempted for the most promising event found, R018_00013. Both camera and PMT signals suggest that the event was fully deposited. Therefore, a standard procedure for 2p decay analysis was performed. Details below.

Event post-processing
----------------------

Details on how and why: :ref:`link <eventpostprocessing>`

The tracks were extracted from raw image and constant background was subtracted. Signal of interest from PMT was found manually and constant background was also subtracted.

.. plot::
    statusreport/b2p/r18_13Compare.py
    :width: 800

Input: gaussian threshold 1000, median filter size 3, gaussian filter sigma 2, pixel threshold ----
Output: mean bg ---, std dev ---

.. plot::
    statusreport/b2p/r18_13Wfm.py
    :width: 800

Wfm start 8527.96 us, wfm stop 8535.00 us, mean bg 776, std dev ---

The shape of the waveform suggests that one track has a long vertical component. Furthermore, a third peak at ~8534 us is visible. This may be a noise variation or a signal from recoil. The latter is unlikely, tough - for energies up to ~400 keV electronic losses are approx. 10% of total energy loss.

Preliminary fit
---------------

One up-down combination that seems to be the best was reconstructed. Results below.

.. image:: b2p/r18_13Fit.png
    :width: 800

* Track 1:

 * Energy: 1303 keV
 * Range: 49.8 mm
 * Length on camera: 48.8 mm
 * Length on PMT: 9.8 mm
 * Vertical angle phi: -11.4 deg
 * Horizontal angle theta: 162 deg

* Track 2:

 * Energy: 2056 keV
 * Range: 103 mm
 * Length on camera: 46.2 mm
 * Length on PMT: 92.1 mm
 * Vertical angle phi: 63.4 deg
 * Horizontal angle theta: 262 deg

* Sum of proton energies: 3359 keV
* Angle between tracks: 104 degrees
* Recoil energy: 48 keV (need to check)

