\ :sup:`55`\Zn beta-1p reconstruction
======================================

The goal
########
This data has been analysed before by AG in her `thesis <http://isods1.zfj.fuw.edu.pl/dokuwiki/lib/exe/fetch.php?media=documentation:publications:giska_master_1100-mgr-fz-fjc-300164.pdf>`_. The goal of this analysis is to re-examine 1-proton \ :sup:`55` \Zn decays with *treco* reconstruction algorithm to check its performance with this kind of data.

Event selection
#################
According to AG's thesis, **241** events of beta-p were registered during Big-RIPS run. Because of non-optimal gas mixture for such a high-energy events, only **21** events were fully deposited in OTPC in the end.
*treco* reconstruction was attempted on those 21 events only. 

Event postprocessing
####################

In this step tracks are extracted from an original image and constant background is subtracted automatically.
Signal of interest from PMT waveform is also found automatically. Constant background is calculated as mean sample value from portions of the waveform on the left and right hand side of the signal of interest.

Details on how and why: :ref:`link <eventpostprocessing>`

Inspection of the waveforms shows that some signals are quite low and signal-to-noise ratio is very low.
In some cases (R067_00099 for instance) low energy deposit at the start of the track makes it difficult to property determine the start position. One possible solution to increase SNR and the quality of the tracks is to bin adjacent samples. This decreases the sampling ratio of the signal and potentially throws away some information, but increases the quality by influencing the original signal as little as possible.
Below, two versions of the waveforms are showed - one with original sampling ratio, the other with binning 3.
Binning of 3 decreases the PMT signal resolution to approximately match the camera resolution.

R019_00014
-----------

.. plot:: statusreport/images/postprocessing/r19_14Compare.py
    :width: 800

Input: gaussian threshold 1000, median filter size 3, gaussian filter sigma 2, pixel threshold 4000
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r19_14Wfm.py
    :width: 800

Wfm start 3825.88 us, wfm stop  3832.64 us, mean bg 633, std dev 48

R019_00026
-----------

.. plot:: statusreport/images/postprocessing/r19_26Compare.py
    :width: 800

Input: gaussian threshold 1000, median filter size 3, gaussian filter sigma 2, pixel threshold 4000
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r19_26Wfm.py
    :width: 800

Wfm start 44652.0 us, wfm stop 44656.23 us, mean bg 633, std dev 48

R067_00090
-----------

.. plot:: statusreport/images/postprocessing/r67_90Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r67_90Wfm.py
    :width: 800

Wfm start 44581.82 us, wfm stop  44585.41 us, mean bg 633, std dev 48

This track looks good in both camera and PMT - straight track, clear Bragg profile on the PMT.

R069_00053
-----------

.. plot:: statusreport/images/postprocessing/r69_53Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r69_53Wfm.py
    :width: 800

Wfm start 6407.93 us, wfm stop 6415.13 us, mean bg 556, std dev 40

R071_00137
-----------

.. plot:: statusreport/images/postprocessing/r71_137Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r71_137Wfm.py
    :width: 800

Wfm start 58846.78 us, wfm stop 58850.70 us, mean bg 532, std dev 44

R077_00008
-----------

.. plot:: statusreport/images/postprocessing/r77_8Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r77_8Wfm.py
    :width: 800

Wfm start  2520.87 us, wfm stop 2529.02 us, mean bg 523, std dev 36

R077_00015
-----------

.. plot:: statusreport/images/postprocessing/r77_15Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r77_15Wfm.py
    :width: 800

Wfm start  9230.20 us, wfm stop 9233.96 us, mean bg 682, std dev 49

R067_00111
-----------

.. plot:: statusreport/images/postprocessing/r67_111Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r67_111Wfm.py
    :width: 800

Wfm start 54584.98 us, wfm stop 54588.0 us, mean bg 607, std dev 45

R071_00080
-----------

.. plot:: statusreport/images/postprocessing/r71_80Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r71_80Wfm.py
    :width: 800

Wfm start 7304.19 us, wfm stop 7311.68 us, mean bg 536, std dev 59

R067_00099
-----------

.. plot:: statusreport/images/postprocessing/r67_99Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r67_99Wfm.py
    :width: 800

Wfm start 18828.12 us, wfm stop 18836.5 us, mean bg 692, std dev 43

R067_00112
-----------

.. plot:: statusreport/images/postprocessing/r67_112Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r67_112Wfm.py
    :width: 800

Wfm start 107271.98 us, wfm stop 107278.00 us, mean bg 576, std dev 43

R068_00086
-----------

.. plot:: statusreport/images/postprocessing/r68_86Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r68_86Wfm.py
    :width: 800

Wfm start 7914.82 us, wfm stop 7921.99 us, mean bg 527, std dev 37

R068_00132
-----------

.. plot:: statusreport/images/postprocessing/r68_132Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r68_132Wfm.py
    :width: 800

Wfm start 37192.88 us, wfm stop 37198.68 us, mean bg 592, std dev 41

R068_00224
-----------

.. plot:: statusreport/images/postprocessing/r68_224Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r68_224Wfm.py
    :width: 800

Wfm start 25970.49 us, wfm stop 25976.52 us, mean bg 571, std dev 40

R068_00252
-----------

.. plot:: statusreport/images/postprocessing/r68_252Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r68_252Wfm.py
    :width: 800

Wfm start 38974.76 us, wfm stop 38980.12 us, mean bg 663, std dev 45

R071_00069
-----------

.. plot:: statusreport/images/postprocessing/r71_69Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r71_69Wfm.py
    :width: 800

Wfm start 2018.78 us, wfm stop 2025.02 us, mean bg 531, std dev 36

R076_00161
-----------

.. plot:: statusreport/images/postprocessing/r76_161Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r76_161Wfm.py
    :width: 800

Wfm start 3718.63 us, wfm stop 3722.58 us, mean bg 503, std dev 40

R067_00018
-----------

.. plot:: statusreport/images/postprocessing/r67_18Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r67_18Wfm.py
    :width: 800

Wfm start 5037.5 us, wfm stop 5045 us, mean bg 616, std dev 34

R071_00073
-----------

.. plot:: statusreport/images/postprocessing/r71_73Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r71_73Wfm.py
    :width: 800

Wfm start 21915.69 us, wfm stop 21918.07 us, mean bg 597, std dev 63

R066_00004
-----------

.. plot:: statusreport/images/postprocessing/r66_4Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r66_4Wfm.py
    :width: 800

Wfm start 1375.19us, wfm stop 1381.43us, mean bg 548, std dev 35

R069_00051
-----------

.. plot:: statusreport/images/postprocessing/r69_51Compare.py
    :width: 800

Input: gaussian threshold 1800, median filter size 3, gaussian filter sigma 2, pixel threshold 1500
Output: mean bg --, std dev --

.. plot:: statusreport/images/postprocessing/r69_51Wfm.py
    :width: 800

Wfm start 40050.99 us, wfm stop 40050.99 us, mean bg 519, std dev 41

Initial guesses
################

Files with initial guesses for *treco.EventViewer* and config files for len-len parameter scanner:

JSON param files:

* :download:`R019_00014.json <images/eyeballs/R019_00014.json>`     :download:`R019_00014.cfg <images/fitConfigs/R019_00014.cfg>` 
* :download:`R019_00026.json <images/eyeballs/R019_00026.json>`     :download:`R019_00026.cfg <images/fitConfigs/R019_00026.cfg>` 
* :download:`R067_00090.json <images/eyeballs/R067_00090.json>`     :download:`R067_00090.cfg <images/fitConfigs/R067_00090.cfg>` 
* :download:`R069_00053.json <images/eyeballs/R069_00053.json>`     :download:`R069_00053.cfg <images/fitConfigs/R069_00053.cfg>` 
* :download:`R071_00137.json <images/eyeballs/R071_00137.json>`     :download:`R071_00137.cfg <images/fitConfigs/R071_00137.cfg>` 
* :download:`R077_00008.json <images/eyeballs/R077_00008.json>`     :download:`R077_00008.cfg <images/fitConfigs/R077_00008.cfg>` 
* :download:`R077_00015.json <images/eyeballs/R077_00015.json>`     :download:`R077_00015.cfg <images/fitConfigs/R077_00015.cfg>` 
* :download:`R067_00111.json <images/eyeballs/R067_00111.json>`     :download:`R067_00111.cfg <images/fitConfigs/R067_00111.cfg>` 
* :download:`R071_00080.json <images/eyeballs/R071_00080.json>`     :download:`R071_00080.cfg <images/fitConfigs/R071_00080.cfg>` 
* :download:`R067_00099.json <images/eyeballs/R067_00099.json>`     :download:`R067_00099.cfg <images/fitConfigs/R067_00099.cfg>` 
* :download:`R067_00112.json <images/eyeballs/R067_00112.json>`     :download:`R067_00112.cfg <images/fitConfigs/R067_00112.cfg>` 
* :download:`R068_00086.json <images/eyeballs/R068_00086.json>`     :download:`R068_00086.cfg <images/fitConfigs/R068_00086.cfg>` 
* :download:`R068_00132.json <images/eyeballs/R068_00132.json>`     :download:`R068_00132.cfg <images/fitConfigs/R068_00132.cfg>` 
* :download:`R068_00224.json <images/eyeballs/R068_00224.json>`     :download:`R068_00224.cfg <images/fitConfigs/R068_00224.cfg>` 
* :download:`R068_00252.json <images/eyeballs/R068_00252.json>`     :download:`R068_00252.cfg <images/fitConfigs/R068_00252.cfg>` 
* :download:`R071_00069.json <images/eyeballs/R071_00069.json>`     :download:`R071_00069.cfg <images/fitConfigs/R071_00069.cfg>` 
* :download:`R076_00161.json <images/eyeballs/R076_00161.json>`     :download:`R076_00161.cfg <images/fitConfigs/R076_00161.cfg>` 
* :download:`R067_00018.json <images/eyeballs/R067_00018.json>`     :download:`R067_00018.cfg <images/fitConfigs/R067_00018.cfg>` 
* :download:`R071_00073.json <images/eyeballs/R071_00073.json>`     :download:`R071_00073.cfg <images/fitConfigs/R071_00073.cfg>` 
* :download:`R066_00004.json <images/eyeballs/R066_00004.json>`     :download:`R066_00004.cfg <images/fitConfigs/R066_00004.cfg>` 
* :download:`R069_00051.json <images/eyeballs/R069_00051.json>`     :download:`R069_00051.cfg <images/fitConfigs/R069_00051.cfg>` 

Fits
#####

Here are presented the results of fitting. So far, waveforms without binning were used.

R019_00014
-----------

.. image:: images/55zn1pFits/R019_00014.png
    :width: 800

E0 = 2234 keV
Range: 117.87 mm
Length on camera: 44.50 mm
Length on PMT: 108.73 mm
Vertical angle: -67.29 deg
Horizontal angle: 58 deg

R066_00004
-----------

.. image:: images/55zn1pFits/R066_00004.png
    :width: 800

E0 = 1978 keV
Range: 96.76 mm
Length on camera: 23.76 mm
Length on PMT: 93.79 mm
Vertical angle: 75.79 deg
Horizontal angle: -4 deg

R067_00090
-----------

.. image:: images/55zn1pFits/R067_00090.png
    :width: 800

E0 = 1415 keV
Range: 56.73 mm
Length on camera: 41.58 mm
Length on PMT: 38.6 mm
Vertical angle: -42.87 deg
Horizontal angle: 46.8 deg

R067_00099
-----------

.. image:: images/55zn1pFits/R067_00099.png
    :width: 800

E0 = 2420 keV
Range: 134.2 mm
Length on camera: 71.94 mm
Length on PMT: 113.29 mm
Vertical angle: 57.78 deg
Horizontal angle: -64 deg

R067_00111
-----------

.. image:: images/55zn1pFits/R067_00111.png
    :width: 800

E0 = 1701 keV
Range: 75.96 mm
Length on camera: 71.28 mm
Length on PMT: 26.25 mm
Vertical angle: -20.22 deg
Horizontal angle: 88.5 deg

R067_00112
-----------

.. image:: images/55zn1pFits/R067_00112.png
    :width: 800

E0 = 2591 keV
Range: 150.141 mm
Length on camera: 123.42 mm
Length on PMT: 85.50 mm
Vertical angle: 34.71 deg
Horizontal angle: 62.1 deg

R068_00086
-----------

.. image:: images/55zn1pFits/R068_00086.png
    :width: 800

| E0 = 2338 keV
| Range: 126.89 mm
| Length on camera: 59.4 mm
| Length on PMT: 112.13 mm
| Vertical angle: 62.09 deg
| Horizontal angle: -46.2 deg

R068_00132
-----------

.. image:: images/55zn1pFits/R068_00132.png
    :width: 800

| E0 = 1802 keV
| Range: 83.36 mm
| Length on camera: 23.1 mm
| Length on PMT: 80.1 mm
| Vertical angle: 73.91 deg
| Horizontal angle: 82.5 deg

R068_00224
-----------

.. image:: images/55zn1pFits/R068_00224.png
    :width: 800

| E0 = 2127 keV
| Range: 108.87 mm
| Length on camera: 66.66 mm
| Length on PMT: 86.08 mm
| Vertical angle: 52.25 deg
| Horizontal angle: 249.0 deg

R068_00252
-----------

.. image:: images/55zn1pFits/R068_00252.png
    :width: 800

| E0 = 1866 keV
| Range: 88.17 mm
| Length on camera: 46.86 mm
| Length on PMT: 74.69 mm
| Vertical angle: 57.9 deg
| Horizontal angle: 172 deg

R071_00069
-----------

.. image:: images/55zn1pFits/R071_00069.png
    :width: 800

| E0 = 1920 keV
| Range: 92.27 mm
| Length on camera: 15.84 mm
| Length on PMT: 90.90 mm
| Vertical angle: 80.12 deg
| Horizontal angle: 43.7 deg

R071_00073
-----------

.. image:: images/55zn1pFits/R071_00073.png
    :width: 800

| E0 = 1249 keV
| Range: 46.65 mm
| Length on camera: 44.22 mm
| Length on PMT: 14.86 mm
| Vertical angle: 18.58 deg
| Horizontal angle: -76.0 deg

R071_00080
-----------

.. image:: images/55zn1pFits/R071_00080.png
    :width: 800

| E0 = 2358 keV
| Range: 128.74 mm
| Length on camera: 60.06 mm
| Length on PMT: 113.87 mm
| Vertical angle: -62.19 deg
| Horizontal angle: 105.0 deg

R071_00137
-----------

.. image:: images/55zn1pFits/R071_00137.png
    :width: 800

| E0 = 1438 keV
| Range: 58.2 mm
| Length on camera: 34.98 mm
| Length on PMT: 46.51 mm
| Vertical angle: -53.06 deg
| Horizontal angle: -8.0 deg

R076_00161
-----------

.. image:: images/55zn1pFits/R076_00161.png
    :width: 800

| E0 = 1445 keV
| Range: 58.66 mm
| Length on camera: 34.98 mm
| Length on PMT: 47.09 mm
| Vertical angle: 53.39 deg
| Horizontal angle: 81.40 deg

R077_00008
-----------

.. image:: images/55zn1pFits/R077_00008.png
    :width: 800

| E0 = 2422 keV
| Range: 134.51 mm
| Length on camera: 402.6 mm
| Length on PMT: 128.35 mm
| Vertical angle: -72.58 deg
| Horizontal angle: 270.0 deg

R077_00015
-----------

.. image:: images/55zn1pFits/R077_00015.png
    :width: 800

| E0 = 1410 keV
| Range: 56.39 mm
| Length on camera: 39.6 mm
| Length on PMT: 40.14 mm
| Vertical angle: -45.39 deg
| Horizontal angle: 117 deg


Results
#########


:: 

    Event       E0          Range       imL         wfmL        Theta   phi
    R071_00069  1920.22     92272.75    15840.00    90903.00    43.70   80.12
    R068_00086  2337.84     126894.32   59400.00    112133.00   -46.20  62.09
    R071_00137  1438.52     58198.45    34980.00    46513.00    -8.00   -53.06
    R067_00099  2419.53     134202.14   71940.00    113291.00   -64.00  57.58
    R067_00112  2591.44     150141.85   123420.00   85499.00    62.10   34.71
    R068_00224  2127.39     108871.38   66660.00    86078.00    249.00  52.25
    R067_00018  1841.06     86234.11    13860.00    85113.00    53.25   80.75
    R019_00014  2220.15     116671.38   44880.00    107694.00   57.34   -67.38
    R077_00015  1410.00     56388.84    39600.00    40144.00    117.00  -45.39
    R068_00132  1802.56     83359.58    23100.00    80095.00    82.50   73.91
    R076_00161  1445.77     58662.23    34980.00    47092.00    81.40   53.39
    R067_00111  1700.83     75959.17    71280.00    26248.00    88.50   -20.22
    R019_00026  2461.18     137995.00   127380.00   53075.00    84.00   -22.62
    R066_00004  1977.66     96760.54    23760.00    93798.00    -4.00   75.79
    R077_00008  2422.94     134511.36   40260.00    128345.00   270.00  -72.58
    R071_00073  1249.90     46650.38    44220.00    14861.00    -76.00  18.58
    R069_00051  1171.89     42183.78    8580.00 41302.00    147.08  78.26
    R071_00080  2358.64     128738.42   60060.00    113870.00   105.00  -62.19
    R068_00252  1866.73     88173.72    46860.00    74691.00    172.00  57.90
    R069_00053  2183.94     113599.25   12540.00    112905.00   65.93   -83.66
    R067_00090  1415.48     56734.97    41580.00    38600.00    46.80   -42.87
    
.. plot:: statusreport/images/55ZnSpectrum.py
    :width: 800
    

For comparison, here is the spectrum obtained by AG in her thesis:

.. image:: images/agSpectrum.png
    :width: 800