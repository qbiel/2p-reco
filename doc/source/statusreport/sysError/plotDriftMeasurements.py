import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('driftMeasurements.csv', parse_dates=['date'])
fig, ax = plt.subplots()

ax.scatter(df['date'], df['driftVel'])
ax.set_xlabel('Date')
ax.set_ylabel('Drift velocity [cm/us]')
ax.grid()
plt.show()