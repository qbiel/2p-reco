Systematic error analysis
==========================

Several systematic uncertainties exist that can affect the precision of the track reconstruction. Here are listed considered sources of those uncertainties as well as parameters that are affected by them:

* gas density variations influenced by temperature/pressure variations - affects lengths on both image and wfm
* gas density variations influenced by gas composition -  affects lengths on both image and wfm
* pixel width measurement error - affects length on image
* drift velocity variations - affects length on waveform

Gas mix density variation 
###########################

Gas mixture density is calculated using gas component densities at STP (standard temperature & pressure: :math:`T_0 = 273.15 K` and :math:`p_0 = 1013.25 hPa`) taken from literature and basic mixing rules for ideal gases.
Density of a single componen gas is given by:

.. math::
    \rho = \rho_0 \cdot \frac{p}{p_0}\frac{T_0}{T}

where :math:`\rho` is a gas density in pressure p and absolute temperature T and :math:`\rho_0` is a gas density at STP. Density of a gas mixture is calculated using a following equation:

.. math::
     \rho_{mix} = \frac{T_0}{T_0 + t} \cdot \frac{p}{p_0} \cdot \frac{1}{v_{tot}} \cdot \sum_i v_i \cdot \rho_{0,i},

where :math:`\rho_{0,i}` is the density of the i-th gas component STP and :math:`v_i` is the flow of the i-th gas component and :math:`v_{tot} = \sum_i v_i`.

**The gas composition set during RIKEN experiment was 70 ml/min Ar, 30 ml/min He and 2 ml/min :math:`CF4`**.
The data from meteo station shows that during th experiment the mean temperature was 25.3 degrees C. On the other hand, the mean pressure was 1014.3 hPa. Density values at STP and experiment conditions are given in a table below:

.. list-table:: Gas component densities and gas mixture density in STP and experiment conditions (25.3 deg. C, 1014.3 hPa)
   :widths: 25 25 50
   :header-rows: 1

   * - Gas
     - Density at STP, g/l
     - Density at exp. conditions, g/l
   * - He
     - 0.1785
     - 0.1635
   * - Ar
     - 1.7840
     - 1.6345
   * - :math:`CF_4`
     - 3.9470
     - 3.6162
   * - Mixture
     - 1.3542
     - **1.2407**

One can use small error propagation method to determine the uncertainty of gas mix density as a function of uncertainties of temperature, pressure and composition:

.. math::
    s_{\rho_{mix}}^2 = \sum_i s_{v_i}^2 \rho_i^2 (\frac{v_{tot} - v_i}{v_{tot}^2})^2 + s_p^2(\frac{\rho_{mix}}{p})^2 + s_T^2(\frac{\rho_{mix}}{T})^2

Plugging the numbers from the table, function of uncertainty takes the form:

.. math::
    s_{\rho_{mix}}^2 = 2.527 \cdot 10^{-5} s_{v_{Ar}}^2 + 1.280 \cdot 10^{-6} s_{v_{He}}^2 + 1.208 \cdot 10^{-3} s_{v_{CF_4}}^2 + 1.496 \cdot 10^{-6} s_p^2 + 1.728 \cdot 10^{-5} s_T^2

The plots below show how gas mixture density changes with temperature and pressure (all other variables constant). They confirm the analytical expression above:

.. plot:: statusreport/images/tpRhoPlot.py
    :width: 800


The data from meteo station shows that during RIKEN experiment the mean temperature was 25.3 degrees C and temperature variations did not exceed 1 degree. On the other hand, the mean pressure was 1014.3 hPa with pressure variations not exceeding 10 hPa.
To account for the fact that the meteostation was not calibrated in any way, I will assume more generoud temperature & pressure uncertainties, respectively +- 5 degrees and +- 30 hPa. Also, a generous 10% error on all gas flows is assumed. Using those information
I estimate that **density variations from temperature, pressure & gas composition changes amount to 0.0549 g/l, which is 4.4%**.

Range changes with gas density and composition
################################################

To establish how track ranges change with gas mixture, two analyses were performed. First, several SRIM files were created for gas mixtures with constant composition but varying density. This should account for temperature/pressure variations.
Second, several SRIM files were created for varying gas composition and constant density. Afterwards, from all generated SRIM files track ranges were taken for three energies - low, where energy deposits mostly in the Bragg peak, moderate and high. 

Constant gas composition, varying density
--------------------------------------------

.. image:: sysError/constantCompositionDensityPlot300kev.png
    :width: 800
.. image:: sysError/constantCompositionDensityPlot1000kev.png
    :width: 800
.. image:: sysError/constantCompositionDensityPlot3000kev.png
    :width: 800

The vertical lines on the plot represent 4.4% variation of density calculated in the previous section (although this value includes gas composition changes too). Apart from vertical scale, all three plots (for 300, 1000 and 3000 keV) look identical. Therefore the relative variation of track range does not depend on track energy.

Reading from these plots, the relative change of proton range is :math:`\frac{\Delta r}{r_0} \approx 0.8 \Delta \rho`. Assuming density variation to be 0.0549 g/l, **the relative systematic uncertainty of range is 4.4%**.

.. note:: 
    As typical ranges of protons in :math:`^54Zn` are between 10-30 mm, range variation amounts up to 1.32mm. That is 2 pixels on camera or 7 samples on PMT waveform.

Drift velocity uncertainty
############################

Drift velocity was measured manually numerous times during the experiment. The results are shown below:

.. plot:: statusreport/sysError/plotDriftMeasurements.py
    :width: 800

The errorbars on these data points are unknown, but are assumed to be relatively large. Therefore, a mean value of drift velocity was used for all reconstructions. As the measurements were started on 13 April around noon, only the points after that timestamp were used to calculate the mean. **The used value of drift velocity was 1.93 cm/us**.

The standard deviation of drift velocity (after 04.13 12:00) was 0.032 cm/us, which is 1.6% of the mean. However, there is a discrepancy between measured value and a simulation done in Magboltz by VG - simulated value is 17.5% larger than the measurement:

.. image:: sysError/VGsimulationsVsExperiment.png
    :width: 800

This discrepancy is still not fully understood. The final value of drift velocity errorbars is not yet decided.

Pixel size uncertainty
########################

Pixel size on image was calculated by dividing real OTPC length on image by the same length measured on an image in pixels. A crude way used to get uncertainty of that value is to assume that the precision of measurement is +- 2 pixels.
Taking this into account **the final pixel width was :math:`(660 \pm 7)` um.** 
