Chamber borders on camera
##########################

The field of view of the camera is wider than the chamber. Therefore, before any reconstruction takes place, one needs to check if analyzed tracks did not hit any walls of the chamber. In order to do that, the positions of chamber walls on the camera must be known.

At the beginning of the experiment, a picture of the iluminated chamber was taken, where all chamber borders were visible. From that picture, coordinates of chamber borders were taken and used to crop analyzed image as the first step of event post-processing.

Below is the image used to determine chamber coordinates:

.. plot:: statusreport/borderCheck/showBorders.py  
  :width: 600


An alternative is to impose event images on the iluminated image and check for border collisions this way. The position of LEDs may be an additional indicator of alignment - if the camera was tilted after taking the iluminated picture, it would show.
Below are two examples of tracks that have escaped shown in two ways:


.. plot:: statusreport/borderCheck/showBorders2.py  
  :width: 800

Coordinates of borders assumed for the rest of the analysis (in pixels):

* Left: x = 82
* Right: x = 380
* Top: y = 20
* Bottom: y = 494

.. note::
    For camera binning = 2, border coordinates should be divided by 2.