
import imageio as io
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

imOrig = io.imread('../raw/R076_00102.new.png')
xBorder0 = 82
xBorder1 = 380
yBorder0 = 30
yBorder1 = 494

fig, ax = plt.subplots(figsize=(6.4,6.4))
ax.set_title('R076_00102')
ax.imshow(imOrig, cmap='hot')
ax.set_xlim(0, imOrig.shape[1])
ax.set_ylim(imOrig.shape[0], 0)
ax.set_xlabel('px')
ax.set_ylabel('px')
ax.vlines(xBorder0, 0, imOrig.shape[0], color='red')
ax.vlines(xBorder1, 0, imOrig.shape[0], color='red')
ax.hlines(yBorder0, 0, imOrig.shape[1], color='red')
ax.hlines(yBorder1, 0, imOrig.shape[1], color='red')
plt.show()
