import imageio as io

eventFile = '../eventList.txt'

with open(eventFile) as f:
    lines = f.readlines()
for line in lines:
    event = line[:-1]
    print(event)
    im = io.imread(f'../raw/{event}.new.png')
    # borders for binning = 1
    xBorder0 = 82
    xBorder1 = 380
    yBorder0 = 30
    yBorder1 = 494
    if im.shape[0] == 256:
        xBorder0 = xBorder0 // 2
        xBorder1 = xBorder1 // 2
        yBorder0 = yBorder0 // 2
        yBorder1 = yBorder1 // 2
    code = """
import imageio as io
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

imOrig = io.imread('../raw/{}.new.png')
xBorder0 = {}
xBorder1 = {}
yBorder0 = {}
yBorder1 = {}

fig, ax = plt.subplots(figsize=(6.4,6.4))
ax.set_title('{}')
ax.imshow(imOrig, cmap='hot')
ax.set_xlim(0, imOrig.shape[1])
ax.set_ylim(imOrig.shape[0], 0)
ax.set_xlabel('px')
ax.set_ylabel('px')
ax.vlines(xBorder0, 0, imOrig.shape[0], color='red')
ax.vlines(xBorder1, 0, imOrig.shape[0], color='red')
ax.hlines(yBorder0, 0, imOrig.shape[1], color='red')
ax.hlines(yBorder1, 0, imOrig.shape[1], color='red')
plt.show()
""".format(event, xBorder0, xBorder1, yBorder0, yBorder1, event)
    event = event.lower()
    with open(f'{event}Border.py', 'w') as f:
        f.write(code)