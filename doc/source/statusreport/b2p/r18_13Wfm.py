import matplotlib.pyplot as plt
import numpy as np

from SmartLoad import SmartLoad

# /home/adam/Documents/zn54/treco/doc/source/statusreport/images/R038_00226.new_scope.meta
wfmObj = SmartLoad('raw/R018_00013.new_scope.meta', channel=0)
wfmObj.loadChannel(8527.96, 8535.00)

bg = 776
x = wfmObj.x
y = -wfmObj.data - bg
xbin = x[::3]
ybin = y + np.roll(y, 1) + np.roll(y, 2)
ybin = ybin[::3]

fig, (ax, ax2) = plt.subplots(nrows=2)
ax.set_title('Waveform (after bg reduction)')
ax.set_xlabel('Time [us]')
ax.set_ylabel('ADC channel')
ax.plot(x, y)

ax2.set_title('Waveform (binning=3)')
ax2.set_xlabel('Time [us]')
ax2.set_ylabel('ADC channel')
ax2.plot(xbin, ybin)
plt.show()
