import imageio as io
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

x0 = 106
y0 = 138

imOrig = io.imread('raw/R018_00013.new.png')
imPost = io.imread('cR018_00013.png')
imOrig = imOrig[y0:y0 + imPost.shape[0], x0:x0 + imPost.shape[1]]

fig, (ax1, ax2) = plt.subplots(figsize=[6.4, 2.4], ncols=2)
plt.tight_layout()
norm = Normalize(np.amin(imOrig), np.amax(imOrig))
ax1.axis('off')
ax2.axis('off')
ax1.imshow(imOrig, cmap='hot', norm=norm)
ax2.imshow(imPost, cmap='hot', norm=norm)
ax1.set_title('Original (cropped)')
ax2.set_title('After postprocessing')

plt.show()
