import numpy as np
import matplotlib.pyplot as plt

R = 8.31446
T0 = 273.2
p0 = 101325  # Pa


heliumFlow = 30
argonFlow = 70
cf4Flow = 2

Mhe = 4.002602
Mar = 39.948
Mcf4 = 88.0043


def findNearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx, array[idx]


def singleGasDensity(M, p, Tcels):
    return p * M / (R * (Tcels + T0))


def mixDensity(densities, flows):
    density = 0
    for i in range(len(densities)):
        density += densities[i] * flows[i]
    return density / sum(flows)


fig, (ax, ax2) = plt.subplots(nrows=2)

temps = []
densities = []
tmin = 20
tmax = 30
temps = np.arange(tmin, tmax, 0.1)
for i in range(len(temps)):
    t = temps[i]
    densities.append(mixDensity(
        (singleGasDensity(Mhe, p0, t),
         singleGasDensity(Mar, p0, t),
         singleGasDensity(Mcf4, p0, t)),
        (heliumFlow, argonFlow, cf4Flow)) / 1000)
ax.plot(temps, densities)
idx25 = findNearest(temps, 25)
dens25 = densities[idx25[0]]
densP10 = findNearest(densities, dens25 * 1.01)
densM10 = findNearest(densities, dens25 * 0.99)
ax.set_xlabel('temperature [deg. C]')
ax.set_ylabel('gas density [g/l]')
ax.hlines(dens25, tmin, tmax, linestyles='dashed')
ax.hlines(dens25 * 1.01, tmin, tmax, linestyles='dashed')
ax.hlines(dens25 * 0.99, tmin, tmax, linestyles='dashed')
ax.annotate('{:.3f} g/l @ 25 deg. C'.format(dens25), (20, dens25 + 0.0001))
ax.annotate('+1%', (20, dens25 * 1.01 + 0.0001))
ax.annotate('-1%', (20, dens25 * 0.99 + 0.0001))
ax.grid()
# ax.annotate('density @ 25 deg.', (20, dens25 + 0.001))
print(np.polyfit(temps, densities,1))

densities = []
presMin = 970
presMax = 1056
pressures = np.arange(presMin, presMax, 1)
for i in range(len(pressures)):
    p = pressures[i]
    densities.append(mixDensity(
        (singleGasDensity(Mhe, p * 100, 25),
         singleGasDensity(Mar, p * 100, 25),
         singleGasDensity(Mcf4, p * 100, 25)),
        (heliumFlow, argonFlow, cf4Flow)) / 1000)
ax2.plot(pressures, densities)
ax2.set_xlabel('pressure [hPa]')
ax2.set_ylabel('density [g/cm3]')
idx25 = findNearest(pressures, p0 / 100)
dens25 = densities[idx25[0]]
densP10 = findNearest(densities, dens25 * 1.01)
densM10 = findNearest(densities, dens25 * 0.99)
ax2.hlines(dens25, presMin, presMax, linestyles='dashed')
ax2.hlines(dens25 * 1.01, presMin, presMax, linestyles='dashed')
ax2.hlines(dens25 * 0.99, presMin, presMax, linestyles='dashed')
ax2.annotate('{:.1f}'.format(dens25), (20, dens25 + 0.0001))
ax2.annotate('+1%', (970, dens25 * 1.01 + 0.0001))
ax2.annotate('-1%', (970, dens25 * 0.99 + 0.0001))
ax2.grid()
print(np.polyfit(pressures, densities,1))
plt.show()
