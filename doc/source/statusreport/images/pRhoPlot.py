import numpy as np
import matplotlib.pyplot as plt

R = 8.31446
T0 = 273.2
p0 = 101325  # Pa


heliumFlow = 30
argonFlow = 70
cf4Flow = 2

Mhe = 4.002602
Mar = 39.948
Mcf4 = 88.0043


def findNearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx, array[idx]


def singleGasDensity(M, p, Tcels):
    return p * M / (R * (Tcels + T0))


def mixDensity(densities, flows):
    density = 0
    for i in range(len(densities)):
        density += densities[i] * flows[i]
    return density / sum(flows)


rhoHe = singleGasDensity(Mhe, p0, 25)
rhoAr = singleGasDensity(Mar, p0, 25)
rhoCF4 = singleGasDensity(Mcf4, p0, 25)

flows = np.arange(1, 3, 0.05)
densities = np.zeros(len(flows))
for i in range(len(flows)):
    densities[i] = mixDensity((rhoHe, rhoAr, rhoCF4), (heliumFlow, argonFlow, flows[i]))
plt.plot(flows, densities)

idxCenter = findNearest(flows, 2)[0]
densCenter = densities[idxCenter]
densP1 = findNearest(densities, densCenter * 1.01)
densM1 = findNearest(densities, densCenter * 0.99)
plt.hlines(densCenter, flows[0], flows[-1], linestyles='dashed')
plt.hlines(densCenter * 1.01, flows[0], flows[-1], linestyles='dashed')
plt.hlines(densCenter * 0.99, flows[0], flows[-1], linestyles='dashed')
plt.annotate('{:.1f}'.format(densCenter), (20, densCenter + 0.4))
plt.annotate('+1%', (20, densCenter * 1.01 + 0.4))
plt.annotate('-1%', (20, densCenter * 0.99 + 0.4))


plt.show()


heliumDensity0 = singleGasDensity(Mhe,p0,0)
argonDensity0 = singleGasDensity(Mar,p0,0)
cf4Density0 = singleGasDensity(Mcf4,p0,0)

print('Helium density at 0: {:.4f}'.format(heliumDensity0))
print('Argion density at 0: {:.4f}'.format(argonDensity0))
print('CF4 density at 0: {:.4f}'.format(cf4Density0))

print(mixDensity((heliumDensity0 ,argonDensity0, cf4Density0),(heliumFlow,argonFlow,cf4Flow)))
