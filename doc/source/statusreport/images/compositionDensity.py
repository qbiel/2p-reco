import numpy as np
import matplotlib.pyplot as plt

R = 8.31446
T0 = 273.2
p0 = 101325  # Pa

heliumFlow = 30
argonFlow = 70
cf4Flow = 2

Mhe = 4.002602
Mar = 39.948
Mcf4 = 88.0043


def findNearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx, array[idx]


def singleGasDensity(M, p, Tcels):
    return p * M / (R * (Tcels + T0))


def mixDensity(densities, flows):
    density = 0
    for i in range(len(densities)):
        density += densities[i] * flows[i]
    return density / sum(flows)


rhoHe = singleGasDensity(Mhe, p0, 25)
rhoAr = singleGasDensity(Mar, p0, 25)
rhoCF4 = singleGasDensity(Mcf4, p0, 25)

fig, (ax, ax2, ax3) = plt.subplots(nrows=3)

# CF4
flows = np.arange(1, 3, 0.05)
densities = np.zeros(len(flows))
for i in range(len(flows)):
    densities[i] = mixDensity((rhoHe, rhoAr, rhoCF4), (heliumFlow, argonFlow, flows[i]))
ax.plot(flows, densities)

idxCenter = findNearest(flows, 2)[0]
densCenter = densities[idxCenter]
densP1 = findNearest(densities, densCenter * 1.01)
densM1 = findNearest(densities, densCenter * 0.99)
ax.hlines(densCenter, flows[0], flows[-1], linestyles='dashed')
ax.hlines(densCenter * 1.01, flows[0], flows[-1], linestyles='dashed')
ax.hlines(densCenter * 0.99, flows[0], flows[-1], linestyles='dashed')
ax.annotate('{:.1f}'.format(densCenter), (20, densCenter + 0.001))
ax.annotate('+1%', (1.1, densCenter * 1.01 + 0.002))
ax.annotate('-1%', (1.1, densCenter * 0.99 + 0.002))
ax.set_xlabel('CF4 flow [ml/min]')
ax.set_ylabel('gas mix density [g/l]')

plt.show()

# Ar
flows = np.arange(1, 3, 0.05)
densities = np.zeros(len(flows))
for i in range(len(flows)):
    densities[i] = mixDensity((rhoHe, rhoAr, rhoCF4), (heliumFlow, argonFlow, flows[i]))
ax2.plot(flows, densities)