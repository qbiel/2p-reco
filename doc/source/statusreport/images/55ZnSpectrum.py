import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

df = pd.read_csv('./55zn1pFits/results.txt', delimiter='\t')
print(df)
energies = df['E0']

fig, ax = plt.subplots()
ax.hist(energies, bins = np.arange(200,4100,100))
ax.set_xlabel('Energy [keV]')
ax.set_ylabel('Counts')
ax.set_xticks(np.arange(0,4100, 500))
ax.set_title('Zn-55 1p energies')
plt.show()