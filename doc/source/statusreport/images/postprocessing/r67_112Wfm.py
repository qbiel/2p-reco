
import matplotlib.pyplot as plt
import pandas as pd
from SmartLoad import SmartLoad
import numpy as np

df = pd.read_csv('pmtData551p.txt', delimiter='\t')
zi = df.loc[df['# event'] == 'R067_00112'].iloc[0]['zi']
zf = df.loc[df['# event'] == 'R067_00112'].iloc[0]['zf']
bg = df.loc[df['# event'] == 'R067_00112'].iloc[0]['mu']
wfmObj = SmartLoad('R067_00112.new_scope.meta', channel=0)
wfmObj.loadChannel(zi, zf)
x = wfmObj.x
y = -wfmObj.data + bg

fig, (ax1, ax2) = plt.subplots(nrows=2, sharex = True, figsize = [6.4, 4.])
plt.subplots_adjust(hspace = 0.5)
ax1.set_title('Waveform (after bg reduction), binning=1')
ax1.set_ylabel('ADC channel')
ax1.plot(x, y)

xbin = x[::3]
ybin = y + np.roll(y, -1) + np.roll(y, -2)
ybin = ybin[::3]
print(xbin.shape)
print(ybin.shape)
ax2.set_title('Waveform (after bg reduction), binning=3')
ax2.set_xlabel('Time [us]')
ax2.set_ylabel('ADC channel')
ax2.plot(xbin, ybin)
plt.show()