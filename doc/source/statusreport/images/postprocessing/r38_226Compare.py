import imageio as io
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

imOrig = io.imread('R038_00226.new.png')
imPost = io.imread('r38_226.png')
imOrig = imOrig[145:145 + imPost.shape[0], 76:76 + imPost.shape[1]]

fig, (ax1, ax2) = plt.subplots(figsize=[6.4, 2.4], ncols=2)
plt.tight_layout()
norm=Normalize(np.amin(imOrig), np.amax(imOrig))
ax1.axis('off')
ax2.axis('off')
ax1.imshow(imOrig, cmap='hot',norm=norm)
ax2.imshow(imPost, cmap='hot',norm=norm)
ax1.set_title('Original (cropped)')
ax2.set_title('After postprocessing')

plt.show()