import pandas as pd

pmtDataFile = 'pmtData551p.txt'
df = pd.read_csv(pmtDataFile, delimiter='\t')
df.set_index('# event', inplace=True)
for index, row in df.iterrows():
    filename = index
    run = int(filename[1:4])
    event = int(filename[5:10])
    code = """
import matplotlib.pyplot as plt
import pandas as pd
from SmartLoad import SmartLoad
import numpy as np

df = pd.read_csv('{0}', delimiter='\\t')
zi = df.loc[df['# event'] == '{1}'].iloc[0]['zi']
zf = df.loc[df['# event'] == '{1}'].iloc[0]['zf']
bg = df.loc[df['# event'] == '{1}'].iloc[0]['mu']
wfmObj = SmartLoad('{1}.new_scope.meta', channel=0)
wfmObj.loadChannel(zi, zf)
x = wfmObj.x
y = -wfmObj.data + bg

fig, (ax1, ax2) = plt.subplots(nrows=2, sharex = True, figsize = [6.4, 4.])
plt.subplots_adjust(hspace = 0.5)
ax1.set_title('Waveform (after bg reduction), binning=1')
ax1.set_ylabel('ADC channel')
ax1.plot(x, y)

xbin = x[::3]
ybin = y + np.roll(y, -1) + np.roll(y, -2)
ybin = ybin[::3]
print(xbin.shape)
print(ybin.shape)
ax2.set_title('Waveform (after bg reduction), binning=3')
ax2.set_xlabel('Time [us]')
ax2.set_ylabel('ADC channel')
ax2.plot(xbin, ybin)
plt.show()""".format(pmtDataFile, filename)

    with open(f'r{run}_{event}Wfm.py', 'w') as f:
        f.write(code)