import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter

from SmartLoad import SmartLoad

# /home/adam/Documents/zn54/treco/doc/source/statusreport/images/R038_00226.new_scope.meta
wfmObj = SmartLoad('R047_00040.new_scope.meta', channel=0)
wfmObj.loadChannel(399.7, 401.5)

bg = 775
x = wfmObj.x
y = -wfmObj.data - bg

fig, (ax, ax2) = plt.subplots(nrows=2)
plt.tight_layout()
ax.set_title('Waveform (after bg reduction)')
ax.set_xlabel('Time [us]')
ax.set_ylabel('ADC channel')
ax.plot(x, y)

ax2.set_title('Waveform after gaussian smoothing - sigma=1 sample')
ax2.set_xlabel('Time [us]')
ax2.set_ylabel('ADC channel')
ax2.plot(x, gaussian_filter(y, 1))
plt.show()