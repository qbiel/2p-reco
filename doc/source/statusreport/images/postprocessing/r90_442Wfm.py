import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter

from SmartLoad import SmartLoad

# /home/adam/Documents/zn54/treco/doc/source/statusreport/images/R038_00226.new_scope.meta
wfmObj = SmartLoad('R090_00442.new_scope.meta', channel=0)
wfmObj.loadChannel(5022.9, 5023.6)

bg = 617
x = wfmObj.x
y = -wfmObj.data - bg

fig, ax = plt.subplots()
plt.tight_layout()
ax.set_title('Waveform (after bg reduction)')
ax.set_xlabel('Time [us]')
ax.set_ylabel('ADC channel')
ax.plot(x, y)

plt.show()