import matplotlib.pyplot as plt

from SmartLoad import SmartLoad

# /home/adam/Documents/zn54/treco/doc/source/statusreport/images/R038_00226.new_scope.meta
wfmObj = SmartLoad('R038_00226.new_scope.meta', channel=0)
wfmObj.loadChannel(709.4, 710.6)

bg = 646
x = wfmObj.x
y = -wfmObj.data - bg

fig, ax = plt.subplots()
ax.set_title('Waveform (after bg reduction)')
ax.set_xlabel('Time [us]')
ax.set_ylabel('ADC channel')
ax.plot(x, y)
plt.show()