
import imageio as io
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
import treco.postprocessing as post

imOrig = io.imread('./newCameraBorders.png')
imEvent = io.imread('./R067_00088.new.png')
imEvent2 = io.imread('./R068_00218.new.png')
xBorder0 = 82
xBorder1 = 380
yBorder0 = 20
yBorder1 = 494
imOrig = np.roll(imOrig, 5, 1)  # correct for camera tilt

fig, (ax, ax2) = plt.subplots(figsize=(12.8, 6.4), ncols=2)
fig.tight_layout()
ax.imshow(imOrig, cmap='hot', alpha=0.7)
ax.imshow(imEvent, cmap='hot', alpha=0.7)
ax.set_title('R067_00088')
ax.set_xlim(0, imOrig.shape[1])
ax.set_ylim(imOrig.shape[0], 0)
ax.set_xlabel('px')
ax.set_ylabel('px')
imEvent -= 1400
imEvent2 = np.ma.masked_where(imEvent2 < 2000, imEvent2)
imEvent2 = imEvent2 * 2
ax2.imshow(imOrig, cmap='gray')
ax2.imshow(imEvent2, cmap='hot')
ax2.set_title('R068_00218')
ax2.set_xlim(0, imOrig.shape[1])
ax2.set_ylim(imOrig.shape[0], 0)
ax2.set_xlabel('px')
ax2.set_ylabel('px')
plt.show()
