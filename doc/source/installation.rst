Installation
====================

Requirements
------------
* Python >= 3.9.7
* Numpy >= 1.20
* Matplotlib >= 3.3
* Xarray >= 0.19
* Scipy >= 1.6
* Tqdm > 4.56
* Imageio >= 2.9
* opencv-python-headless >= 4.7.0

Optional dependencies
######################
* IPython >= 7.27 - although not required for basic calculations and parameter scanning,
  *treco* heavily depends on IPython for interactivity with Matplotlib,
  so if you plan to use any of the viewers in *treco*, you must
  use it with IPython.


Download the latest version of *treco* and *scopereaders* dependency:

.. code-block:: console

   $ git clone https://gitlab.com/qbiel/otpc-scope-readers.git
   $ git clone https://gitlab.com/qbiel/2p-reco.git
   $ pip install -e otpc-scope-readers
   $ pip install -e 2p-reco
