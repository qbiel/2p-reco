Fitting algorithms
===================

As stated in the previous section, comparing real life events to reconstructions is essentially a least squares problem - the best :math:`(e_0, \phi, \theta, ...)` parameter set gives the lowest :math:`S_{total}(e_0, \phi, \theta, ...)` value. As such, it can be solved using a variety of known and well-established methods. In general, solving methods fall into two categories: differential methods and direct searches. Differential methods, as the name implies, focus on finding the minimum using a Jacobian of a model function, while direct searches evaluate S-functions for different parameter values without using derivatives.

Both solving method types have their upsides and downsides. While differential methods are generally faster, their solution tends to depend on starting parameter values and they don't always handle global minima properly. Additionally, a Jacobian for a loss function has to be approximated numerically. Direct search methods tend to be simpler, but also slower.

Here are a couple of fitting algorithms suggested for use with *treco*.

Brute force direct search
--------------------------

This is the simplest possible direct search method - :math:`S` function is evaluated for every parameter combination in given parameter space with a given step and then announces the lowest :math:`S` value as the best reconstruction.
This shocking simplification may seem like a step backwards, but it provides some powerful advantages:

* the solution depends only on step value (as long as it lies within given parameter space)
* changes of :math:`S` value around the minimum can provide information on minimum stability and uncertainties
* no need for initial guesses (but they help)
* it's dead simple to implement

Of course, those advantages come with a cost, which is speed. Having to prepare a reconstruction for every combination takes a lot of time, even though *treco* is cutting some corners.
Large parameter set doesn't help either - in typical *treco* scanner for a single particle event there are 10 parameters - initial energy e0, two angles :math:`\phi` and :math:`\theta`, initial positions x,y,z and
two smoothing and two scaling factors for image and waveform. Every additional track adds three more parameters.
This makes the method practically useless for events with more than two tracks.

Example:

Let's assume we would like to make a parameter scan of a single-track event. Parameter space is as follows:

* energy: 1000-2000 keV with 100 keV steps (10)
* vertical angle: 50-70 degrees with 5 degrees steps (4)
* horizontal angle: 150-170 degrees with 5 degrees steps (4)
* x0 position: 50-56 px with 1 px steps (6)
* y0 position: 40-46 px with 1 px steps (6)
* z0 position: 0-0.1 us with 0.02 us steps (5)
* image sigma: 1-4 px with 1 px steps (3)
* waveform sigma: 2-5 with 1 sample steps (3)
* image & waveform scaling: 0.8-1.5 with 0.1 steps (7 and 7)

This parameter space consists of almost 13 million points. Decreasing the step size twice for energy and angles increases number of points to more than 100 million points. If, keeping old step sizes, we would like to analyze a two-track event with the same ranges and steps for second track as for the first track we would get 
:math:`2 \cdot 10^9` points. As you can see, increasing parameter space ranges, decreasing step sizes and analyzing multiparticle events can quickly increase the number of points to process to riddiculous values.
Therefore it is very important to keep parameter ranges reasonably narrow and steps reasonably big.

During the development, several variations of a brute force algorithm were tested. Initially E_0, \phi and \theta were used as parameters describing a track with weighted S_total method fot getting a global minimum, but very quickly they were changed to imLength, wfmLength and \theta. This change eventually led to development of approximation of independence method.

Full brute force
##################

This method creates a 10-dimensional parameter matrix (13 in two-track event) where every point is a value of S_total for a certain combination of parameters. For practicality, two parameter matrices (one for S_im and one for S_wfm) with fewer dimensions (for example no z0 in S_im) are created first, then both are adequately expanded and added to each other element-wise with weights. Analyzing the parameter matrix gives information on the best fit (global minimum), but also on parameter uncertainties and correlations between parameters.

Due to a very large size, a parameter matrix in not saved in its entirety, but some of the dimensions are compressed.
Some parameters are less important for the analysis than others - they are called *subparameters*.
Initial tracks positions, diffusion factors and scales are subparameters - we are not interested in their uncertainties or correlation, only that they are set to give the best fit. Eventually, the parameter matrix has 3 dimensions (6 for two-track event), where each point is a value of S-function for coordinates of this point (E0, \theta, \phi for example) and *best set* of subparameters for these coordinates.

Brute force with analytical fit for scales
###########################################

Fitting the amplitude of the signal to data when all other parameters are set is a linear least-squares problem and can be solved analytically. Therefore two subparameters - scale_C and scale_W - don't have to be found by brute force.
This greatly reduces the number of parameter combinations to be analyzed and speeds up the algorithm significantly.

Analytical solution of scale fitting
+++++++++++++++++++++++++++++++++++++

Consider a model of a signal before scaling (image or waveform) :math:`f(x; E_0, \phi..)` and a signal from data :math:`y(x)`. The model signal intensity is a function of position *x* and parameters, but not a function of amplitude. Scaling multiplies each *x* point by the same value *a*. The comparison function can be expressed as:

.. math::

    S(E_0, ..., a) = \sum_x (y(x) - a * f(x; E_0,...))^2

We are interested in minimizing *S* for a, so finding a such that :math:`\frac{\partial S}{\partial a} = 0`:

.. math::
     \frac{\partial S}{\partial a} = \sum_x 2(y(x) - a \cdot f(x; E_0,...)) \cdot f(x; E_0...) = \\
                                   = \sum_x 2y(x) \cdot f(x; E_0...) - 2a \cdot f(x; E_0,...)^2 = 0 \\
    \sum_x y(x) \cdot f(x; E_0...)  = a  \cdot \sum_x f(x; E_0,...)^2 \\
                                 a  = \frac{\sum_x y(x) \cdot f(x; E_0...)}{\cdot f(x; E_0,...)^2}

Brute force with approximation of independence
###############################################

Brute force search can also be used with approximation of independence method of finding best parameter set.
As there are fewer parameters in *S_{im}* and *S_{wfm}*, fitting is much faster than full brute force.