Comparison function
====================

In order to quantitatively establish the quality of the reconstruction, the non-linear least squares method can be used. Two comparison functions are introduced: one for an image comparison, one for a waveform:

.. math::

   S_{im} =  \sum_{x,y} [im_{exp}(x,y) - im_{sim}(x,y)]^2
   
   S_{wfm} =  \sum_z [wfm_{exp}(z) - wfm_{sim}(z)]^2

:math:`S_{im}` and :math:`S_{wfm}` are functions of track parameters.
:math:`S_{im}` depends on:

    * particles initial energies E0
    * horizontal angles of tracks \theta
    * vertical angles of tracks \phi
    * initial tracks position x0 & y0
    * image smoothing factor :math:`\sigma_c`
    * image scaling factor 

That is 7 parameters for 1-track event, 10 parameters for 2-track. :math:`S_{wfm}`, on the other hand, depends on:
    * particles initial energies E0
    * vertical angles of tracks \phi
    * initial tracks position z0
    * waveform diffusion factor :math:`\sigma_w`
    * waveform scaling factor

That is 5 parameters for 1-track event, 7 parameters for 2-track.

A different set of parameters may be used, for example initial energy and vertical angle can both be expressed as functions of horizontal and vertical range (imL and wfmL). The number of parameters, however, stays the same.

Some parameters appear in both :math:`S_{im}` and :math:`S_{wfm}`, which means if those two functions would be minimized independently, there is no guarantee that, for instance, best :math:`E_0` would be the same for image and waveform.
This suggests that both functions should be minimized together.
Several approaches are proposed to tackle this problem.

Weighted :math:`S_{total}`
--------------------------------------------

This approach introduces a total comparison function:

.. math::

    S_{total} = \frac{S_{im}}{w_{im}} + \frac{S_{wfm}}{w_{wfm}}

Minimizing :math:`S_{total}` should provide a fit that is 'equally good' for image and waveform.

The issue is the choice of weights, :math:`w_{im}` and :math:`w_{wfm}`. They should be chosen so that neither image nor waveform 'dominate' the fit. 
Some possible weights choices:

    1. Weights are connected with degrees of freedom, so roughly a numer of points fitted to image and waveform. However, simply taking numer of pixels/samples as weights has a drawback that weights depend on image/waveform size, which is usually arbitrary.
    2. A variation of solution 1: weights are number of points fitted to image/waveform, but only points composing a track. In case of a waveform it would be signal length in samples, but in case of an image a number of non-zero pixels on post-processed image.
    3. It can be argued that track width on the image carries no information, so weights should be sum of track lengths on an image and signal length in samples.

It's hard to determine which choice of weights is most justified. Therefore, different approaches were studied. On the other hand, some test showed that choice of weights has little influence on the global minimum of :math:`S_{total}`, but it has influence on the errorbars.

Approximation of independence
------------------------------

This approach utilizes a hypothesis that in some conditions and with a clever choice of parameters camera image and PMT waveform can be minimized independently.

Let's consider a single-particle decay. As stated before, such a track can be fully described by 10 parameters, including 
particle initial energy :math:`E_0` and vertical angle :math:`\phi`. We can choose to describe an event using horizontal and vertical range components instead of :math:`E_0` and :math:`\phi`:

.. math:: 

    imL = range(E_0) * cos \phi

    wfmL = range(E_0) * sin \phi

Now let's see what happens to a PMT signal when we set a constant vertical range and are varying horizontal range.
As we increase a horizontal component, the initial energy increases, so the amplitude of a waveform also increases.
But the shape of the signal roughly stays the same. See the example below:

[plots here]


The same happens the other way around - when we keep the horizontal component constant and vary the vertical one, the shape of an image signal is approximately the same. This brings the conclusion that in theory we could find initial energy and  vertical angle \phi from camera signal only (or PMT signal only) by measuring signal integral and length **if we had an absolute energy calibration** between pixel/sample brightness and energy deposit.

However, there is no absolute calibration for OTPC (for now) and for that reason scaling parameters for image and waveform are introduced. This means that signal amplitude no longer gives any information on track inclination.
Furthermore, assuming waveform shape differences are negligible when varying horizontal range component (and *vice-versa*)
it can be stated that **image signal does not depend on a vertical range component and waveform does not depend on a horizontal range component.** Comparison functions can now be rewritten:

.. math::

    S_{im}(E_0, \phi, \theta, x_0, y_0, \sigma_C, scale_C) \approx S_{im}(imL, \theta, x_0, y_0, \sigma_C, scale_C)
    
    S_{wfm}(E_0, \phi, z_0, \sigma_W, scale_W) \approx S_{wfm}(wfmL, z_0, \sigma_W, scale_W)

Now both comparison functions depend on different parameters, so they can be minimized independently.

For multi-particle events there is an important difference - while the overall signal amplitude still gives no information on range components, the *ratio* of single track components of the signal (strictly speaking the ratio of their integrals) gives information on the ratio of single track energies. Comparison functions for two-track event take the form of:

.. math::

    S_{im}(E_{0 1,2}, \phi_{1,2}, \theta_{1,2}, x_0, y_0, \sigma_C, scale_C) \approx S_{im}(\frac{E_{01}}{E_{02}}, imL_{1,2}, \theta_{1,2}, x_0, y_0, \sigma_C, scale_C)
    
    S_{wfm}(E_{01,2}, \phi_{1,2}, z_0, \sigma_W, scale_W) \approx S_{wfm}(\frac{E_{01}}{E_{02}}, wfmL_{1,2}, z_0, \sigma_W, scale_W)

One parameter is still common for both comparison functions. There are several ideas on how to handle this issue:
    
    * minimize :math:`S_{im}` an :math:`S_{wfm}` independently (either using both horiz/vert conponents or ration and one component as parameters), but take best horizontal components from :math:`S_{im}` minimum and best vertical components from :math:`S_{wfm}`. This ignores ratio parameter, but it may be used to check consistency of both fits. 

    * iterative fitting - using estimated horizontal components (from manual estimation, for example) as given minimize :math:`S_{wfm}(wfmL_1, wfmL_2, ...)` (:math:`E_0` ratio or :math:`imL_{1,2}` are not fit parameters). Then use vertical components from best waveform fit as given when minimizing :math:`S_{im}(imL_1, imL_2...)`. Having best horizontal components use them to fit :math:`S_wfm` for :math:`wfmL_1` and :math:`wfmL_2` again. Repeat until best parameters stop changing (probably 1-3 iterations).

