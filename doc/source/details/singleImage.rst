Single track image/waveform creation
====================================

With energy loss profiles generated, *treco* can create an image and a waveform.
Even with multi-track events, single-track images&waveforms are created first and then combined.
At this point smoothing and rescaling is not yet applied - this happens after combining all tracks and is not handled by *treco.Track* (unless explicitly demanded - see tutorial).

Waveform generation
--------------------

This part is easy - loss profile for waveform essentially *is* the waveform. The only factors remaining
are proper orienting and shifting to the right place.

First, an empty waveform of length twice the desired size is created. Note that sampling rate and drift velocity information are no longer needed here - they were already used when creating loss profile.
Depending on the sign of the vertical angle :math:`\phi` (in other words: if the track is going from top to bottom or from bottom to top) loss profile is flipped. Flipping occurs for :math:`\phi > 0`. 
Next, waveform loss profile is added in such a way that a start of the track is **always** in the middle.
Afterwards, a fragment of the waveform of the desired size is selected in such a way that the start of the track is at z0. z0 is always **the start of the track, not necessarily the waveform**. For :math:`\phi > 0`, the end of the track is on the left side of z0.

.. warning::

    If a loss profile is longer than the size of the waveform, it will be trimmed to fit the waveform.

Below are all the steps described above visualized for a proton of energy 1500 keV, vertical angle 30 degrees and initial position z0 = 5 us from the center. The waveform is 2000 samples long with sampling rate of 100 MSa/s.

.. plot:: plots/wfmGenerate.py

.. note::
    For visualization and user interaction, *treco* uses a time scale in microseconds on X axis of the waveform
    with 0 always in the middle. Internally, given position in us is rounded to the nearest sample if necessary.

Image generation
-----------------

.. warning::
    The procedure described below has been modified multiple times during the development and details may be outdated, but the general idea stays the same.

In contrast to waveforms, images have two dimensions which makes things more complicated, because a 1D loss profile has to be converted into a 2D object and a horizontal angle :math:`\theta` has to be taken into consideration.

To achieve that, *treco* imposes a loss profile on a single row of pixels starting in the middle of the picture, thus creating an idealized image of a perfectly horizontal track (:math:`\theta = 0`). Afterwards it rotates an image by a desired :math:`\theta` angle. For rotation, *treco* uses *scipy.ndimage.rotate* function.

This method has a drawback - if a track is longer than half of image width, the initial horizontal track will not fit. To prevent that, initial image width&length are three times bigger than the final image.
This way even a track starting at the bottom left-hand side and ending at the top right-hand side will be rendered correctly. After rotation, a fragment of the image is selected and nominated the final image.
The position of the fragment depends on a desired x,y position.

All those steps are once again visualized with a proton of energy 500 keV, 30 degrees vertical angle and 60 degrees horizontal. A small image size of (20,20) pixels is used to better see individual pixels. Image pixel size is 660um with binning 1: 

.. plot:: plots/imGenerate.py

.. note::
    When creating multiparticle events, single track images are left at stage 3 (without selcting image fragment). Setting the final image size and :math:`x_0` & :math:`y0` positions happens as the last stage of multitrack image composition. Same applies to waveform generation.