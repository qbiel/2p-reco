Generating energy loss profiles
================================

Energy loss profile is a distribution of particle energy deposited along the track.
Heavy ions follow a characteristic loss profile known as the Bragg curve, where energy loss gets higher towards the end of the track. In image & waveform reconstruction, loss profile determines pixel intensities and sample values. Therefore, *treco* generates two loss profiles - one for an image and one for a waveform.

*treco* generates loss profiles essentially by taking a derivative of energy-range function using the following algorithm:

    1. Calculate range for given initial particle energy.
    2. Divide that range into bins of given and equal length.
    3. Calculate energy at the edges of all bins. Set energy at the end of last bin to 0.
    4. Energy loss in a bin is the difference between particle energy on bin edges.
    5. (Correct energy loss for nuclear losses.)


Comments on steps:

| Step 2: 

* Due to rounding, total length of all bins may be slightly larger than track range (up to 1 bin length). Furthermore, the energy loss calculated in the last bin may be slightly higher because there is a minimum energy value allowed by *treco*. It is equal to the energy at the first point in SRIM table (usually 1 keV).

* A quiet assumption is made that initial position of a particle is at the edge of a pixel/sample. 

* It has been found that loss profiles are most reliable when vertical/horizontal range is a multiple of sample/pixel size.

| Step 5: See section on :ref:`nuclear corrections <nuclcorr>`,

The image below shows loss generation algorithm in action:

.. plot:: plots/binningDemo.py
   :width: 800


*treco.Track* has two methods for calculating loss profiles: *treco.Track.makeImageLoss()* and *treco.Track.makeWfmLoss()*. Here is an example of image and waveform loss profiles for a 1 MeV proton at vertical angles of 30 degrees and 70 degrees:

.. plot:: plots/lossExample.py

.. _binning:

Bin sizes
---------
Bin is a segment of the track **(along that track)** that is recorded by one image pixel or one waveform sample.
This means that bin size not only depends on pixel/sample size, but also on track inclination. The image below should provide better understanding:

.. plot:: plots/bin3d.py

*treco* calculates bin sizes using following equations:


.. math:: 

   binSize_{image} = \frac{pixelSize * camBinning}{cos(\phi)}

   binSize_{wfm} = |\frac{driftVelocity}{samplingRate} * \frac{1}{sin(\phi)}|

where :math:`\phi` is an angle between a track and camera plane (vertical angle), *driftVelocity* and *pixelSize* are taken from *treco* configuration file, whereas *camBinning* is a value of 'binning' option set in the camera during taking image (usually 1, 2 or 4).
There are two special cases: totally vertical track (:math:`cos(\phi)=0`) and totally horizontal track (:math:`sin(\phi)=0`). In those cases bin sizes, respectively for image and waveform, are set to infinity (*math.inf*), and loss profile generator essentially puts all particle energy in one bin.

You may have noticed that image bin size doesn't depend on track horizontal angle :math:`\theta`.
Track angle on the image is handled in the next step of the reconstruction.

.. _nuclcorr:

Nuclear losses correction
--------------------------

.. warning::
   Nuclear losses correction is currently disabled in *treco* and will be returned in future release.

Heavy ions can lose energy in gas by ionization or nuclear interactions, and only the former interaction is recorded on images and waveforms. To take that fact into consideration, *treco* can rescale energy loss for every bin to only include ionization losses. The value of rescaling factor is taken from SRIM source table:
a ratio of :math:`\frac{dE/dx_{elec.}}{dE/dx_{elec.} + dE/dx_{nucl.}}`
is calculated for each particle energy in the table and, similarly to energy-range, a cubic interpolation is performed between table points.

It has been found that for protons (at least in Ar/He/CF4 60/40/2 gas mixture) nuclear correction factor is negligible. It's value is close to 1 on the whole track except for the end, although it never drops below 0.9.
Therefore, to speed up calculations, applying the correction is optional in *treco* (not even fully implemented in current version). The plot below shows how the correction factor changes with energy in described conditions. Note that energy range is only 1-100 keV:

.. plot:: plots/nuclearCorrectionTest.py