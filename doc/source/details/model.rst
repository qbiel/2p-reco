Theoretical model
===================

A theoretical model *treco* generates is a set of an image and a waveform that is expected from tracks of given energy, directions and other parameters.

.. toctree::
    :maxdepth: 1
    :Caption: Creating a theoretical model

    srimparse
    lossprofile
    singleImage
    fullImage
