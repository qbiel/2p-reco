Fitting model to data event
===========================

The whole purpose of creating a reconstruction is to compare it with a real-life event.
While *treco.Track* and simulators provided a way to reconstruct an event, it is actually up to the user to decide on the most suitable comparison mechanism. However, *treco* provides a set of tools which you can use to find the best matching reconstruction.

.. toctree::
    :maxdepth: 1

    postprocessing
    statistics
    parameterscan