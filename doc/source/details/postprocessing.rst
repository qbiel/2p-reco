.. _eventpostprocessing:

Event postprocessing
======================

Real life event needs some preparation in order to be compared to a simulation.
*treco.utils* provides some tools for both image & waveform postprocessing. The currently used procedure is described below.

Image background reduction
---------------------------

This process is automatic with some parameters user can tweak arbitrarily. Postprocessing procedure for an image subtracts constant background and cuts the decay out of the original image, leaving all pixels outside the tracks set to 0.

tl;dr:
 1. Crop the image to remove sync LEDs and margins outside the chamber.
 2. Make a histogram of pixel intensities.
 3. Calculate mean pixel intensity in the gaussian peak and standard deviation (exclude salt-and-pepper noise and track pixels)
 4. Subtract mean pixel intensity from the whole image.
 5. Copy the image. Apply a median filter and gaussian filter on the copy.
 6. Get positions of all pixels brighter than an arbitrary threshold (on the copy)
 7. On the original image, preserve the pixels from step 6 and set all other pixels to 0.
 8. Crop the image leaving only the track and small margins on all sides.

First, an image is cropped to remove margins outside the OTPC chamber. This also removes sync LEDs from the image.
Next, a histogram of pixel intensities is prepared. With the histogram we can learn about a background of the image as well as the noise. An example (R038_00226) is shown here:

There are 3 areas of interest here. One is a gaussian shape at low pixel intensities, the other is a long tail on the right hand side of the gaussian that ends at ~6000 ADC channels. At the end of the scale (2\ :sup:`16` \ - 1 ADC) there are also some bright pixels from the decay that for now can be ignored. The center of the gaussian curve is identified as constant background of the image, while its width is a measure of gaussian noise the camera introduces. The tail is probably a salt-and-pepper' noise characteristic for some CCD cameras. While it is difficult to get a measurement of salt-and-pepper noise, it is easier for gaussian noise. To remove constant background, the value of the center of the gaussian is subtracted from the whole image. If a pixel value was lower than the background, it is set to 0. The most important part of this operation is to remove constant background from the proton tracks.

Next, a copy of the image is made and median and gaussian filters are applied to the copy.
Afterwards, a mask is created that covers all pixels with brightness lower than a threshold (usually 2000 ADC channels).
The only area uncovered by the mask should be the decay. Without median filter, some pixels from salt-and-pepper noise might be bright enough to escape masking. Without a gaussian filter, edges of the mask around the decay would be sharp and uneven.
This is why a mask is created from a filtered copy. However filters (especially median filter) can distort the tracks and make fitting unreliable. Therefore the mask created on the copy is eventually applied to the original image; all masked pixels are set to 0. This leaves only undistorted tracks on the image; noise and ion signals outside tracks are removed.
Finally, the image is cut so that tracks take most of the space of the image with ~4 px margin on all sides.

Waveform backgound reduction
-----------------------------

Waveform postprocessing has fever steps than image procedure, but user has to manually select waveform borders.

 1. Manually select left and right borders of the signal of interest.
 2. Grab portions of the waveform from the left and right hand side of the signal. The length of the portions is equal to the length of the signal of interest.
 3. Make a histogram of sample intensities from two empty portions of the waveform. It should follow a gaussian distribution.
 4. Find the mean sample value and a standard deviation.
 5. Subtract the mean sample value from the signal of interest. Standard deviation can be used later for errorbars estimation.


Note on background uniformity:
This procedure assumes that image & waveform background is constant. This may not be true - CCD sensors tend to have a linear background, whereas PMT waveforms have low frequency noise components, so background 'floats'.
Nevertheless, signals on the waveform are short enought that it is safe to assume a constant background along the signal.
Similarly, camera background changes slowly with height on the image, slowly enough to safely assume a constant image background as well.


Smoothing
-----------

Sometimes it is a good idea to smooth both image and waveform using a gaussian filter.
It greatly improves visual inspection of the comparison, and although no tests were yet performed in that direction, the author strongly believes that it improves the comparison more than just visually.

The larger the value of :math:`\sigma` of the filter, the less influence the noise has on the image/waveform. At some point, however, smoothing can probably hurt the resolution. Once again, this remains to be tested.

The author found that a sigma value of 1 pixel for the camera and 1 sample for the waveform is usually sufficient for a smooth event.

Below is an example of 2p decay of zinc-54 before and after smoothing with gaussian filter:

.. plot:: plots/eventSmoothing.py
    :width: 800

Scaling
--------

In case of image, pixel values represent the brightness of light along the track and are given in units dependent on camera settings. In case of waveform, sample values represent the voltage on the output of photomultiplier (so also the brightness of light) and are given in either volts od ADC channels. *treco* scanners scale both image and waveform by dividing all pixels/samples
by the maximum value of an image pixel/wfm sample, similarly to the simulation.
Noise can get in the way - for example a very noisy waveform can have a spike that puts maximum value higher than it should be. This may be helped in two ways - by smoothing and/or by fine tuning the scale of the simulation (see :ref:`smoothing and rescaling <simsmoothing>` of the simulation).

.. note::
    *treco* scanners save event scaling factors for simulation and real life events. Therefore it is possible to estimate absolute detector calibration, that is the ratio between energy deposit in keV and pixel/sample brightness.
