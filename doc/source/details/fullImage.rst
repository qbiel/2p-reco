Multitrack image/waveform composition
=====================================

Once single-track image and waveform are prepared, it is time for final touches.
This is as far as *treco.Track* can take us - procedures described below are performed by two simulators,
*treco.ImageSimulator* and *treco.WfmSimulator*.

Imposing single-track images/waveforms
---------------------------------------

For multi-particle events, images and waveforms of single-track events are simply added to each other. Simulators also force two tracks to set the same image size, sampling, binning etc.
All tracks share the same starting point; also, pixel/sample values still hold information on energy loss in keV, no rescaling was performed yet, so it is safe to add tracks by simply summing.

This step is obviously omitted when dealing with a single-track event.

.. _simsmoothing:

Smoothing, scaling and setting :math:`x0`, :math:`y0` & :math:`z0`
------------------------------------------------------------------
This is the last step of the reconstruction. Due to detector specific effects (like diffusion of electrons drifting in gas or response functions of the camera and photomultiplier) tracks have a certain width. With camera and PMT, these effects can be simulated by using a gaussian filter on both image and waveform. Silicon photomultipliers have a more complicated response function which currently can't be simulated by *treco*. 

*treco* uses *scipy.ndimage.gaussian_filter* function to perform smoothing with a desired smoothing factor :math:`\sigma`.

Optionally, a scaling of pixel/sample values can be performed. This is useful for comparing a reconstruction with a real event. *treco* scales both image and waveform by multiplying original pixel/sample values by *scaleC* and *scaleW* parameters.

.. note::
    Smoothing is applied before scaling.

By scaling we lose an important piece of information from the simulation - up to scaling, by integrating the whole image/waveform we would get the total energy emitted in the event. However, by dividing signal area by *scaleC* or *scaleW* parameter one can recover the total energy.
As a final step, images and waveforms are trimmed to the desired size and tracks are positioned to correct :math:`x0`, :math:`y0` & :math:`z0`.

Below, once again, is a visualization of steps described above. This time we will simulate a two-proton decay with 600 keV and 800 keV protons. 

.. plot:: plots/finalTouch.py