.. _srimparse:

Getting energy-range function from SRIM table
=============================================

First step of creating a theoretical model is to find out a relationship between particle energy and its range.
Function of energy vs range (and *vice-versa*) depends on particle type and a stopping gas mixture.
A common way to get this information is to use SRIM simulation tool. SRIM calculates energy loss, particle range and straggling lengths for a particular ion and a particular gas mixture.
A typical SRIM output file fragment is shown below:

:: 

     ==================================================================
                  Calculation using SRIM-2006 
                  SRIM version ---> SRIM-2012.01
                  Calc. date   ---> July 22, 2021 
     ==================================================================

     Disk File Name = SR.in

     Ion = Hydrogen     [1] , Mass = 1.008 amu

     Density =  1.2407E-03 g/cm3 = 2.7873E+19 atoms/cm3
     Target is a GAS 
     ======= Target  Composition ========
        Atom   Atom   Atomic    Mass     
        Name   Numb   Percent   Percent  
        ----   ----   -------   -------  
          H      1    007.27    000.27   
          C      6    001.82    000.81   
         He      2    027.27    004.07   
         Ar     18    063.64    094.84   
     ====================================
     Bragg Correction = 0.00%
     Stopping Units =  keV/micron   
     See bottom of Table for other Stopping units 

     Ion = Hydrogen     [1] , Mass = 1.008 amu

       Ion        dE/dx      dE/dx     Projected  Longitudinal   Lateral
      Energy      Elec.      Nuclear     Range     Straggling   Straggling
    -----------  ---------- ---------- ----------  ----------  ----------
    999.999 eV    9.965E-03  1.039E-03   39.39 um    39.60 um    30.49 um  
       1.10 keV   1.045E-02  1.017E-03   42.88 um    42.01 um    32.45 um  
       1.20 keV   1.092E-02  9.961E-04   46.36 um    44.32 um    34.35 um  
       1.30 keV   1.136E-02  9.763E-04   49.81 um    46.54 um    36.19 um  
       1.40 keV   1.179E-02  9.576E-04   53.25 um    48.68 um    37.97 um  
       1.50 keV   1.220E-02  9.397E-04   56.66 um    50.74 um    39.71 um  
       1.60 keV   1.261E-02  9.227E-04   60.06 um    52.73 um    41.39 um  
       1.70 keV   1.299E-02  9.065E-04   63.44 um    54.66 um    43.03 um  
       1.80 keV   1.337E-02  8.909E-04   66.80 um    56.53 um    44.62 um  
       2.00 keV   1.409E-02  8.618E-04   73.48 um    60.07 um    47.71 um  
       2.25 keV   1.488E-02  8.286E-04   81.75 um    64.24 um    51.39 um  
       2.50 keV   1.564E-02  7.985E-04   89.95 um    68.16 um    54.90 um  
       2.75 keV   1.636E-02  7.710E-04   98.08 um    71.86 um    58.24 um  
       3.00 keV   1.706E-02  7.458E-04  106.12 um    75.35 um    61.44 um  
       3.25 keV   1.774E-02  7.225E-04  114.07 um    78.66 um    64.52 um  
       3.50 keV   1.840E-02  7.010E-04  121.94 um    81.81 um    67.47 um  
       3.75 keV   1.904E-02  6.810E-04  129.73 um    84.80 um    70.32 um  
       4.00 keV   1.967E-02  6.624E-04  137.43 um    87.65 um    73.06 um  
       4.50 keV   2.088E-02  6.286E-04  152.60 um    92.96 um    78.28 um  
       5.00 keV   2.204E-02  5.987E-04  167.43 um    97.83 um    83.16 um  
       .........

For more information how to use SRIM, check `SRIM website <http://www.srim.org/>`_.

*treco* extracts 'Ion Energy', 'dE/dx' and 'Projected range' columns and applies cubic interpolation
between range and energy points. Both 'dE/dx' columns are used when a nuclear loss correction factor is applied to loss profiles. An example of energy-range function from SRIM table shown above is plotted below:

.. plot:: plots/srimtable.py
   
