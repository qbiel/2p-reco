import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from treco import Track

plt.style.use('./docstyle.mplstyle')
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set_aspect('auto')

t = Track('proton', e0=1500, theta=0, phi=-45)
t.pixelWidth = 500

# track
xs = np.arange(0,5,0.5)
ys = np.zeros(xs.size)
zs = xs / np.tan(45)
ax.plot(xs, ys, zs, lw=10, solid_capstyle='round', color='orange')

# track fragment
xss = np.arange(1,2.1,0.5)
yss = np.zeros(xss.size)
zss = xss / np.tan(45)
ax.plot(xss, yss, zss, lw=10, solid_capstyle='round', color='red')

# bin size width label
label = 'Bin size'
ax.text(1, 0, 0.7, label, (1,0,0.7), fontsize=14)

# bin lines
x1 = [1,1]
y1 = [0,0]
z1 = [0,zs[2]]
ax.plot(x1,y1,z1, '--', color='black', lw=1)
x2 = [2,2]
y2 = [0,0]
z2 = [0,zs[4]]
ax.plot(x2,y2,z2, '--', color='black', lw=1)

# flat track
xs = np.arange(0,5,0.5)
ys = np.zeros(xs.size)
zs = np.zeros(xs.size)
ax.plot(xs, ys, zs, lw=10, solid_capstyle='round', color='orange', alpha = .5)

# track fragment
xss = np.arange(1,2.1,0.5)
yss = np.zeros(xss.size)
zss = np.zeros(xss.size)
ax.plot(xss, yss, zss, lw=10, solid_capstyle='round', color='red', alpha = .5)

# pixel width
label = 'Pixel width'
ax.text(1.5, -0.4, 0, label, (1,0,0), fontsize=14)

ax.axes.xaxis.set_ticklabels([])
ax.axes.yaxis.set_ticklabels([])
ax.axes.zaxis.set_ticklabels([])
plt.grid(True)
ax.set_ylim3d(-1,1)
plt.show()
