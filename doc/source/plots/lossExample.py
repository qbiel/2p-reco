import matplotlib.pyplot as plt
import numpy as np

from treco import Track
import treco.constants as const

pixelSize = 660
driftVelocity = 19300
sampling = 100

t = Track('proton', e0=1000, phi=30)
imLoss30 = t.makeImageLoss(pixelSize=pixelSize)
wfmLoss30 = t.makeWfmLoss(driftVelocity=driftVelocity, sampling=sampling)
imLoss30 = np.pad(imLoss30, (0,1))
wfmLoss30 = np.pad(wfmLoss30, (0,1))

t.phi = 70
imLoss70 = t.makeImageLoss(pixelSize=pixelSize)
wfmLoss70 = t.makeWfmLoss(driftVelocity=driftVelocity, sampling=sampling)
imLoss70 = np.pad(imLoss70, (0,1))
wfmLoss70 = np.pad(wfmLoss70, (0,1))

plt.style.use('./docstyle.mplstyle')
fig, (ax1, ax2) = plt.subplots(ncols=2, nrows=1)
title = '''Proton, e0 = 1MeV,\n\
pixelSize = {}um,\n\
driftVel={:.2f} mm/us,\n\
sampling={} MSa/s'''.format(pixelSize,
                            driftVelocity / 1000,
                            sampling)

ax1.set_title('Image loss profiles')
ax1.plot(imLoss30, label='phi=30')
ax1.plot(imLoss70, label='phi=70')
ax1.set_xlim(0, 100)
ax1.set_ylim(0,imLoss70.max()*(1.1))
ax1.set_xlabel('Bin number')
ax1.set_ylabel('Energy lost in bin [keV]')
ax1.legend(title=title, fontsize=14, title_fontsize=14)

ax2.set_title('Waveform loss profiles')
ax2.plot(wfmLoss30, label='phi=30')
ax2.plot(wfmLoss70, label='phi=70')
ax2.set_xlim(0, 400)
ax2.set_ylim(0,wfmLoss30.max()*(1.1))
ax2.set_xlabel('Bin number')
ax2.set_ylabel('Energy lost in bin [keV]')
ax2.legend(title=title, fontsize=14, title_fontsize=14)


plt.show()
