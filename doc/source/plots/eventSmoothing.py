import matplotlib.pyplot as plt
import os

from treco.utils.ExpImage import ExpImage
from treco.utils.ExpWaveform import ExpWaveform

imagePath = os.path.join(os.path.dirname(os.getcwd()),
                         '../../events/r38_226/r38_226.png')
wfmPath = os.path.join(os.path.dirname(os.getcwd()),
                       '../../events/r38_226/R038_00226.new_scope.meta')

imageObj = ExpImage(imagePath, sigma=1)
wfmObj = ExpWaveform(wfmPath, start=709, stop=711, sigma=1)
image = imageObj._rawImage
wfm = wfmObj._rawwfm
smoothImage = imageObj.image
smoothWfm = wfmObj.wfm

fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(ncols=2, nrows=2)
sigmac0 = '\u03c3 = 0'
sigmac1 = '\u03c3 = 0'
sigmaw0 = '\u03c3 = 0'
sigmaw = '\u03c3 = 0'
propsw = dict(boxstyle='round', facecolor='wheat',  alpha=0.5)
propsi = dict(boxstyle='round', facecolor='brown',  alpha=0.5)
ax1.text(0.05, 0.95, '\u03c3 = 0 px', transform=ax1.transAxes, fontsize=12, color='white',
         verticalalignment='top', bbox=propsi)
ax2.text(0.05, 0.95, '\u03c3 = 1 px', transform=ax2.transAxes, fontsize=12, color='white',
         verticalalignment='top', bbox=propsi)
ax3.text(0.05, 0.95, '\u03c3 = 0 samples', transform=ax3.transAxes, fontsize=12,
         verticalalignment='top', bbox=propsw)
ax4.text(0.05, 0.95, '\u03c3 = 1 sample', transform=ax4.transAxes, fontsize=12,
         verticalalignment='top', bbox=propsw)

ax1.imshow(image, cmap='hot')
ax2.imshow(smoothImage, cmap='hot')
ax3.plot(wfm)
ax3.set_ylim(500,10500)
ax4.plot(smoothWfm)
ax4.set_ylim(500,10500)


plt.show()
