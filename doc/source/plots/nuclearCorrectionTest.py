import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate as scp
import os

import treco.srimUtils as srim

srimFile = os.path.join(os.path.dirname(os.getcwd()),
                        '../../srimTables/srimOutH.txt')
srimDic = srim.SrimOutput(srimFile)

rangeF = scp.interp1d(srimDic.energyArr, srimDic.rangeArr, kind='cubic')
energyF = scp.interp1d(srimDic.rangeArr, srimDic.energyArr, kind='cubic')
dedxElecF = scp.interp1d(srimDic.energyArr, srimDic.dedxElecArr, kind='cubic')
dedxNuclF = scp.interp1d(srimDic.energyArr, srimDic.dedxNuclArr, kind='cubic')
corrF = scp.interp1d(srimDic.energyArr, srimDic.dedxElecArr / (srimDic.dedxElecArr + srimDic.dedxNuclArr), kind='cubic')

plt.style.use('docstyle.mplstyle')
fig, ax = plt.subplots(1, 1)

e = np.arange(1, 100, 1)
corr = corrF(e)
dedxElec = dedxElecF(e)

dedxNucl = dedxNuclF(e)
ax.plot(e, corr, '--', label='Correction function')
ax.set_ylabel('Ion. dE/dx share')
ax.set_xlabel('Particle energy [keV]')
ax.set_ylim(0.5, 1.1)

plt.show()
