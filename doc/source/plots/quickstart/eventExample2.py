import matplotlib.pyplot as plt
import os

from treco import Track, EventViewer

imagePath = os.path.join(os.path.dirname(os.getcwd()),
                         '../../../events/r38_226/r38_226.png')
wfmPath = os.path.join(os.path.dirname(os.getcwd()),
                       '../../../events/r38_226/R038_00226.new_scope.meta')
v = EventViewer()
t = Track('proton', e0=750, phi=-30, theta=45)
v.addTrack(t)
v.x0 = 20
v.y0 = 20
v.imSize = (128, 128)
v.wfmSize = 1000
v.binning = 2
v.loadEventImage(imagePath)
v.loadEventWfm(path=wfmPath, wfmStart=708, wfmStop=712)
v.normalize()
plt.show()
