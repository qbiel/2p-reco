import matplotlib.pyplot as plt
import os

from treco import Track, EventViewer
import treco.constants as const

imagePath = os.path.join(os.path.dirname(os.getcwd()),
                         '../../../events/r38_226/r38_226.png')
wfmPath = os.path.join(os.path.dirname(os.getcwd()),
                       '../../../events/r38_226/R038_00226.new_scope.meta')
v = EventViewer()
t = Track('proton')
t2 = Track('proton')
v.binning = 2

v.loadEventImage(imagePath)
v.loadEventWfm(path=wfmPath, wfmStart=708, wfmStop=712)

v.x0 = 26
v.y0 = 13
v.z0 = 709.7
v.expimage.sigma = 1
v.expwfm.sigma = 3
v.expwfm.bg = 600
v.sigmaC = 1.5
v.sigmaW = 3.5
t.theta = 170
t2.theta = 50

t.setLengths(33 * const.pixelWidth, 0.7 * const.driftVel, False)
t2.setLengths(20 * const.pixelWidth, 0.05 * const.driftVel, True)

v.addTrack(t)
v.addTrack(t2)
v.normalize()
plt.show()
