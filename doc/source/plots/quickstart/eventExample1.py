import matplotlib.pyplot as plt

from treco import Track, EventViewer

v = EventViewer()
t = Track('proton', e0=750, phi=-30, theta=45)
v.addTrack(t)

plt.show()
