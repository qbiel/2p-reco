import matplotlib.pyplot as plt
import treco
t = treco.Track('proton')
phis = []
wfmLs = []
for phi in range(-60, 1, 1):
    t.setImageLength(60000, phi=phi)
    phis.append(phi)
    wfmLs.append(t.wfmLength)
phis = np.array(phis)
wfmLs = np.array(wfmLs) / 1000
plt.plot(phis, wfmLs, label=f'imL={t.imLength / 1000:.1f} mm')
plt.xlabel('Vertical angle [deg.]')
plt.ylabel('Length on waveform [mm]')
plt.legend()
plt.show()
