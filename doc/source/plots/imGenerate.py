import matplotlib.pyplot as plt
import matplotlib.patches as patches
import math
import numpy as np
from scipy.ndimage import rotate

from treco import Track


def imageFromLossDemo(loss,
                      size: tuple[int, int],
                      theta: float,
                      x0: int,
                      y0: int,):
    initSize = (math.ceil(3 * max(size)), math.ceil(3 * max(size)))
    p = np.zeros(initSize)
    middlePoint = (initSize[0] // 2, initSize[1] // 2)
    if loss.size > middlePoint[1]:
        loss = loss[:middlePoint[1]]
    p[middlePoint[0], middlePoint[1]:middlePoint[1] + loss.size] = loss
    beforeRot = np.copy(p)
    p = rotate(p, theta, reshape=False)
    afterRot = np.copy(p)
    y0Demo = initSize[0] // 2 - y0
    x0Demo = initSize[0] // 2 - x0
    image = p[initSize[0] // 2 - y0:initSize[0] // 2 - y0 + size[0],
              initSize[1] // 2 - x0:initSize[1] // 2 - x0 + size[1]]
    return beforeRot, afterRot, image, x0Demo, y0Demo


t = Track('proton',
          e0=500,
          phi=30,
          theta=60)
imSize = (20, 20)
x0 = 7
y0 = 15
binning = 1
camLoss = t.makeImageLoss(660, 1)

plt.style.use('docstyle.mplstyle')
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(ncols=2, nrows=2)

beforeRot, afterRot, image, x0, y0 = imageFromLossDemo(camLoss,
                                                       imSize,
                                                       t.theta,
                                                       x0, y0)
rect = patches.Rectangle((x0, y0), imSize[1], imSize[0], fill=False, lw=1, edgecolor='lime')
rect2 = patches.Rectangle((x0, y0), imSize[1], imSize[0], fill=False, lw=1, edgecolor='lime')

ax1.set_title('Image loss profile')
ax1.set_xlabel('Bin number')
# ax1.set_xlim(-5, 50)
ax1.plot(camLoss)

ax2.set_title('Image before rotation')
ax2.imshow(beforeRot, cmap='hot')
ax2.add_patch(rect)

ax3.set_title('Image after rotation')
ax3.imshow(afterRot, cmap='hot')
ax3.add_patch(rect2)

ax4.set_title('Final image')
ax4.imshow(image, cmap='hot')

plt.show()
