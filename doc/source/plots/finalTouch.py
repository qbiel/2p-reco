import matplotlib.pyplot as plt

from treco import Track, ImageSimulator, WfmSimulator


t = Track('proton', e0=800, theta=60, phi=-30)

t2 = Track('proton', e0=600, theta=170, phi=60)

isim = ImageSimulator(x0=75, y0=50,
                      imSize=(64, 128),
                      binning=1,
                      sigma=1.5,
                      scale=1)
wsim = WfmSimulator(z0=1,
                    samplingRate=100,
                    wfmSize=500,
                    sigma=5,
                    scale=1)
isim.addTrack(t)
isim.addTrack(t2)
wsim.addTrack(t)
wsim.addTrack(t2)

plt.style.use('docstyle.mplstyle')
fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(ncols=2, nrows=3)

ax1.imshow(isim._unpadImage(t.image), cmap='hot')
ax1.set_title('Track 1 - 800keV')
ax3.imshow(isim._unpadImage(t2.image), cmap='hot')
ax3.set_title('Track 2 - 600keV')


ax2.plot(np.linspace(wsim.wfmStart * 2, wsim._getWfmStop() * 2, wsim.wfmSize * 2), t.wfm)
ax4.plot(np.linspace(wsim.wfmStart * 2, wsim._getWfmStop() * 2, wsim.wfmSize * 2), t2.wfm)
ax2.set_ylim(-1,28)
ax4.set_ylim(-1,28)

ax5.set_title('Final model')
ax5.imshow(isim.simImage, cmap='hot')
ax6.plot(wsim.wfmX, wsim.simWfm)

fig.tight_layout()
plt.show()