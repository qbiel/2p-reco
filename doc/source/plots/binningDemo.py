import matplotlib.pyplot as plt
import numpy as np
import math
from treco import Track

plt.style.use('./docstyle.mplstyle')
# fig = plt.figure(figsize=(3.2,2.4), dpi=600)
# fig = plt.figure(figsize=(6.4,4.8), dpi=600)
fig = plt.figure()
plt.tight_layout()
print(fig.get_size_inches())
print(fig.dpi)
ax = fig.add_axes([0.05, 0.4, 0.9, 0.55])
axTrack = fig.add_axes([0.05, 0.05, 0.9, 0.2])

# prepare track and loss profile
t = Track('proton', phi=0, theta=0)
t.range = 4000
pixelSize = 650
loss = t.makeImageLoss(pixelSize=pixelSize)
image = t.makeImage(loss, (9, 5 + t.range // pixelSize), sigma=0.9, x0=0, y0=4)[0]

# draw track
axTrack.imshow(image, cmap='hot', aspect='auto', interpolation='None')
axTrack.set_xlim(-1, t.range // pixelSize + 2)
axTrack.axis('off')
for i in np.arange(-0.5, 11, 1):
    axTrack.axvline(x=i)

# draw loss profile
prange = t.range
binSize = pixelSize / math.cos(math.pi * t.phi / 180)
x = np.arange(prange, -binSize, -binSize)
loss = np.pad(loss, (0,1))
label = f'Bin size dL={binSize}um\nTrack range L={t.range}um'
ax.bar(x, loss, width=-binSize, lw=1, ec='black', align='edge', label=label)

# track labels
props = dict(boxstyle='round', facecolor='wheat', alpha=0.8)
text = r'$E_0 = E(L)$'
axTrack.text(-0.75, 1, text, fontsize=13, bbox=props, color='black')
for i in range(1, t.range // pixelSize + 1):
    text = r'$E_{} = E(L - {}dL)$'.format(i,i)
    axTrack.text(-0.75 + i, 1, text, fontsize=13, bbox=props, color='black')

# loss profile labels
props = dict(boxstyle='round', facecolor='blue', alpha=0.5)
for i in range(0, t.range // pixelSize):
    text = r'$dE_{0} = E_{0} - E_{1}$'.format(i,i+1)
    ax.text(t.range - binSize * (i + 0.2) + 50, 5, text, fontsize=14, color='white')
text = r'$dE_6 = E_6$'
ax.text(t.range - binSize * (6 + 0.25), 5, text, bbox=props, fontsize=14, color='white')


ax.set_xlabel('Position on track [um]')
ax.set_ylabel('Energy loss in bin [keV]')
ax.invert_xaxis()
ax.legend(fontsize=14)


plt.show()
