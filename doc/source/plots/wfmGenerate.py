import matplotlib.pyplot as plot
import numpy as np

from treco import Track

t = Track('proton', e0=1500, phi=30)
sampling = 100
wfmSize = 2000
driftVelocity = 19300
wfm = np.zeros(wfmSize)
wfmStart = -10
wfmStop = 10
wfmX = np.linspace(wfmStart, wfmStop, wfmSize)
loss = t.makeWfmLoss(driftVelocity=driftVelocity, sampling=sampling)

plt.style.use('docstyle.mplstyle')
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(ncols=2, nrows=2)
ax1.set_title('Wfm loss profile')
ax1.set_xlabel('Bin number')
ax1.set_ylabel('Energy loss in bin [kev]')
ax1.plot(loss)

ax2.set_title('Loss profile flipped')
ax2.set_xlabel('Bin number')
ax2.set_ylabel('Energy loss in bin [kev]')
ax2.plot(np.flip(loss))

ax3.set_title('Waveform in the middle')
ax3.set_xlabel('Time [us]')
ax3.set_ylabel('Energy loss in bin [kev]')
ax3.plot(np.linspace(wfmStart * 2, wfmStop * 2, wfmSize * 2),
         t.makeWfm(loss, size=wfmSize, padded=True)[0])

wfm = t.makeWfm(loss, size=wfmSize, z0=5 * sampling)[0]
ax4.set_title('Final waveform')
ax4.set_xlabel('Time [us]')
ax4.set_ylabel('Energy loss in bin [kev]')
ax4.plot(wfmX, wfm)

fig.tight_layout()
plt.show()
