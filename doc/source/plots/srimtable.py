import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
import os

import treco.srimUtils as sr
from treco import Track

srimFile = os.path.join(os.path.dirname(os.getcwd()),
                        '../../srimTables/srimOutH.txt')
srimDic = sr.SrimOutput(srimFile)
energyFromFile = srimDic.energyArr
rangeFromFile = srimDic.rangeArr / 1000
energyRangeF = interp1d(energyFromFile, rangeFromFile)
energyX = np.arange(1,5050,10)

plt.style.use('docstyle.mplstyle')
fig = plt.figure()

plt.plot(energyX, energyRangeF(energyX), label='interpolation', zorder=1, color='orange')
plt.scatter(energyFromFile, rangeFromFile, label='table points', color='blue', zorder=2, s=20)

plt.xlabel('Ion Energy [keV]')
plt.ylabel('Ion range [mm]')
plt.xlim(-100,5100)
plt.ylim(-10,500)


energyZoom = np.arange(1,210,10)
ax1Zoom = fig.add_axes([0.3,0.5,0.2,0.3])
prangeZoom = energyRangeF(energyZoom)
ax1Zoom.scatter(energyFromFile, rangeFromFile, color='blue', zorder=2, s=20)
ax1Zoom.plot(energyZoom,prangeZoom, zorder=1, color='orange')
ax1Zoom.set_xlim(0,200)
ax1Zoom.set_ylim(0,5)
ax1Zoom.set_xlabel('Ion energy [keV]')
ax1Zoom.set_ylabel('Ion range [mm]')

plt.show()