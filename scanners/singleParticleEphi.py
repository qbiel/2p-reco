import sys
import os
import numpy as np
from tqdm import tqdm
import configparser
from argparse import ArgumentParser
from multiprocessing import Pool

from treco import ImageSimulator, WfmSimulator, Track
import treco.utils.scanParser as sp
from treco.utils.ExpImage import ExpImage
from treco.utils.ExpWaveform import ExpWaveform
from treco.utils.datasets import makeDataset
from treco.scanProcessors.singleTrack import processWfmEphi, processImageEphi
from treco.utils import commonParamList
from treco.utils.constrainParamSpace import makeConstraintParamListEphi


def updateP(args: tuple):
    """
    Apply result of processWfmEphi() function to results arrays
    and update the progress bar.

    This function is indended as a callback funtion for multiprocessing
    worker pool.

    Arguments:
        args: tuple returned by point processor
    Global variables:
        ds:   DataSet instance storing results
        pbar: progress bar instance

    """
    dzeta, bestParams, processedCount, e0, phi = args
    loc = {'e0_0': e0, 'phi_0': phi}
    ds['dzetasP'].loc[loc] = 1000 * dzeta
    loc = loc | {'subparam': ['z0', 'sigmaP', 'scaleP']}
    ds['bestSubparams'].loc[loc] = bestParams[[2, 4, 6]]
    pbar.update(processedCount)


def updateC(args: tuple):
    """
    Apply result of processPointImage() function to results arrays
    and update the progress bar.

    This function is indended as a callback funtion for multiprocessing
    worker pool.

    Arguments:
        args: tuple returned by point processor
    Global variables:
        ds:   DataSet instance storing results
        pbar: progress bar instance

    """
    dzeta, bestParams, processedCount, e0, phi, thetaList = args
    loc = {'e0_0': e0, 'phi_0': phi, 'theta_0': thetaList}
    ds['dzetasC'].loc[loc] = 1000 * dzeta
    loc = loc | {'subparam': ['x0', 'y0', 'sigmaC', 'scaleC']}
    ds['bestSubparams'].loc[loc] = bestParams[:, [0, 1, 3, 5]]
    pbar.update(processedCount)


def imageInitializer(particle: str, imShape: int, binning: int):
    """
    Create image simulator with a single track.

    This funtion is intended as an initializer function
    for multiprocessing worker pool. Simulator instance
    is passed to worker functions as global variable.

    Arguments:
        particle: particle name (accepted by Track object)
        imShape: shape of the simulated image
        binning: camera binning

    """
    global iSim
    iSim = ImageSimulator(imSize=imShape,
                          binning=binning,
                          simulateProjections=False)
    trackImage = Track(particle,
                       simulateProjs=False,
                       simulateWfm=False)
    iSim.addTrack(trackImage)


def wfmInitializer(particle: str, wfmSize: int, sampling: int):
    """
    Create waveform simulator with a single track.

    This funtion is intended as an initializer function
    for multiprocessing worker pool. Simulator instance
    is passed to worker functions as global variable.
    Arguments:
        particle: particle name (accepted by Track object)


    """

    global wSim
    wSim = WfmSimulator(z0=0,
                        wfmSize=wfmSize,
                        samplingRate=sampling)
    trackWfm = Track(particle,
                     simulateProjs=False,
                     simulateImage=False)
    wSim.addTrack(trackWfm)


def workerP(*args, **kwargs):
    return processWfmEphi(wSim, *args, **kwargs)


def workerC(*args, **kwargs):
    return processImageEphi(iSim, *args, **kwargs)


def errorHandler(e):
    """Simple error handler."""
    tqdm.write('error')
    tqdm.write("-->{}<--".format(e.__cause__))

# ==================== DEFINITIONS END HERE =============================


# parse config file path
parser = ArgumentParser(
    description='''Parameter scanner for single particle event.''')
parser.add_argument('--config', '-c', default='parameterScanSingle.cfg',
                    help='Config file containing input/output\
                          paths and scan ranges')
parser.add_argument('--nworkers', '-n', default=1, type=int,
                    help='Number of parallel workers')
parser.add_argument('--test', '-t', action='store_true',
                    help='Test mode, not saving results')
parser.add_argument('--force', '-f', action='store_true',
                    help='Force write to file even if file exists.')
args = parser.parse_args()

# get config file
cfg = configparser.ConfigParser()
cfg.read(args.config)

# set saving path and check if exists
outputPath = os.path.join(cfg['Paths']['outputDirectory'],
                          cfg['Paths']['outputFilename'])
if os.path.exists(outputPath) and not args.force and not args.test:
    raise IOError("Output file of given name exists. To override, run with -f")

# get parameter arrays (e0/phi or imL/wfmL,phiSgn)
try:
    paramArr = np.load(cfg['Paths']['paramArray'])
except KeyError:
    print('Parameter array with constraints not found. Creating...')
    paramArr = makeConstraintParamListEphi(cfg, lambda x, y: True)

# get theta arrays
thetaList = sp.getParticleParameter(cfg, 1, 'theta')

# get subparameter arrays
cpList = commonParamList.fromConfig(cfg)

# load data
try:
    wfmStart = cfg['Misc'].getfloat('pmtFragmentStart')
    wfmStop = cfg['Misc'].getfloat('pmtFragmentStop')
except KeyError:
    wfmStart = 1e-15
    wfmStop = 2e-15

expWfm = ExpWaveform(path=cfg['Paths']['pmtPath'],
                     start=wfmStart,
                     stop=wfmStop,
                     sigma=cfg['Misc'].getfloat('pmtSmoothSigma'),
                     bg=cfg['Misc'].getint('pmtBgLevel'),
                     amplitude=1,
                     scale=1)

expImage = ExpImage(path=cfg['Paths']['imagePath'],
                    sigma=cfg['Misc'].getfloat('imageSmoothSigma'),
                    bg=0,
                    amplitude=1,
                    scale=1)

# create dataset for dzetas and best parameters
ds = makeDataset('ephi', cfg)

# print path info and wait for user to confirm
print('Scanning image {}'.format(cfg['Paths']['imagePath']))
print('Output path {}'.format(outputPath))
if args.test:
    print('TEST MODE - output will not be saved')
if not (input('Continue? (y/N)') in ['y', 'Y']):
    print('Aborting...')
    sys.exit(0)

# =========================SINGLE THREAD=========================

# kept outside multiprocessing frame for debugging purposes

if not args.nworkers > 1:
    wfmInitializer(cfg['Particle1']['particle'], expWfm.wfm.size, expWfm.sampling)
    pbar = tqdm(total=paramArr.size * len(cpList.z0) *
                len(cpList.sigmaP) * len(cpList.scaleP), position=0)
    pbar.set_description('Analyzing PMT waveforms...')

    # scan wfm matrix
    for item in paramArr:
        results = processWfmEphi(wSim, expWfm, item['e0_0'], item['phi_0'], cpList)
        updateP(results)
    pbar.close()

    imageInitializer(cfg['Particle1']['particle'],
                     expImage.image.shape,
                     cfg['Misc'].getint('camerabinning'))
    pbar = tqdm(total=paramArr.size * len(thetaList) *
                len(cpList.x0) * len(cpList.y0) * len(cpList.sigmaC) *
                len(cpList.scaleC), position=0)
    pbar.set_description('Analyzing camara images...')

    # scan camera matrix
    for item in paramArr:
        results = processImageEphi(iSim, expImage, item['e0_0'], item['phi_0'],
                                   thetaList, cpList)
        updateC(results)
    pbar.close()

# =========================MULTI THREAD=========================
else:

    pbar = tqdm(total=paramArr.size * len(cpList.z0) *
                len(cpList.sigmaP) * len(cpList.scaleP), position=0)
    pbar.set_description('Analyzing PMT waveforms...')

    workers = []
    pool = Pool(args.nworkers,
                initializer=wfmInitializer,
                initargs=(cfg['Particle1']['particle'],
                          expWfm.wfm.size,
                          expWfm.sampling))
    for item in paramArr:
        workers.append(pool.apply_async(workerP,
                                        args=(expWfm, item['e0_0'], item['phi_0'], cpList),
                                        callback=updateP,
                                        error_callback=errorHandler))
    pool.close()
    pool.join()
    pbar.close()

    pbar = tqdm(total=paramArr.size * len(thetaList) *
                len(cpList.x0) * len(cpList.y0) * len(cpList.sigmaC) *
                len(cpList.scaleC), position=0)
    workers = []
    pbar.set_description('Analyzing camera images...')

    pool = Pool(args.nworkers, initializer=imageInitializer,
                initargs=(cfg['Particle1']['particle'],
                          expImage.image.shape,
                          cfg['Misc'].getint('camerabinning')))
    for item in paramArr:
        workers.append(pool.apply_async(workerC,
                                        args=(expImage, item['e0_0'], item['phi_0'],
                                              thetaList, cpList),
                                        callback=updateC,
                                        error_callback=errorHandler))
    pool.close()
    pool.join()
    pbar.close()

if not args.test:
    ds.to_netcdf(outputPath, 'w')
