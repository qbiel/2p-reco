import sys
import os
import numpy as np
from tqdm import tqdm
import configparser
from argparse import ArgumentParser
from multiprocessing import Pool

from treco import ImageSimulator, WfmSimulator, Track
import treco.utils.scanParser as sp
from treco.utils.trackDataclasses import commonParamList
from treco.utils.ExpImage import ExpImage
from treco.utils.ExpWaveform import ExpWaveform
from treco.scanProcessors.doubleTrack import processWfmEphi, processImageEphi
from treco.utils.constrainParamSpace import makeConstraintParamListEphi
from treco.utils.datasets import makeDataset


def updateP(args: tuple):
    """
    Apply result of processPointWfm() function to results arrays
    and update the progress bar.

    This function is indended as a callback funtion for multiprocessing
    worker pool.

    Arguments:
        args: tuple returned by point processor
    Global variables:
        ds:   DataSet instance storing results
        pbar: progress bar instance

    """
    dzeta, bestParams, processedCount, e0Pair, phiPair = args
    loc = {
        'e0_0': e0Pair[0], 'e0_1': e0Pair[1],
        'phi_0': phiPair[0], 'phi_1': phiPair[1]
    }
    ds['dzetasP'].loc[loc] = 1000 * dzeta

    loc = loc | {'subparam': ['z0', 'sigmaP', 'scaleP']}
    ds['bestSubparams'].loc[loc] = bestParams[[2, 4, 6]]
    pbar.update(processedCount)


def updateC(args: tuple):
    """
    Apply result of processPointImage() function to results arrays
    and update the progress bar.

    This function is indended as a callback funtion for multiprocessing
    worker pool.

    Arguments:
        args: tuple returned by processPointWfm
    Global variables:
        ds:   DataSet instance storing results
        pbar: progress bar instance

    """
    dzeta, bestParams, processedCount, e0Pair, phiPair, thetaListPair = args
    loc = {
        'e0_0': e0Pair[0], 'e0_1': e0Pair[1],
        'phi_0': phiPair[0], 'phi_1': phiPair[1],
        'theta_0': thetaListPair[0], 'theta_1': thetaListPair[1]}
    ds['dzetasC'].loc[loc] = 1000 * dzeta
    loc = loc | {'subparam': ['x0', 'y0', 'sigmaC', 'scaleC']}
    ds['bestSubparams'].loc[loc] = bestParams[:, :, [0, 1, 3, 5]]
    pbar.update(processedCount)


def imageInitializer():
    """
    Create image simulator with a single track.

    This funtion is intended as an initializer function
    for multiprocessing worker pool. Simulator instance
    is passed to worker functions as global variable.

    """
    global iSim
    iSim = ImageSimulator(imSize=expImage.image.shape,
                          binning=cfg['Misc'].getint('camerabinning'),
                          simulateProjections=False,
                          x0=cpList.x0[0], y0=cpList.y0[0])
    for i in range(2):
        trackImage = Track(cfg['Particle{}'.format(i + 1)]['particle'])
        iSim.addTrack(trackImage)


def wfmInitializer():
    """
    Create waveform simulator with a single track.

    This funtion is intended as an initializer function
    for multiprocessing worker pool. Simulator instance
    is passed to worker functions as global variable.

    """

    global wSim
    wSim = WfmSimulator(z0=cpList.z0[0],
                        wfmSize=expWfm.wfm.size,
                        samplingRate=expWfm.sampling,
                        wfmStart=cfg['Misc'].getfloat('pmtFragmentStart'))
    for i in range(2):
        trackWfm = Track(cfg['Particle{}'.format(i + 1)]['particle'])
        wSim.addTrack(trackWfm)


def workerP(*args, **kwargs):
    return processWfmEphi(wSim, *args, **kwargs)


def workerC(*args, **kwargs):
    return processImageEphi(iSim, *args, **kwargs)


def errorHandler(e):
    """Simple error handler."""
    tqdm.write('error')
    tqdm.write("-->{}<--".format(e.__cause__))

# ==================== DEFINITIONS END HERE =============================


# parse config file path
parser = ArgumentParser(
    description='''Parameter scanner for single particle event.''')
parser.add_argument('--config', '-c', default='parameterScanLite.cfg',
                    help='Config file containing input/output\
                          paths and scan ranges')
parser.add_argument('--nworkers', '-n', default=1, type=int,
                    help='Number of parallel workers')
parser.add_argument('--test', '-t', action='store_true',
                    help='Test mode, not saving results')
parser.add_argument('--force', '-f', action='store_true',
                    help='Force write to file even if file exists.')
args = parser.parse_args()

# get config file
cfg = configparser.ConfigParser()
cfg.read(args.config)

# set saving path and check if exists
outputPath = os.path.join(cfg['Paths']['outputDirectory'],
                          cfg['Paths']['outputFilename'])
if os.path.exists(outputPath) and not args.force and not args.test:
    raise IOError("Output file of given name exists. To override, run with -f")

# get parameter arrays (e0/phi or imL/wfmL,phiSgn)
try:
    paramArr = np.load(cfg['Paths']['paramArray'])
except KeyError:
    print('Parameter array with constraints not found. Creating...')
    paramArr = makeConstraintParamListEphi(cfg, lambda x, y: True)

# get theta arrays
thetaList1 = sp.getParticleParameter(cfg, 1, 'theta')
thetaList2 = sp.getParticleParameter(cfg, 2, 'theta')

# get subparameter arrays
cpList = commonParamList.fromConfig(cfg)

# load data
try:
    wfmStart = cfg['Misc'].getfloat('pmtFragmentStart')
    wfmStop = cfg['Misc'].getfloat('pmtFragmentStop')
except KeyError:
    wfmStart = 1e-15
    wfmStop = 2e-15

expWfm = ExpWaveform(path=cfg['Paths']['pmtPath'],
                     start=wfmStart,
                     stop=wfmStop,
                     sigma=cfg['Misc'].getfloat('pmtSmoothSigma'),
                     bg=cfg['Misc'].getint('pmtBgLevel'),
                     amplitude=1,
                     scale=1)

expImage = ExpImage(path=cfg['Paths']['imagePath'],
                    sigma=cfg['Misc'].getfloat('imageSmoothSigma'),
                    bg=0,
                    amplitude=1,
                    scale=1)

# create dataset for dzetas and best parameters
ds = makeDataset('ephi', cfg)
print(ds)

# print path info and wait for user to confirm
print('Scanning image {}'.format(cfg['Paths']['imagePath']))
print('Output path {}'.format(outputPath))
if args.test:
    print('TEST MODE - output will not be saved')
if not (input('Continue? (y/N)') in ['y', 'Y']):
    print('Aborting...')
    sys.exit(0)


# =========================SINGLE THREAD=========================

# kept outside multiprocessing frame for debugging purposes
if not args.nworkers > 1:
    wfmInitializer()
    imageInitializer()
    # pbar = tqdm(total=paramArr.size * len(cpList.z0) *
    #             len(cpList.sigmaP) * len(cpList.scaleP), position=0)
    # pbar.set_description('Analyzing PMT waveforms...')

    # # scan wfm matrix
    # for item in paramArr:
    #     results = processWfmEphi(wSim, expWfm,
    #                              (item['e0_0'], item['e0_1']),
    #                              (item['phi_0'], item['phi_1']),
    #                              cpList)
    #     updateP(results)
    # pbar.close()

    pbar = tqdm(total=paramArr.size *
                len(thetaList1) * len(thetaList2) *
                len(cpList.x0) * len(cpList.y0) * len(cpList.sigmaC) *
                len(cpList.scaleC), position=0)
    pbar.set_description('Analyzing camara images...')

    # scan camera matrix
    # scan wfm matrix
    for item in paramArr:
        results = processImageEphi(iSim, expImage,
                                   (item['e0_0'], item['e0_1']),
                                   (item['phi_0'], item['phi_1']),
                                   (thetaList1, thetaList2),
                                   cpList)
        updateC(results)
    pbar.close()


# =========================MULTI THREAD=========================
else:

    pbar = tqdm(total=paramArr.size * len(cpList.z0) *
                len(cpList.sigmaP) * len(cpList.scaleP), position=0)
    pbar.set_description('Analyzing PMT waveforms...')

    pool = Pool(args.nworkers, initializer=wfmInitializer)
    for item in paramArr:
        pool.apply_async(workerP,
                         args=(expWfm,
                               (item['e0_0'], item['e0_1']),
                               (item['phi_0'], item['phi_1']),
                               cpList),
                         callback=updateP,
                         error_callback=errorHandler)
    pool.close()
    pool.join()
    pbar.close()

    pbar = tqdm(total=paramArr.size * len(thetaList1) * len(thetaList2) *
                len(cpList.x0) * len(cpList.y0) * len(cpList.sigmaC) *
                len(cpList.scaleC), position=0)
    pbar.set_description('Analyzing camara images...')

    pool = Pool(args.nworkers, initializer=imageInitializer)
    for item in paramArr:
        pool.apply_async(workerC,
                         args=(expImage,
                               (item['e0_0'], item['e0_1']),
                               (item['phi_0'], item['phi_1']),
                               (thetaList1, thetaList2),
                               cpList),
                         callback=updateC,
                         error_callback=errorHandler)
    pool.close()
    pool.join()
    pbar.close()
if not args.test:
    ds.to_netcdf(outputPath, 'w')
