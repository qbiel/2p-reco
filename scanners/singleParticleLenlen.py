#!/usr/bin/python3

import sys
import os
import numpy as np
from tqdm import tqdm
import configparser
from argparse import ArgumentParser
from multiprocessing import Pool

from treco import ImageSimulator, WfmSimulator, Track
import treco.utils.scanParser as sp
from treco.utils.ExpImage import ExpImage
from treco.utils.ExpWaveform import ExpWaveform
from treco.utils.datasets import makeDataset
from treco.scanProcessors.singleTrack import processWfmLenlen, processImageLenlen
from treco.utils import commonParamList
from treco.utils.constrainParamSpace import makeConstraintParamListLenlen


def updateP(args: tuple):
    """
    Apply result of processWfmEphi() function to results arrays
    and update the progress bar.

    This function is indended as a callback funtion for multiprocessing
    worker pool.

    Arguments:
        args: tuple returned by point processor
    Global variables:
        ds:   DataSet instance storing results
        pbar: progress bar instance

    """
    dzeta, bestParams, processedCount, imL, wfmL, phiSgn = args
    loc = {'imL_0': imL, 'wfmL_0': wfmL, 'phiSgn_0': phiSgn}
    ds['dzetasP'].loc[loc] = dzeta
    loc = loc | {'subparam': ['z0', 'sigmaP', 'scaleP']}
    ds['bestSubparams'].loc[loc] = bestParams[[2, 4, 6]]
    pbar.update(processedCount)


def updateC(args: tuple):
    """
    Apply result of processPointImage() function to results arrays
    and update the progress bar.

    This function is indended as a callback funtion for multiprocessing
    worker pool.

    Arguments:
        args: tuple returned by point processor
    Global variables:
        ds:   DataSet instance storing results
        pbar: progress bar instance

    """
    dzeta, bestParams, processedCount, imL, wfmL, phiSgn, thetaList = args
    loc = {'imL_0': imL, 'wfmL_0': wfmL,
           'phiSgn_0': phiSgn, 'theta_0': thetaList}
    ds['dzetasC'].loc[loc] = dzeta
    loc = loc | {'subparam': ['x0', 'y0', 'sigmaC', 'scaleC']}
    ds['bestSubparams'].loc[loc] = bestParams[:, [0, 1, 3, 5]]
    pbar.update(processedCount)


def imageInitializer(particle: str, imShape: int, binning: int):
    """
    Create image simulator with a single track.

    This funtion is intended as an initializer function
    for multiprocessing worker pool. Simulator instance
    is passed to worker functions as global variable.

    Arguments:
        particle: particle name (accepted by Track object)
        imShape: shape of the simulated image
        binning: camera binning

    """
    global iSim
    iSim = ImageSimulator(imSize=imShape,
                          binning=binning,
                          simulateProjections=False)
    trackImage = Track(particle)
    iSim.addTrack(trackImage)


def wfmInitializer(particle: str, wfmSize: int, sampling: int, wfmStart: float):
    """
    Create waveform simulator with a single track.

    This funtion is intended as an initializer function
    for multiprocessing worker pool. Simulator instance
    is passed to worker functions as global variable.
    Arguments:
        particle: particle name (accepted by Track object)


    """

    global wSim
    wSim = WfmSimulator(wfmSize=wfmSize,
                        samplingRate=sampling,
                        wfmStart=wfmStart)
    trackWfm = Track(particle)
    wSim.addTrack(trackWfm)


def workerP(*args, **kwargs):
    return processWfmLenlen(wSim, *args, **kwargs)


def workerC(*args, **kwargs):
    return processImageLenlen(iSim, *args, **kwargs)


def errorHandler(e):
    """Simple error handler."""
    tqdm.write('error')
    tqdm.write("-->{}<--".format(e.__cause__))

# ==================== DEFINITIONS END HERE =============================


# parse config file path
parser = ArgumentParser(
    description='''Parameter scanner for single particle event.''')
parser.add_argument('--config', '-c', default='parameterScanSingle.cfg',
                    help='Config file containing input/output\
                          paths and scan ranges')
parser.add_argument('--nworkers', '-n', default=1, type=int,
                    help='Number of parallel workers')
parser.add_argument('--test', '-t', action='store_true',
                    help='Test mode, not saving results')
parser.add_argument('--force', '-f', action='store_true',
                    help='Force write to file even if file exists.')
parser.add_argument('--yes', '-y', action='store_true',
                    help='Do not ask user to confirm.')
args = parser.parse_args()

# get config file
cfg = configparser.ConfigParser()
cfg.read(args.config)

# set saving path and check if exists
outputPath = os.path.join(cfg['Paths']['outputDirectory'],
                          cfg['Paths']['outputFilename'])
if os.path.exists(outputPath) and not args.force and not args.test:
    raise IOError("Output file of given name exists. To override, run with -f")

# get parameter arrays (e0/phi or imL/wfmL,phiSgn)
try:
    paramArr = np.load(cfg['Paths']['paramArray'])
except KeyError:
    print('Parameter array with constraints not found. Creating...')
    paramArr = makeConstraintParamListLenlen(cfg, lambda x, y: True)

# get theta arrays
thetaList = sp.getParticleParameter(cfg, 1, 'theta')

# get subparameter arrays
cpList = commonParamList.fromConfig(cfg)

# load data
try:
    wfmStart = cfg['Misc'].getfloat('pmtFragmentStart')
    wfmStop = cfg['Misc'].getfloat('pmtFragmentStop')
except KeyError:
    wfmStart = None
    wfmStop = None

expWfm = ExpWaveform(path=cfg['Paths']['pmtPath'],
                     start=wfmStart,
                     stop=wfmStop,
                     sigma=cfg['Misc'].getfloat('pmtSmoothSigma'),
                     bg=cfg['Misc'].getint('pmtBgLevel'),
                     scale=1)

expImage = ExpImage(path=cfg['Paths']['imagePath'],
                    sigma=cfg['Misc'].getfloat('imageSmoothSigma'),
                    bg=0,
                    scale=1)

# create dataset for dzetas and best parameters
ds = makeDataset('lenlen', cfg)

# print path info and wait for user to confirm
print('Scanning image {}'.format(cfg['Paths']['imagePath']))
print('Output path {}'.format(outputPath))
if args.test:
    print('TEST MODE - output will not be saved')
if not args.yes:
    if not (input('Continue? (y/N)') in ['y', 'Y']):
        print('Aborting...')
        sys.exit(0)

# =========================SINGLE THREAD=========================

# kept outside multiprocessing frame for debugging purposes

if not args.nworkers > 1:
    wfmInitializer(cfg['Particle1']['particle'], expWfm.wfm.size, expWfm.sampling, expWfm.wfmStart)
    pbar = tqdm(total=paramArr.size * len(cpList.z0) *
                len(cpList.sigmaP), position=0)
    pbar.set_description('Analyzing PMT waveforms...')

    # scan wfm matrix
    for item in paramArr:
        results = processWfmLenlen(wSim, expWfm,
                                   item['imL_0'],
                                   item['wfmL_0'],
                                   item['phiSgn_0'], cpList)
        updateP(results)
    pbar.close()

    imageInitializer(cfg['Particle1']['particle'],
                     expImage.image.shape,
                     cfg['Misc'].getint('camerabinning'))
    pbar = tqdm(total=paramArr.size * len(thetaList) *
                len(cpList.x0) * len(cpList.y0) * len(cpList.sigmaC), position=0)
    pbar.set_description('Analyzing camara images...')

    # scan camera matrix
    for item in paramArr:
        results = processImageLenlen(iSim, expImage,
                                     item['imL_0'],
                                     item['wfmL_0'],
                                     item['phiSgn_0'],
                                     thetaList, cpList)
        updateC(results)
    pbar.close()

# =========================MULTI THREAD=========================
else:

    pbar = tqdm(total=paramArr.size * len(cpList.z0) * len(cpList.sigmaP), position=0)
    pbar.set_description('Analyzing PMT waveforms...')

    workers = []
    pool = Pool(args.nworkers,
                initializer=wfmInitializer,
                initargs=(cfg['Particle1']['particle'],
                          expWfm.wfm.size,
                          expWfm.sampling,
                          expWfm.wfmStart))
    for item in paramArr:
        workers.append(pool.apply_async(workerP,
                                        args=(expWfm,
                                              item['imL_0'],
                                              item['wfmL_0'],
                                              item['phiSgn_0'],
                                              cpList),
                                        callback=updateP,
                                        error_callback=errorHandler))
    pool.close()
    pool.join()
    pbar.close()

    pbar = tqdm(total=paramArr.size * len(thetaList) *
                len(cpList.x0) * len(cpList.y0) * len(cpList.sigmaC), position=0)
    workers = []
    pbar.set_description('Analyzing camera images...')

    pool = Pool(args.nworkers, initializer=imageInitializer,
                initargs=(cfg['Particle1']['particle'],
                          expImage.image.shape,
                          cfg['Misc'].getint('camerabinning')))
    for item in paramArr:
        workers.append(pool.apply_async(workerC,
                                        args=(expImage, item['imL_0'],
                                              item['wfmL_0'],
                                              item['phiSgn_0'],
                                              thetaList, cpList),
                                        callback=updateC,
                                        error_callback=errorHandler))
    pool.close()
    pool.join()
    pbar.close()

if not args.test:
    ds.to_netcdf(outputPath, 'w')
